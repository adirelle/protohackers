# syntax=docker/dockerfile:1

FROM rust:1-buster as builder

ARG TARGET

WORKDIR /src

COPY Cargo.toml Cargo.lock /src/
COPY packages/ /src/packages/

ENV CARGO_HOME=/cargo CARGO_TARGET_DIR=/target

RUN --mount=type=cache,target=${CARGO_HOME} \
    --mount=type=cache,target=${CARGO_TARGET_DIR}/debug/build \
    cargo build -p ${TARGET}

FROM debian:buster-slim

ARG TARGET
ENV TARGET=${TARGET}

COPY --from=builder /target/debug/${TARGET} /usr/local/bin/${TARGET}

EXPOSE 20000/TCP

CMD /usr/local/bin/${TARGET} 0.0.0.0:20000
