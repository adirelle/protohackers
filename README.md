# Protohackers

Solutions to [Protohackers](https://protohackers.com/) exercices, written in rust.

## Running using docker compose

Use [docker compose](https://docs.docker.com/compose/) to compile and start all the services locally :

```bash
docker compose up
```

This will start all servers ports 20000-2000x on all addresses (`0.0.0.0`) by default.

The listening address can be configured by setting the PUBLIC_IP environment variable.

The port number is 20000 plus the problem number, e.g.:

* smoke_text: 20000,
* prime_time: 20001,
* means_to_and_end: 20002,
* ...
