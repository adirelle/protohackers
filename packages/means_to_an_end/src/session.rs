use std::collections::BTreeMap;
use std::ops::RangeInclusive;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
pub struct Timestamp(i32);

impl From<i32> for Timestamp {
    fn from(value: i32) -> Self {
        Self(value)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Price(i32);

impl From<i32> for Price {
    fn from(value: i32) -> Self {
        Self(value)
    }
}

impl From<Price> for i32 {
    fn from(value: Price) -> Self {
        value.0
    }
}

#[derive(Debug)]
pub struct Session {
    prices: BTreeMap<Timestamp, Price>,
}

impl Session {
    pub fn new() -> Self {
        Session {
            prices: BTreeMap::new(),
        }
    }

    pub fn insert(&mut self, timestamp: Timestamp, price: Price) {
        _ = self.prices.insert(timestamp, price)
    }

    pub fn query(&mut self, range: RangeInclusive<Timestamp>) -> Option<Price> {
        if range.start() > range.end() {
            return None;
        }
        let values = self.prices.range(range);
        let count = values.clone().count() as i64;
        if count > 0 {
            Some(Price(
                (values.map(|(_, p)| p.0 as i64).sum::<i64>() / count) as i32,
            ))
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_one_value() {
        let mut sess = Session::new();
        let p: Price = 50.into();

        sess.insert(15.into(), p);

        assert_eq!(p, sess.query(15.into()..=16.into()).unwrap());
    }

    #[test]
    fn test_no_value() {
        let mut sess = Session::new();
        assert!(sess.query(15.into()..=16.into()).is_none());
    }

    #[test]
    fn test_two_values() {
        let mut sess = Session::new();
        let expected: Price = 75.into();

        sess.insert(15.into(), 50.into());
        sess.insert(16.into(), 100.into());

        assert_eq!(expected, sess.query(15.into()..=17.into()).unwrap());
    }

    #[test]
    fn test_invalid_range() {
        let mut sess = Session::new();

        assert!(sess.query(17.into()..=15.into()).is_none());
    }
}
