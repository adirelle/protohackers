use super::session::{Price, Timestamp};
use anyhow::{anyhow, Error, Result};

#[derive(Debug, PartialEq, Eq)]
pub enum Frame {
    Insert(Timestamp, Price),
    Query(Timestamp, Timestamp),
}

impl TryFrom<&[u8]> for Frame {
    type Error = Error;

    fn try_from(value: &[u8]) -> Result<Self> {
        if value.len() != 9 {
            return Err(anyhow!("invalid frame length"));
        }

        match value[0] {
            b'I' => Ok(Frame::Insert(
                i32::from_be_bytes(value[1..5].try_into()?).into(),
                i32::from_be_bytes(value[5..9].try_into()?).into(),
            )),
            b'Q' => Ok(Frame::Query(
                i32::from_be_bytes(value[1..5].try_into()?).into(),
                i32::from_be_bytes(value[5..9].try_into()?).into(),
            )),
            _ => Err(anyhow!("invalid frame type: {}", value[0])),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_valid_insert() {
        let data = b"I\x00\x00\x00\x01\x00\x00\x00\x02";
        let f = &data[..].try_into().unwrap();

        assert_eq!(Frame::Insert(1.into(), 2.into()), *f);
    }

    #[test]
    fn parse_valid_query() {
        let data = b"Q\x00\x00\x00\x01\x00\x00\x00\x02";
        let f = &data[..].try_into().unwrap();

        assert_eq!(Frame::Query(1.into(), 2.into()), *f);
    }
}
