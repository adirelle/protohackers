mod frame;
mod session;

use std::ops::AddAssign;

use frame::Frame;
use server::prelude::*;
use session::Session;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::select;

#[tokio::main]
async fn main() -> Result<()> {
    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new())?;

    server::serve(Server, "0.0.0.0:20000".parse()?).await
}

#[derive(Debug, Default, Clone)]
struct Stats {
    inserts: usize,
    queries: usize,
    errors: usize,
}

impl AddAssign for Stats {
    fn add_assign(&mut self, rhs: Self) {
        self.inserts += rhs.inserts;
        self.queries += rhs.queries;
        self.errors += rhs.errors;
    }
}

#[derive(Debug)]
struct Server;

impl TCPHandlerFactory for Server {
    type Handler = Connection;
    type Stats = Stats;

    #[tracing::instrument]
    fn create(&mut self, socket: TcpStream, _client_addr: SocketAddr) -> Result<Self::Handler> {
        Ok(Connection::new(socket))
    }
}

#[derive(Debug)]
struct Connection {
    socket: TcpStream,
    session: Session,
    stats: Stats,
}

impl Connection {
    fn new(socket: TcpStream) -> Self {
        Self {
            socket,
            session: Session::new(),
            stats: Stats::default(),
        }
    }

    #[tracing::instrument(skip(self))]
    async fn request(&mut self, req: &[u8]) {
        if let Err(err) = self.request_inner(req).await {
            tracing::warn!("error: {}", err);
            self.stats.errors += 1;
        }
    }

    async fn request_inner(&mut self, req: &[u8]) -> Result<()> {
        match req.try_into()? {
            Frame::Insert(timestamp, price) => {
                self.stats.inserts += 1;
                self.session.insert(timestamp, price)
            }
            Frame::Query(from, to) => {
                self.stats.queries += 1;
                let result = self.session.query(from..=to).unwrap_or(0.into());
                self.socket.write_i32(result.into()).await?;
            }
        };
        Ok(())
    }
}

#[async_trait]
impl TCPHandler<Stats> for Connection {
    async fn handle(mut self, until: CancellationToken) -> Result<Stats> {
        let mut buf = [0; 9];

        loop {
            select! {
                _ = until.cancelled() => break Ok(self.stats),
                res = self.socket.read_exact(&mut buf[..]) => match res  {
                    Ok(_) => self.request(&buf[..]).await,
                    Err(err) if err.kind() == std::io::ErrorKind::UnexpectedEof => {
                        break Ok(self.stats.clone())
                    }
                    Err(err) => break Err(err.into()),
                }
            }
        }
    }
}
