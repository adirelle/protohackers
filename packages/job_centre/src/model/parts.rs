use std::fmt::{Debug, Display};

use internment::ArcIntern;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};

// ========================================== ID ==========================================

#[derive(Serialize, Deserialize, Default, PartialEq, Eq, Ord, PartialOrd, Hash, Clone, Copy)]
#[serde(transparent)]
pub struct ID(u32);

impl ID {
    pub fn next(&mut self) -> ID {
        self.0 += 1;
        ID(self.0)
    }
}

impl From<u32> for ID {
    fn from(value: u32) -> Self {
        ID(value)
    }
}

impl From<ID> for u32 {
    fn from(value: ID) -> Self {
        value.0
    }
}

impl Display for ID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "#{}", self.0)
    }
}

impl Debug for ID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "#{}", self.0)
    }
}

// ========================================== Priority ==========================================

#[derive(Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
#[serde(transparent)]
pub struct Priority(u32);

impl From<u32> for Priority {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

impl From<Priority> for u32 {
    fn from(value: Priority) -> Self {
        value.0
    }
}

impl Display for Priority {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "+{}", self.0)
    }
}

impl Debug for Priority {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "+{}", self.0)
    }
}

// ========================================== Key ==========================================

#[derive(Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
pub struct Key {
    pub priority: Priority,
    pub id: ID,
}

impl PartialEq<ID> for Key {
    fn eq(&self, other: &ID) -> bool {
        &self.id == other
    }
}

impl Display for Key {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.priority, self.id)
    }
}

impl Debug for Key {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({:?},{:?})", self.priority, self.id)
    }
}

// ========================================== Payload ==========================================

#[derive(Serialize, Deserialize, PartialEq, Default, Clone)]
#[serde(transparent)]
pub struct Payload(Map<String, Value>);

impl<'a, T: IntoIterator<Item = (&'a str, V)>, V: Into<Value>> From<T> for Payload {
    fn from(value: T) -> Self {
        Payload(Map::from_iter(
            value.into_iter().map(|(k, v)| (k.into(), v.into())),
        ))
    }
}

#[cfg(test)]
impl Payload {
    pub fn put<K: Into<String>, V: Into<Value>>(mut self, key: K, value: V) -> Self {
        self.0.insert(key.into(), value.into());
        self
    }
}

impl Display for Payload {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = serde_json::to_string(self).unwrap();
        f.write_str(&s)
    }
}

impl Debug for Payload {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = serde_json::to_string(self).unwrap();
        f.write_str(&s)
    }
}

// ========================================== QueueName ==========================================

#[derive(PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct QueueName(ArcIntern<String>);

impl Debug for QueueName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "#{}", self.0.as_str())
    }
}

impl From<&str> for QueueName {
    fn from(value: &str) -> Self {
        Self(ArcIntern::new(value.to_owned()))
    }
}

impl From<String> for QueueName {
    fn from(value: String) -> Self {
        Self(ArcIntern::new(value))
    }
}

impl AsRef<str> for QueueName {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl Display for QueueName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}
