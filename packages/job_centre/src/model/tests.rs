use futures::future::join;
use tokio::test;

use super::parts::*;
use super::*;

#[test]
async fn test_put_then_get() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let queues = [&queue1];
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);

    let id1 = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not error");

    let job = centre
        .try_get(queues)
        .await
        .expect("should not error")
        .expect("should return a job");

    assert_eq!((id1, &queue1, priority5, &payload), job.as_tuple());
}

#[test]
async fn test_aborted_job_is_released() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let queues = [&queue1];
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);

    let id1 = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not error");

    // Get then immediately drop the job
    let mut job = centre
        .try_get(queues)
        .await
        .expect("should not error")
        .expect("should return a job");

    assert!(
        job.abort().await.expect("should not error"),
        "should return true"
    );

    let job = centre
        .try_get(queues)
        .await
        .expect("should not error")
        .expect("should return a job");

    assert_eq!((id1, &queue1, priority5, &payload), job.as_tuple());
}

#[test]
async fn test_dropped_job_is_released() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let queues = [&queue1];
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);

    let id1 = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not error");

    // Get then immediately drop the job
    _ = centre
        .try_get(queues)
        .await
        .expect("should not error")
        .expect("should return a job");

    let job = centre
        .try_get(queues)
        .await
        .expect("should not error")
        .expect("should return a job");

    assert_eq!((id1, &queue1, priority5, &payload), job.as_tuple());
}

#[test]
async fn test_highest_priority_job_is_returned_first() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);
    let priority10 = Priority::from(10);

    let _id1 = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not error");

    let id2 = centre
        .put(&queue1, priority10, payload.clone())
        .await
        .expect("should not error");

    let job = centre
        .try_get([&queue1])
        .await
        .expect("should not error")
        .expect("should return a job");

    assert_eq!((id2, &queue1, priority10, &payload), job.as_tuple());
}

#[test]
async fn test_only_return_jobs_from_watched_queues() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let queue2 = QueueName::from("queue2");
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);
    let priority10 = Priority::from(10);

    let id1 = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not error");

    let _id2 = centre
        .put(&queue2, priority10, payload.clone())
        .await
        .expect("should not error");

    let job = centre
        .try_get([&queue1])
        .await
        .expect("should not error")
        .expect("should return a job");

    assert_eq!((id1, &queue1, priority5, &payload), job.as_tuple());
}

#[test]
async fn test_put_then_delete() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);

    let id = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not error");
    assert!(
        centre.delete(id).await.expect("should not error"),
        "job should have been in the queue"
    );

    assert!(
        centre
            .try_get([&queue1])
            .await
            .expect("should not error")
            .is_none(),
        "try_get should not return the job"
    );
}

#[test]
async fn test_async_single_queue_single_client() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let payload = Payload::default().put("foo", "bar");
    let priority = Priority::from(5);

    let client = {
        let centre = centre.clone();
        let queue1 = queue1.clone();

        tokio::spawn(async move { centre.get([&queue1]).await })
    };

    let id = centre
        .put(&queue1, priority, payload.clone())
        .await
        .expect("should not error");

    let job = client
        .await
        .expect("should not error")
        .expect("should return a job");

    assert_eq!((id, &queue1, priority, &payload), job.as_tuple());
}

#[test]
async fn test_async_multiple_queues_single_client() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let queue2 = QueueName::from("queue2");
    let payload = Payload::default().put("foo", "bar");
    let priority = Priority::from(5);

    let client = {
        let centre = centre.clone();
        let queue1 = queue1.clone();
        let queue2 = queue2.clone();

        tokio::spawn(async move { centre.get([&queue1, &queue2]).await })
    };

    let id = centre
        .put(&queue2, priority, payload.clone())
        .await
        .expect("should not give an error");

    let job = client
        .await
        .expect("should not give an error")
        .expect("should return a job");

    assert_eq!((id, &queue2, priority, &payload), job.as_tuple());
}

#[test]
async fn test_async_single_queue_multiple_clients() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);
    let priority10 = Priority::from(10);

    let client1 = {
        let centre = centre.clone();
        let queue1 = queue1.clone();

        tokio::spawn(async move { centre.get([&queue1]).await })
    };

    let client2 = {
        let centre = centre.clone();
        let queue1 = queue1.clone();

        tokio::spawn(async move { centre.get([&queue1]).await })
    };

    let id1 = centre
        .put(&queue1, priority10, payload.clone())
        .await
        .expect("should not give an error");

    let id2 = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not give an error");

    let (job1, job2) = join(client1, client2).await;

    let job1 = job1.expect("should not error").expect("should give a job");
    assert_eq!((id1, &queue1, priority10, &payload), job1.as_tuple());

    let job2 = job2.expect("should not error").expect("should give a job");
    assert_eq!((id2, &queue1, priority5, &payload), job2.as_tuple());
}

#[test]
async fn test_async_multiple_queue_multiple_clients() {
    let centre = Centre::default();
    let queue1 = QueueName::from("queue1");
    let queue2 = QueueName::from("queue2");
    let payload = Payload::default().put("foo", "bar");
    let priority5 = Priority::from(5);
    let priority10 = Priority::from(10);

    let client1 = {
        let centre = centre.clone();
        let queue1 = queue1.clone();
        let queue2 = queue2.clone();

        tokio::spawn(async move { centre.get([&queue1, &queue2]).await })
    };

    let client2 = {
        let centre = centre.clone();
        let queue1 = queue1.clone();
        let queue2 = queue2.clone();

        tokio::spawn(async move { centre.get([&queue1, &queue2]).await })
    };

    let id1 = centre
        .put(&queue2, priority10, payload.clone())
        .await
        .expect("should not give an error");

    let id2 = centre
        .put(&queue1, priority5, payload.clone())
        .await
        .expect("should not give an error");

    let (job1, job2) = join(client1, client2).await;

    let job1 = job1.expect("should not error").expect("should give a job");
    assert_eq!((id1, &queue2, priority10, &payload), job1.as_tuple());

    let job2 = job2.expect("should not error").expect("should give a job");
    assert_eq!((id2, &queue1, priority5, &payload), job2.as_tuple());
}
