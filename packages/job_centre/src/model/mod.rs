#[cfg(test)]
pub mod tests;

mod parts;

use crate::error::*;

use std::collections::{BTreeMap, HashMap, HashSet, VecDeque};
use std::fmt::Debug;
use tokio::sync::{mpsc, oneshot};

pub use parts::*;
use tracing::Level;

// ========================================== Message ==========================================

enum Message {
    Put(QueueName, Priority, Payload, oneshot::Sender<ID>),
    TryGet(HashSet<QueueName>, oneshot::Sender<Option<JobParts>>),
    Get(HashSet<QueueName>, mpsc::Sender<JobParts>),
    Abort(JobParts, oneshot::Sender<bool>),
    Delete(ID, oneshot::Sender<bool>),
}

// ========================================== Centre ==========================================

type MessageSender = mpsc::UnboundedSender<Message>;
type MessageReceiver = mpsc::UnboundedReceiver<Message>;

#[derive(Debug, Clone)]
pub struct Centre {
    tx: MessageSender,
}

impl Default for Centre {
    fn default() -> Self {
        let (tx, rx) = mpsc::unbounded_channel();
        tokio::task::spawn_blocking(move || CentreWorker::default().run(rx));
        Self { tx }
    }
}

impl Centre {
    pub async fn put(&self, queue: &QueueName, priority: Priority, payload: Payload) -> Result<ID> {
        let (tx, rx) = oneshot::channel();
        self.tx
            .send(Message::Put(queue.clone(), priority, payload, tx))?;
        rx.await.map_err(Into::into)
    }

    pub async fn try_get<'a, I: IntoIterator<Item = &'a QueueName>>(
        &self,
        queues: I,
    ) -> Result<Option<Job>> {
        let (tx, rx) = oneshot::channel();
        self.tx
            .send(Message::TryGet(queues.into_iter().cloned().collect(), tx))?;
        rx.await
            .map(|opt| opt.map(|parts| self.job_from(parts)))
            .map_err(Into::into)
    }

    pub async fn get<'a, I: IntoIterator<Item = &'a QueueName>>(&self, queues: I) -> Result<Job> {
        let (tx, mut rx) = mpsc::channel(1);
        self.tx
            .send(Message::Get(queues.into_iter().cloned().collect(), tx))?;
        if let Some(parts) = rx.recv().await {
            Ok(self.job_from(parts))
        } else {
            Err(Error::Closed)
        }
    }

    pub async fn delete(&self, id: ID) -> Result<bool> {
        let (tx, rx) = oneshot::channel();
        self.tx.send(Message::Delete(id, tx))?;
        rx.await.map_err(Into::into)
    }

    fn job_from(&self, parts: JobParts) -> Job {
        Job::new(parts, self.tx.clone())
    }
}

// ========================================== CentreWorker ==========================================

#[derive(Debug, Default)]
struct CentreWorker {
    queues: HashMap<QueueName, Queue>,
    jobs: HashMap<ID, (QueueName, Key)>,
    last_id: ID,
}

impl CentreWorker {
    fn run(mut self, mut rx: MessageReceiver) {
        while let Some(msg) = rx.blocking_recv() {
            use Message::*;
            match msg {
                Put(queue, priority, payload, tx) => {
                    _ = tx.send(self.put(queue, priority, payload))
                }
                TryGet(queues, tx) => _ = tx.send(self.try_get(&queues)),
                Get(queues, tx) => self.get(&queues, tx),
                Delete(id, tx) => _ = tx.send(self.delete(id)),
                Abort(parts, tx) => _ = tx.send(self.abort(parts)),
            }
        }
    }

    #[tracing::instrument(level=Level::TRACE, skip(self, payload), ret)]
    fn put(&mut self, queue: QueueName, priority: Priority, payload: Payload) -> ID {
        let id = self.last_id.next();
        let key = Key { id, priority };
        assert!(self.jobs.insert(id, (queue.clone(), key)).is_none());
        self.get_or_create_queue(queue).put(key, payload)
    }

    #[tracing::instrument(level=Level::TRACE, skip(self), ret)]
    fn try_get(&mut self, queues: &HashSet<QueueName>) -> Option<JobParts> {
        let (queue, key) = queues
            .iter()
            .filter_map(|name| self.queues.get(name))
            .filter_map(|queue| queue.try_get())
            .reduce(|a, b| if a.1 > b.1 { a } else { b })?;

        let payload = self
            .queues
            .get_mut(&queue)
            .expect("queue should still exist")
            .delete(&key)
            .expect("job should still be queued");

        Some((queue, key, payload).into())
    }

    #[tracing::instrument(level=Level::TRACE, skip(self, tx))]
    fn get(&mut self, queues: &HashSet<QueueName>, tx: mpsc::Sender<JobParts>) {
        if let Some(job) = self.try_get(queues) {
            tracing::trace!(id=%job.key.id, "found job");
            _ = tx.blocking_send(job);
            return;
        }
        for queue in queues.iter() {
            self.get_or_create_queue(queue.clone())
                .add_waiter(tx.clone());
        }
        tracing::trace!("enqueued waiter");
    }

    #[tracing::instrument(skip(self), ret)]
    fn abort(&mut self, parts: JobParts) -> bool {
        let (queue, key, payload) = parts.into();
        if !self.jobs.contains_key(&key.id) {
            return false;
        }
        self.queues
            .get_mut(&queue)
            .expect("queue should already exist")
            .put(key, payload);
        true
    }

    #[tracing::instrument(skip(self), ret)]
    fn delete(&mut self, id: ID) -> bool {
        let Some((queue, key)) = self.jobs.remove(&id) else {
            return false;
        };
        _ = self
            .queues
            .get_mut(&queue)
            .expect("queue should exist")
            .delete(&key);
        true
    }

    fn get_or_create_queue(&mut self, name: QueueName) -> &mut Queue {
        self.queues
            .entry(name.clone())
            .or_insert_with(|| Queue::new(name))
    }
}

// ========================================== Queue ==========================================

struct Queue {
    name: QueueName,
    jobs: BTreeMap<Key, Payload>,
    waiters: VecDeque<mpsc::Sender<JobParts>>,
}

impl Debug for Queue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Queue({}, #jobs={}, #waiters={})",
            self.name,
            self.jobs.len(),
            self.waiters.len()
        )
    }
}

impl Queue {
    fn new(name: QueueName) -> Self {
        tracing::debug!(?name, "creating new queue");
        Self {
            name,
            jobs: BTreeMap::new(),
            waiters: VecDeque::new(),
        }
    }

    fn put(&mut self, key: Key, payload: Payload) -> ID {
        let mut parts: JobParts = (self.name.clone(), key, payload).into();
        while let Some(waiter) = self.waiters.pop_front() {
            match waiter.blocking_send(parts) {
                Ok(_) => {
                    tracing::trace!("job dispatched to waiter");
                    return key.id;
                }
                Err(err) => parts = err.0,
            }
        }
        assert!(
            self.jobs.insert(key, parts.payload).is_none(),
            "job should not be in queue"
        );
        key.id
    }

    fn try_get(&self) -> Option<(QueueName, Key)> {
        self.jobs
            .last_key_value()
            .map(|(key, _)| (self.name.clone(), *key))
    }

    fn delete(&mut self, key: &Key) -> Option<Payload> {
        self.jobs.remove(key)
    }

    fn add_waiter(&mut self, waiter: mpsc::Sender<JobParts>) {
        assert!(
            self.jobs.is_empty(),
            "queue should be empty when adding waiters"
        );
        self.waiters.push_back(waiter);
    }
}

// ========================================== Job ==========================================

pub struct Job {
    inner: Option<(JobParts, MessageSender)>,
}

impl Job {
    fn new(parts: JobParts, tx: MessageSender) -> Self {
        Self {
            inner: Some((parts, tx)),
        }
    }

    pub fn as_tuple(&self) -> (ID, &QueueName, Priority, &Payload) {
        match &self.inner {
            Some((parts, _)) => parts.as_tuple(),
            _ => panic!("as_tuple called on aborted job"),
        }
    }

    pub fn id(&self) -> ID {
        match &self.inner {
            Some((parts, _)) => parts.id(),
            _ => panic!("as_tuple called on aborted job"),
        }
    }

    pub async fn abort(&mut self) -> Result<bool> {
        let (tx, rx) = oneshot::channel();
        self.release(tx);
        rx.await.map_err(Into::into)
    }

    fn release(&mut self, reply: oneshot::Sender<bool>) {
        match self.inner.take() {
            Some((parts, tx)) => {
                tracing::trace!(id = %parts.id(), "aborting job");
                _ = tx.send(Message::Abort(parts, reply));
            }
            None => _ = reply.send(false),
        }
    }
}

impl Drop for Job {
    fn drop(&mut self) {
        let (tx, _) = oneshot::channel();
        self.release(tx);
    }
}

impl Debug for Job {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.inner {
            Some((parts, _)) => parts.fmt(f),
            None => f.write_str("zombie-job"),
        }
    }
}

// ========================================== JonParts ==========================================

#[derive(PartialEq, Clone)]
struct JobParts {
    queue: QueueName,
    key: Key,
    payload: Payload,
}

impl JobParts {
    pub fn id(&self) -> ID {
        self.key.id
    }

    pub fn as_tuple(&self) -> (ID, &QueueName, Priority, &Payload) {
        (self.key.id, &self.queue, self.key.priority, &self.payload)
    }
}

impl Debug for JobParts {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Job({:?}, {:?}, {:?})",
            self.queue, self.key, self.payload
        )
    }
}

impl From<(QueueName, Key, Payload)> for JobParts {
    fn from(value: (QueueName, Key, Payload)) -> Self {
        let (queue, key, payload) = value;
        Self {
            queue,
            key,
            payload,
        }
    }
}

impl From<JobParts> for (QueueName, Key, Payload) {
    fn from(value: JobParts) -> Self {
        let JobParts {
            queue,
            key,
            payload,
        } = value;
        (queue, key, payload)
    }
}
