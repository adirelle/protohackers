use core::fmt;
use std::collections::HashSet;

use serde::{Deserialize, Serialize};

use crate::model::*;

// ========================================== Request ==========================================

#[derive(Debug, Deserialize, PartialEq)]
#[serde(tag = "request")]
pub enum Request {
    #[serde(rename = "put")]
    Put {
        queue: QueueName,
        #[serde(rename = "job")]
        payload: Payload,
        #[serde(rename = "pri")]
        priority: Priority,
    },

    #[serde(rename = "get")]
    Get {
        queues: HashSet<QueueName>,
        #[serde(default)]
        wait: bool,
    },

    #[serde(rename = "delete")]
    Delete {
        id: ID,
    },

    #[serde(rename = "abort")]
    Abort {
        id: ID,
    },

    Invalid {
        reason: String,
    },
}

impl fmt::Display for Request {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Request::*;
        match self {
            Put {
                queue,
                payload,
                priority,
            } => write!(f, "Put({queue}, {priority}, {payload})"),
            Get { queues, wait } => write!(f, "Get({queues:?}, {wait})"),
            Delete { id } => write!(f, "Delete({id})"),
            Abort { id } => write!(f, "Abort({id})"),
            Invalid { reason } => write!(f, "Invalid({reason})"),
        }
    }
}

// ========================================== Response ==========================================

#[derive(Debug, Serialize)]
#[serde(tag = "status")]
pub enum Response {
    #[serde(rename = "ok")]
    Ok,

    #[serde(rename = "no-job")]
    NoJob,

    #[serde(rename = "ok")]
    JobAdded { id: ID },

    #[serde(rename = "ok")]
    JobFound {
        id: ID,
        queue: QueueName,

        #[serde(rename = "pri")]
        priority: Priority,

        #[serde(rename = "job")]
        payload: Payload,
    },

    #[serde(rename = "error")]
    Error { reason: String },
}

impl fmt::Display for Response {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Response::*;
        match self {
            Ok => f.write_str("Ok"),
            NoJob => f.write_str("NoJob"),
            JobAdded { id } => write!(f, "JobAdded({id})"),
            JobFound {
                id,
                queue,
                priority,
                payload,
            } => write!(f, "JobFound({id},{queue},{priority},{payload})"),
            Error { reason } => write!(f, "Error({reason})"),
        }
    }
}

// ========================================== tests ==========================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_deserialize() {
        let id: ID = 12345.into();
        let priority: Priority = 123.into();
        let queue: QueueName = "queue1".into();
        let queue2: QueueName = "queue2".into();
        let queues: HashSet<QueueName> = [queue.clone(), queue2].into();
        let payload = Payload::default().put("foo", "bar");

        let test_cases = vec![
            (
                r#"{"request":"put","queue":"queue1","job":{"foo":"bar"},"pri":123}"#,
                Request::Put {
                    queue,
                    payload,
                    priority,
                },
            ),
            (
                r#"{"request":"get","queues":["queue1","queue2"]}"#,
                Request::Get {
                    queues: queues.clone(),
                    wait: false,
                },
            ),
            (
                r#"{"request":"get","queues":["queue1","queue2"],"wait":true}"#,
                Request::Get { queues, wait: true },
            ),
            (r#"{"request":"delete","id":12345}"#, Request::Delete { id }),
            (r#"{"request":"abort","id":12345}"#, Request::Abort { id }),
        ];

        for (request, expected) in test_cases.into_iter() {
            let actual = serde_json::from_str(request).unwrap();
            assert_eq!(expected, actual);
        }
    }

    #[test]
    fn test_serialize() {
        let id: ID = 12345.into();
        let priority: Priority = 123.into();
        let queue: QueueName = "queue1".into();
        let payload = Payload::default().put("foo", "bar").put("int", 5);

        let test_cases = vec![
            (Response::Ok, r#"{"status":"ok"}"#),
            (Response::NoJob, r#"{"status":"no-job"}"#),
            (Response::JobAdded { id }, r#"{"status":"ok","id":12345}"#),
            (
                Response::JobFound {
                    id,
                    priority,
                    queue,
                    payload,
                },
                r#"{"status":"ok","id":12345,"queue":"queue1","pri":123,"job":{"foo":"bar","int":5}}"#,
            ),
            (
                Response::Error {
                    reason: "no no".into(),
                },
                r#"{"status":"error","reason":"no no"}"#,
            ),
        ];

        for (response, expected) in test_cases.into_iter() {
            let actual = serde_json::to_string(&response).unwrap();
            assert_eq!(expected, actual);
        }
    }
}
