use serde_json::{from_slice, to_writer};
use tokio_util::bytes::{BufMut, BytesMut};
use tokio_util::codec::{Decoder, Encoder};

use super::messages::{Request, Response};
use crate::error::*;

const LF: u8 = 0x0A;

pub struct Codec;

impl Decoder for Codec {
    type Item = Request;

    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>> {
        let Some(start) = (0..src.len()).find(|i| !src[*i].is_ascii_whitespace()) else {
            return Ok(None);
        };
        let Some(end) = (start..src.len()).find(|i| src[*i] == LF) else {
            return Ok(None);
        };
        let frame = src.split_to(end);
        Ok(Some(from_slice::<Request>(&frame[start..]).unwrap_or_else(
            |err| Request::Invalid {
                reason: err.to_string(),
            },
        )))
    }
}

impl Encoder<Response> for Codec {
    type Error = Error;

    fn encode(&mut self, item: Response, dst: &mut BytesMut) -> Result<()> {
        to_writer(dst.writer(), &item)?;
        dst.put_u8(LF);
        Ok(())
    }
}
