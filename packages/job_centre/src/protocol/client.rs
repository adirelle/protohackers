use std::collections::HashSet;
use std::sync::Arc;

use dashmap::DashMap;
use futures::prelude::*;

use tokio::io::{AsyncRead, AsyncWrite};
use tokio_util::codec::Framed;
use tracing::trace_span;

use super::codec::Codec;
use super::messages::{Request, Response};

use crate::error::*;
use crate::model::*;

#[derive(Debug, Default)]
pub struct Client {
    jobs: DashMap<ID, Job>,
    centre: Arc<Centre>,
}

impl Client {
    pub fn new(centre: Arc<Centre>) -> Self {
        Self {
            centre,
            jobs: DashMap::new(),
        }
    }

    pub async fn handle<S: AsyncRead + AsyncWrite>(&self, conn: S) -> Result<()> {
        let (mut responses, mut requests) = Framed::new(conn, Codec).split();

        while let Some(maybe_request) = requests.next().await {
            use Request::*;
            let request = maybe_request?;
            let span = trace_span!("request", %request);
            let _guard = span.enter();
            let response = match request {
                Put {
                    queue,
                    priority,
                    payload,
                } => self.put(queue, priority, payload).await,
                Get { queues, wait: true } => self.get(queues).await,
                Get {
                    queues,
                    wait: false,
                } => self.try_get(queues).await,
                Abort { id } => self.abort(id).await,
                Delete { id } => self.delete(id).await,
                Invalid { reason } => {
                    tracing::debug!(error=%reason, "invalid request");
                    Ok(Response::Error { reason })
                }
            }?;
            responses.send(response).await?;
        }

        Ok(())
    }

    async fn put(
        &self,
        queue: QueueName,
        priority: Priority,
        payload: Payload,
    ) -> Result<Response> {
        Ok(Response::JobAdded {
            id: self.centre.put(&queue, priority, payload).await?,
        })
    }

    async fn try_get(&self, queues: HashSet<QueueName>) -> Result<Response> {
        let Some(job) = self.centre.try_get(queues.iter()).await? else {
            return Ok(Response::NoJob);
        };
        self.found_job(job).await
    }

    async fn get(&self, queues: HashSet<QueueName>) -> Result<Response> {
        self.found_job(self.centre.get(queues.iter()).await?).await
    }

    async fn found_job(&self, job: Job) -> Result<Response> {
        let id = job.id();
        let entry = self.jobs.entry(id).or_insert(job);
        let (id, queue, priority, payload) = entry.as_tuple();
        Ok(Response::JobFound {
            id,
            queue: queue.clone(),
            priority,
            payload: payload.clone(),
        })
    }

    async fn abort(&self, id: ID) -> Result<Response> {
        let Some((_, mut job)) = self.jobs.remove(&id) else {
            return Ok(Response::Error {
                reason: "job has not been asigned to you".into(),
            });
        };
        Ok(if job.abort().await? {
            Response::Ok
        } else {
            Response::NoJob
        })
    }

    async fn delete(&self, id: ID) -> Result<Response> {
        Ok(if self.centre.delete(id).await? {
            Response::Ok
        } else {
            Response::NoJob
        })
    }
}
