use std::{
    borrow::BorrowMut,
    fmt::Display,
    hash::{BuildHasher, Hasher},
    ops::{Add, AddAssign},
    sync::{Arc, Mutex, MutexGuard},
    time::Duration,
};

use dashmap::DashMap;
use metrics::{atomics::AtomicU64, *};
use tokio::time::Instant;

#[derive(Debug, Default, Clone)]
pub struct Stats(Arc<StatsImpl>);

impl Display for Stats {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Recorder for Stats {
    fn describe_counter(&self, key: KeyName, unit: Option<Unit>, description: SharedString) {
        self.0.describe_counter(key, unit, description)
    }

    fn describe_gauge(&self, key: KeyName, unit: Option<Unit>, description: SharedString) {
        self.0.describe_gauge(key, unit, description)
    }

    fn describe_histogram(&self, key: KeyName, unit: Option<Unit>, description: SharedString) {
        self.0.describe_histogram(key, unit, description)
    }

    fn register_counter(&self, key: &Key) -> Counter {
        self.0.register_counter(key)
    }

    fn register_gauge(&self, key: &Key) -> Gauge {
        self.0.register_gauge(key)
    }

    fn register_histogram(&self, key: &Key) -> Histogram {
        self.0.register_histogram(key)
    }
}

#[derive(Debug, Default)]
struct StatsImpl {
    metadata: DashMap<KeyName, Metadata>,
    counters: DashMap<Key, Arc<AtomicU64>>,
    gauges: DashMap<Key, Arc<AtomicU64>>,
    histograms: DashMap<Key, Arc<HistogramRecorder>>,
}

impl Drop for StatsImpl {
    fn drop(&mut self) {
        tracing::debug!("stats:\n{}", self);
    }
}

impl Recorder for StatsImpl {
    fn describe_counter(&self, key: KeyName, unit: Option<Unit>, description: SharedString) {
        let mut d = self.metadata.entry(key).or_default();
        d.unit = unit;
        d.description = description;
    }

    fn describe_gauge(&self, key: KeyName, unit: Option<Unit>, description: SharedString) {
        let mut d = self.metadata.entry(key).or_default();
        d.unit = unit;
        d.description = description;
    }

    fn describe_histogram(&self, key: KeyName, unit: Option<Unit>, description: SharedString) {
        let mut d = self.metadata.entry(key).or_default();
        d.unit = unit;
        d.description = description;
    }

    fn register_counter(&self, key: &Key) -> Counter {
        self.counters.entry(key.clone()).or_default().clone().into()
    }

    fn register_gauge(&self, key: &Key) -> Gauge {
        self.gauges.entry(key.clone()).or_default().clone().into()
    }

    fn register_histogram(&self, key: &Key) -> Histogram {
        self.histograms
            .entry(key.clone())
            .or_default()
            .clone()
            .into()
    }
}

impl Display for StatsImpl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.counters.is_empty() && self.gauges.is_empty() && self.histograms.is_empty() {
            return f.write_str("no data yet");
        }
        for counter in self.counters.iter() {
            write!(
                f,
                "\n{}: {}",
                counter.key(),
                counter.value().load(std::sync::atomic::Ordering::Relaxed)
            )?;
        }
        for gauge in self.gauges.iter() {
            write!(
                f,
                "\n{}: {}",
                gauge.key(),
                f64::from_bits(gauge.value().load(std::sync::atomic::Ordering::Relaxed))
            )?;
        }
        for histogram in self.histograms.iter() {
            write!(f, "\n{}: {}", histogram.key(), histogram.value())?;
        }

        Ok(())
    }
}

#[derive(Debug, Default)]
struct Metadata {
    unit: Option<Unit>,
    description: SharedString,
}

#[derive(Debug, Default)]
struct HistogramRecorder(Mutex<HistogramData>);

impl HistogramRecorder {
    #[inline]
    fn inner(&self) -> MutexGuard<'_, HistogramData> {
        self.0.lock().expect("poisoned histogram")
    }
}

impl HistogramFn for HistogramRecorder {
    fn record(&self, value: f64) {
        *self.inner() += value;
    }
}

impl Display for HistogramRecorder {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.inner())
    }
}

#[derive(Debug, Default, Clone, Copy)]
enum HistogramData {
    Value {
        min: f64,
        max: f64,
        sum: f64,
        count: u32,
    },
    #[default]
    Empty,
}

impl Display for HistogramData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use HistogramData::*;
        match self {
            Value {
                min,
                max,
                sum,
                count,
            } => write!(
                f,
                "{} value{}, total: {}, avg: {}, min: {}, max: {}",
                count,
                if count > &1 { "s" } else { "" },
                sum,
                sum / f64::from(*count),
                min,
                max,
            ),
            Empty => f.write_str("no values"),
        }
    }
}

impl Add<f64> for HistogramData {
    type Output = HistogramData;

    fn add(self, value: f64) -> Self::Output {
        use HistogramData::*;
        match self {
            Value {
                min,
                max,
                sum,
                count,
            } => Value {
                min: if value < min { value } else { min },
                max: if value > max { value } else { max },
                sum: sum + value,
                count: count + 1,
            },
            Empty => Value {
                min: value,
                max: value,
                sum: value,
                count: 1,
            },
        }
    }
}

impl AddAssign<f64> for HistogramData {
    fn add_assign(&mut self, value: f64) {
        use HistogramData::*;
        if let Value {
            ref mut min,
            ref mut max,
            ref mut sum,
            ref mut count,
        } = self
        {
            if value < *min {
                *min = value;
            }
            if value > *max {
                *max = value;
            }
            *sum += value;
            *count += 1;
        } else {
            *self = Value {
                min: value,
                max: value,
                sum: value,
                count: 1,
            };
        }
    }
}

pub struct Size(usize);

impl From<usize> for Size {
    fn from(value: usize) -> Self {
        Self(value)
    }
}

impl<T> From<&[T]> for Size {
    fn from(value: &[T]) -> Self {
        value.len().into()
    }
}

impl<'a, K: 'a + Eq + std::hash::Hash, V: 'a, S: BuildHasher + Clone> From<&DashMap<K, V, S>>
    for Size
{
    fn from(value: &DashMap<K, V, S>) -> Self {
        value.len().into()
    }
}

impl IntoF64 for Size {
    fn into_f64(self) -> f64 {
        u32::try_from(self.0).map(Into::into).unwrap_or(f64::NAN)
    }
}

#[derive(Debug)]
pub struct Timing {
    key: KeyName,
    started: Instant,
}

impl Timing {
    pub fn start<K: Into<KeyName>>(key: K) -> Self {
        Self {
            key: key.into(),
            started: Instant::now(),
        }
    }
}

impl Drop for Timing {
    fn drop(&mut self) {
        histogram!(self.key.clone(), Instant::now() - self.started)
    }
}

#[derive(Debug)]
pub struct Tracker {
    current_key: KeyName,
    #[allow(dead_code)]
    lifetime: Timing,
}

impl Tracker {
    pub fn new<K: std::borrow::Borrow<str>>(key: K) -> Self {
        let k = key.borrow();
        let current_key: KeyName = format!("{}_current", k).into();
        increment_gauge!(current_key.clone(), 1.0);
        increment_counter!(format!("{}_total", k));
        Tracker {
            current_key,
            lifetime: Timing::start(format!("{}_duration", k)),
        }
    }
}

impl Drop for Tracker {
    fn drop(&mut self) {
        decrement_gauge!(self.current_key.clone(), 1.0);
    }
}
