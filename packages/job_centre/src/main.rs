mod error;
mod model;
mod protocol;

use std::net::SocketAddr;
use std::sync::Arc;

use futures::future::select;
use tokio::net::{TcpListener, TcpStream};
use tokio::signal::unix::{signal, SignalKind};
use tokio::task::JoinSet;
use tracing::Level;
use tracing_subscriber::EnvFilter;

use error::*;
use model::Centre;
use protocol::Client;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(Level::DEBUG)
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    let centre: Arc<Centre> = Default::default();

    let listener = TcpListener::bind("0.0.0.0:20000").await?;
    tracing::info!(socket = %listener.local_addr()?, "listening");

    let mut tasks = JoinSet::new();

    let mut interrupt = signal(SignalKind::interrupt())?;
    let mut terminate = signal(SignalKind::terminate())?;
    let close = select(Box::pin(interrupt.recv()), Box::pin(terminate.recv()));
    tokio::pin!(close);

    loop {
        tokio::select! {
            _ = tasks.join_next() => {},
            Ok((socket, addr)) = listener.accept() => {
                tasks.spawn(serve_client(centre.clone(), socket, addr));
            },
            _ = &mut close => {
                tracing::info!("stopping on signal");
                break
            },
        }
    }

    tasks.shutdown().await;

    Ok(())
}

#[tracing::instrument(skip(centre, socket))]
async fn serve_client(centre: Arc<Centre>, socket: TcpStream, addr: SocketAddr) {
    tracing::info!("client connected");
    match Client::new(centre).handle(socket).await {
        Ok(_) => tracing::info!("client disconnected"),
        Err(err) => tracing::warn!(error = %err, "client error"),
    }
}
