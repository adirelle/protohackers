use std::io::Cursor;
use std::net::SocketAddr;

use futures::{SinkExt, StreamExt};
use tokio::io::{AsyncBufReadExt, BufStream};
use tokio::net::{TcpListener, TcpStream};
use tokio::task::JoinSet;
use tokio_util::codec::Framed;
use tracing::Level;
use tracing_subscriber::fmt::format::FmtSpan;

use self::app::Codec;
use self::cipher::{Op, Stream};

mod app;
mod cipher;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_max_level(Level::DEBUG)
        .init();

    let listener = TcpListener::bind("0.0.0.0:20000").await?;
    let mut clients = JoinSet::<Result<&'static str, Error>>::new();

    tracing::info!(socket=?listener.local_addr(), "ready to serve clients");

    loop {
        tokio::select! {
            Ok((socket, addr)) = listener.accept() => {
                clients.spawn(client(socket, addr));
            }
            Some(result) = clients.join_next() => {
                if let Err(err) = result {
                    tracing::warn!(%err, "connection aborted");
                }
            }
            else => {
                break Ok(());
            }
        }
    }
}

#[tracing::instrument(level = "info", skip(socket), err(level = "warn"))]
async fn client(socket: TcpStream, remote: SocketAddr) -> Result<&'static str, Error> {
    let mut stream = BufStream::new(socket);

    let cipher = read_cipher(&mut stream).await?;
    tracing::debug!(?cipher, "applying");

    let mut framed = Framed::new(Stream::new(stream, cipher), Codec);

    while let Some(request) = framed.next().await.transpose()? {
        tracing::trace!(?request, "received");
        if let Some(response) = request.into_iter().next_back() {
            tracing::trace!(?response, "sending");
            framed.send(response).await?;
        }
    }

    Ok("client closed connection")
}

async fn read_cipher<R: AsyncBufReadExt + Unpin>(mut src: R) -> Result<Vec<Op>, Error> {
    let (len, cipher) = loop {
        let buf = src.fill_buf().await?;
        let mut cursor = Cursor::new(buf);
        match Op::decode_vec(&mut cursor) {
            Ok(cipher) => break (cursor.position(), cipher),
            Err(cipher::Error::Unterminated(_)) => (),
            Err(err) => return Err(err.into()),
        }
    };
    src.consume(len as usize);
    Ok(cipher)
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("i/o: {0}")]
    IO(#[from] std::io::Error),

    #[error("cipher: {0}")]
    Cipher(#[from] cipher::Error),

    #[error("codec: {0}")]
    Codec(#[from] app::Error),
}
