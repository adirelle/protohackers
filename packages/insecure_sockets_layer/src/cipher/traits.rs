use std::sync::Arc;

// ======================================== Cipher ========================================

pub trait Cipher {
    fn cipher(&self, data: u8, pos: u8) -> u8;

    fn validate(&self) -> bool {
        (0..=255).any(|input| self.cipher(input, 179) != input)
    }
}

impl<C: Cipher> Cipher for Vec<C> {
    #[inline]
    fn cipher(&self, data: u8, pos: u8) -> u8 {
        self.iter().fold(data, |data, op| op.cipher(data, pos))
    }
}

impl<C: Cipher> Cipher for Arc<C> {
    #[inline]
    fn cipher(&self, data: u8, pos: u8) -> u8 {
        self.as_ref().cipher(data, pos)
    }
}

impl<C: Cipher> Cipher for Box<C> {
    #[inline]
    fn cipher(&self, data: u8, pos: u8) -> u8 {
        self.as_ref().cipher(data, pos)
    }
}

// ======================================== Decipher ========================================

pub trait Decipher {
    fn decipher(&self, data: u8, pos: u8) -> u8;
}

impl<C: Decipher> Decipher for Vec<C> {
    fn decipher(&self, data: u8, pos: u8) -> u8 {
        self.iter()
            .rev()
            .fold(data, |data, op| op.decipher(data, pos))
    }
}

impl<C: Decipher> Decipher for Arc<C> {
    #[inline]
    fn decipher(&self, data: u8, pos: u8) -> u8 {
        self.as_ref().decipher(data, pos)
    }
}

impl<C: Decipher> Decipher for Box<C> {
    #[inline]
    fn decipher(&self, data: u8, pos: u8) -> u8 {
        self.as_ref().decipher(data, pos)
    }
}
