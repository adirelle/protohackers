use bytes::Buf;

use super::{Cipher, Decipher};

// ======================================== Op ========================================

#[derive(Debug)]
pub enum Op {
    ReverseBits,
    XorConst(u8),
    XorPos,
    AddConst(u8),
    AddPos,
}

impl Op {
    pub fn decode_vec<B>(mut src: B) -> Result<Vec<Self>, Error>
    where
        B: Buf,
    {
        let mut cipher = Vec::new();
        let mut pos = 0_usize;
        while src.has_remaining() {
            use Op::*;
            let op = match src.get_u8() {
                0x00 => {
                    return if cipher.validate() {
                        Ok(cipher)
                    } else {
                        Err(Error::NoOp(cipher))
                    }
                }
                0x01 => ReverseBits,
                0x02 => {
                    if src.has_remaining() {
                        pos += 1;
                        XorConst(src.get_u8())
                    } else {
                        return Err(Error::Unterminated(pos));
                    }
                }
                0x03 => XorPos,
                0x04 => {
                    if src.has_remaining() {
                        pos += 1;
                        AddConst(src.get_u8())
                    } else {
                        return Err(Error::Unterminated(pos));
                    }
                }
                0x05 => AddPos,
                other => return Err(Error::Invalid(other)),
            };
            pos += 1;
            cipher.push(op);
        }
        Err(Error::Unterminated(pos))
    }

    fn reverse_bits(mut input: u8) -> u8 {
        let mut output = 0;
        for _ in 0..8 {
            output <<= 1;
            output |= input & 0x01;
            input >>= 1;
        }
        output
    }
}

impl Cipher for Op {
    fn cipher(&self, data: u8, pos: u8) -> u8 {
        use Op::*;
        match self {
            ReverseBits => Op::reverse_bits(data),
            XorConst(c) => data ^ c,
            XorPos => data ^ pos,
            AddConst(c) => data.wrapping_add(*c),
            AddPos => data.wrapping_add(pos),
        }
    }
}

impl Decipher for Op {
    fn decipher(&self, data: u8, pos: u8) -> u8 {
        use Op::*;
        match self {
            ReverseBits => Op::reverse_bits(data),
            XorConst(c) => data ^ c,
            XorPos => data ^ pos,
            AddConst(c) => data.wrapping_sub(*c),
            AddPos => data.wrapping_sub(pos),
        }
    }
}

// ======================================== Error ========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("invalid cipher operation code: {0:#x}")]
    Invalid(u8),

    #[error("unterminated cipher at pos {0}")]
    Unterminated(usize),

    #[error("no-op cipher: {0:?}")]
    NoOp(Vec<Op>),
}

impl Error {
    pub fn is_unterminated(&self) -> bool {
        matches!(self, Self::Unterminated(_))
    }
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! test_op {
        ( $( $test_name:ident : $op:expr , $clear:expr => $ciphered:expr ; )*  ) => {
            $(
                #[test]
                fn $test_name() {
                    let clear: u8 = $clear;
                    let ciphered: u8 = $ciphered;
                    let pos: u8 = 179;

                    let cipher_result = $op.cipher(clear, pos);
                    assert_eq!(
                        ciphered, cipher_result,
                        "expected ciphered {ciphered:b}, got {cipher_result:b}"
                    );

                    let decipher_result = $op.decipher(ciphered, pos);
                    assert_eq!(
                        clear, decipher_result,
                        "expected deciphered {clear:b}, got {decipher_result:b}"
                    );
                }
            )*
        };
    }

    test_op! {
        reverse_bits: Op::ReverseBits, 0b11010101 => 0b10101011 ;
        xor_const: Op::XorConst(0b10101010), 0b11010101 => 0b01111111 ;
        xor_pos: Op::XorPos, 0b11010101 => 0b01100110 ;
        add_const: Op::AddConst(0b01000000), 0b11010101 => 0b00010101 ;
        add_pos: Op::AddPos, 0b11010101 => 0b10001000 ;
        vec: vec![Op::XorConst(1), Op::ReverseBits], 0b11010101 => 0b00101011 ;
    }

    macro_rules! test_decode_vec {
        ( $( $name:ident: $bytes:expr => $expected:pat ),* ) => {
            $(
                #[test]
                fn $name() {
                    let cipher = Op::decode_vec(&$bytes[..]).expect("should success");
                    assert!(matches!(&cipher[..], $expected));
                }
            )*
        }
    }

    test_decode_vec! {
        exemple_1: [0x02, 0x01, 0x01, 0x00] => [Op::XorConst(1), Op::ReverseBits]
    }
}
