use std::collections::VecDeque;
use std::io::{self, Read, Write};
use std::ops::{Add, AddAssign, Deref};
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

use bytes::Buf;
use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};

use super::{Cipher, Decipher};

// ======================================== Stream ========================================

pub struct Stream<T, C> {
    inner: T,
    cipher: Arc<C>,
    read: Position,
    write: CipherBuffer<Arc<C>>,
}

impl<T, C> Stream<T, C> {
    pub fn new(inner: T, cipher: C) -> Self {
        let cipher = Arc::new(cipher);
        Self {
            inner,
            cipher: cipher.clone(),
            read: Position::default(),
            write: CipherBuffer::new(cipher),
        }
    }
}

impl<T, C> Stream<T, C>
where
    C: Decipher,
{
    fn decipher_slice_mut(&mut self, buf: &mut [u8]) {
        buf.iter_mut().for_each(|b| {
            *b = self.cipher.decipher(*b, *self.read);
            self.read += 1_u8;
        });
    }
}

impl<T, C> Read for Stream<T, C>
where
    T: Read,
    C: Decipher,
{
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let n = self.inner.read(buf)?;
        self.decipher_slice_mut(&mut buf[..n]);
        Ok(n)
    }
}

impl<T, C> AsyncRead for Stream<T, C>
where
    T: AsyncRead + Unpin,
    C: Decipher + Unpin,
{
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<io::Result<()>> {
        let this = self.get_mut();

        futures::ready!(Pin::new(&mut this.inner).poll_read(cx, buf))?;
        this.decipher_slice_mut(buf.filled_mut());
        Poll::Ready(Ok(()))
    }
}

impl<T, C> Stream<T, C>
where
    T: Write,
    C: Cipher,
{
    fn flush_buffer(&mut self) -> io::Result<()> {
        while self.write.has_remaining() {
            let written = self.inner.write(self.write.chunk())?;
            if written == 0 {
                return Err(io::ErrorKind::WriteZero.into());
            }
            self.write.advance(written);
        }
        Ok(())
    }
}

impl<T, C> Write for Stream<T, C>
where
    T: Write,
    C: Cipher,
{
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if self.write.is_full() {
            self.flush_buffer()?;
        }
        let n = self.write.write(buf)?;
        self.flush_buffer()?;
        Ok(n)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.flush_buffer()?;
        self.inner.flush()
    }
}

impl<T, C> Stream<T, C>
where
    T: AsyncWrite + Unpin,
    C: Cipher,
{
    fn poll_flush_buffer(&mut self, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        if !self.write.is_empty() {
            let chunk = self.write.chunk();
            let n = futures::ready!(Pin::new(&mut self.inner).poll_write(cx, chunk))?;
            if n > 0 {
                self.write.advance(n);
            } else {
                return Poll::Ready(Err(io::ErrorKind::WriteZero.into()));
            }
        }
        Poll::Ready(Ok(()))
    }
}

impl<T, C> AsyncWrite for Stream<T, C>
where
    T: AsyncWrite + Unpin,
    C: Cipher,
{
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, io::Error>> {
        let this = self.get_mut();
        if this.write.is_full() {
            futures::ready!(this.poll_flush_buffer(cx))?;
        }
        Poll::Ready(this.write.write(buf))
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), io::Error>> {
        let this = self.get_mut();
        while !this.write.is_empty() {
            futures::ready!(this.poll_flush_buffer(cx))?;
        }
        Pin::new(&mut this.inner).poll_flush(cx)
    }

    fn poll_shutdown(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), io::Error>> {
        let this = self.get_mut();
        while !this.write.is_empty() {
            futures::ready!(this.poll_flush_buffer(cx))?;
        }
        Pin::new(&mut this.inner).poll_shutdown(cx)
    }
}

// ======================================== CipherBuffer ========================================

struct CipherBuffer<C> {
    buf: VecDeque<u8>,
    pos: Position,
    cipher: C,
}

impl<C> CipherBuffer<C> {
    pub const DEFAULT_CAPACITY: usize = 256;

    pub fn new(cipher: C) -> Self {
        Self::with_capacity(cipher, Self::DEFAULT_CAPACITY)
    }

    pub fn with_capacity(cipher: C, capacity: usize) -> Self {
        Self {
            cipher,
            buf: VecDeque::with_capacity(capacity),
            pos: Position::default(),
        }
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.buf.len()
    }

    #[inline]
    pub fn is_full(&self) -> bool {
        self.available() == 0
    }

    #[inline]
    pub fn available(&self) -> usize {
        self.buf.capacity() - self.buf.len()
    }
}

impl<C> Write for CipherBuffer<C>
where
    C: Cipher,
{
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let n = std::cmp::min(buf.len(), self.available());
        if n > 0 {
            self.buf.extend(buf.iter().take(n).map(|src| {
                let out = self.cipher.cipher(*src, *self.pos);
                self.pos += 1_u8;
                out
            }));
        }
        Ok(n)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl<C> Buf for CipherBuffer<C> {
    fn remaining(&self) -> usize {
        self.len()
    }

    fn chunk(&self) -> &[u8] {
        self.buf.as_slices().0
    }

    fn advance(&mut self, cnt: usize) {
        assert!(cnt <= self.len());
        self.buf.drain(..cnt);
    }
}

// ======================================== Position ========================================

#[derive(Debug, Default, Clone, Copy)]
pub struct Position(u8);

impl From<usize> for Position {
    #[inline]
    fn from(value: usize) -> Self {
        Self((value & 0xFF) as u8)
    }
}

impl From<u8> for Position {
    #[inline]
    fn from(value: u8) -> Self {
        Self(value)
    }
}

impl Deref for Position {
    type Target = u8;

    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: Into<Position>> Add<T> for Position {
    type Output = Self;

    #[inline]
    fn add(self, rhs: T) -> Self::Output {
        Self(self.0.wrapping_add(rhs.into().0))
    }
}

impl<T: Into<Position>> AddAssign<T> for Position {
    #[inline]
    fn add_assign(&mut self, rhs: T) {
        self.0 = self.0.wrapping_add(rhs.into().0);
    }
}

// ======================================== Error ========================================

#[cfg(test)]
mod tests {
    use super::*;
    struct TestCipher;

    impl Cipher for TestCipher {
        fn cipher(&self, data: u8, pos: u8) -> u8 {
            data.wrapping_add(pos)
        }
    }

    impl Decipher for TestCipher {
        fn decipher(&self, data: u8, pos: u8) -> u8 {
            data.wrapping_sub(pos)
        }
    }

    #[test]
    fn test_read() {
        let src = [1_u8, 2, 3, 4, 5, 6];
        let mut stream = Stream::new(&src[..], TestCipher);

        let mut dst = [0_u8; 4];
        let mut n = io::Read::read(&mut stream, &mut dst[..3]).expect("should success");

        let n2 = io::Read::read(&mut stream, &mut dst[n..]).expect("should success");
        n += n2;

        assert_eq!(&[1_u8, 1, 1, 1][..], &dst[..n]);
    }

    #[tokio::test]
    async fn test_async_read() {
        use tokio::io::AsyncReadExt;

        let src = &[1_u8, 2, 3, 4, 5, 6][..];
        tokio::pin! { let stream = Stream::new(src, TestCipher); }

        let mut dst = [0_u8; 4];
        let mut n = stream.read(&mut dst[..3]).await.expect("should success");

        let n2 = stream.read(&mut dst[n..]).await.expect("should success");
        n += n2;

        assert_eq!(&[1_u8, 1, 1, 1][..], &dst[..n]);
    }

    #[test]
    fn test_write() {
        let mut dst = [0; 10];
        let mut stream = Stream::new(&mut dst[..], TestCipher);

        let src = [0_u8, 1, 2, 3, 4, 5, 6];
        let mut n = stream.write(&src[..3]).expect("should success");
        assert_eq!(n, 3);

        let n2 = stream.write(&src[n..]).expect("should success");
        assert_eq!(n2, 4);
        n += n2;
        assert_eq!(n, 7);

        stream.flush().expect("should success");

        assert_eq!(&[0_u8, 2, 4, 6, 8, 10, 12][..], &dst[..n]);
    }

    #[tokio::test]
    async fn test_async_write() {
        use tokio::io::AsyncWriteExt;

        let mut dst = Vec::<u8>::new();
        tokio::pin! {
            let stream = Stream::new(&mut dst, TestCipher);
        };

        let src = [0_u8, 1, 2, 3, 4, 5, 6];
        let mut n = stream.write(&src[..3]).await.expect("should success");
        assert_eq!(n, 3);

        let n2 = stream.write(&src[n..]).await.expect("should success");
        assert_eq!(n2, 4);
        n += n2;
        assert_eq!(n, 7);

        stream.flush().await.expect("should success");

        assert_eq!(&[0_u8, 2, 4, 6, 8, 10, 12][..], &dst[..]);
    }
}
