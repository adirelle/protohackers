#[derive(Debug, Eq, PartialEq, PartialOrd, Ord)]
pub struct Order {
    pub count: u32,
    pub toy: ToyName,
}

pub type ToyName = String;
