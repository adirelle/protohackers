use std::collections::BTreeSet;
use std::fmt::Write;
use std::num::ParseIntError;
use std::str::{from_utf8, Utf8Error};

use bytes::{Buf, Bytes, BytesMut};
use tokio_util::codec::{Decoder, Encoder};

use super::model::Order;

type Request = BTreeSet<Order>;

type Response = Order;

pub struct Codec;

impl Codec {
    fn decode_inner(&mut self, src: Bytes) -> Result<Request, Error> {
        from_utf8(src.as_ref())?
            .split(|c| ',' == c)
            .map(|order| {
                let parts = order.splitn(2, |c| c == 'x').collect::<Vec<_>>();
                let [count, toy] = &parts[..] else {
                    return Err(Error::InvalidOrder);
                };
                Ok(Order {
                    count: count.parse::<u32>()?,
                    toy: toy.trim().to_owned(),
                })
            })
            .collect::<Result<_, _>>()
    }
}

impl Decoder for Codec {
    type Item = Request;

    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        let Some(eol) = src.iter().position(|b| b'\n'.eq(b)) else {
            return Ok(None);
        };
        let line = src.split_to(eol).freeze();
        src.advance(1);
        match self.decode_inner(line.clone()) {
            Ok(request) => Ok(Some(request)),
            Err(err) => {
                tracing::warn!(err=%err, line=%line.as_ref().escape_ascii(), "invalid request");
                Err(err)
            }
        }
    }
}

impl Encoder<Response> for Codec {
    type Error = Error;

    fn encode(&mut self, item: Response, dst: &mut BytesMut) -> Result<(), Self::Error> {
        writeln!(dst, "{}x {}", item.count, item.toy).map_err(Into::into)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    IO(#[from] std::io::Error),

    #[error(transparent)]
    Fmt(#[from] std::fmt::Error),

    #[error("invalid order")]
    InvalidOrder,

    #[error("invalid UTF-8: {0}")]
    InvalidUtf8(#[from] Utf8Error),

    #[error("invalid number: {0}")]
    InvalidNumber(#[from] ParseIntError),
}
