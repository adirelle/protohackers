pub mod prelude;

use std::fmt::Debug;
use std::net::SocketAddr;
use std::ops::AddAssign;

use prelude::*;
use tokio::net::{TcpListener, TcpStream};
use tokio::select;
use tokio::signal::unix::{signal, SignalKind};
use tokio::task::JoinSet;
use tracing::{Instrument, Level};

pub trait TCPHandlerFactory {
    type Handler;
    type Stats;

    fn create(&mut self, socket: TcpStream, client_addr: SocketAddr) -> Result<Self::Handler>;
}

#[async_trait]
pub trait TCPHandler<S>: Send + 'static {
    async fn handle(mut self, until: CancellationToken) -> Result<S>;
}

#[derive(Debug)]
enum Event<S> {
    SigInt,
    SigUSR1,
    SigTerm,
    ConnectionOpened(SocketAddr),
    ConnectionClosed(S),
    ConnectionFailed(anyhow::Error),
}

#[tracing::instrument(skip(factory), fields(pid = std::process::id()))]
pub async fn serve<F, S>(mut factory: F, listen_addr: SocketAddr) -> Result<()>
where
    F: TCPHandlerFactory,
    F::Handler: TCPHandler<S>,
    S: AddAssign + Debug + Send + Default + Clone + 'static,
{
    let listener = TcpListener::bind(listen_addr).await?;
    let mut sigint = signal(SignalKind::interrupt())?;
    let mut sigterm = signal(SignalKind::terminate())?;
    let mut sigusr1 = signal(SignalKind::user_defined1())?;
    let until = CancellationToken::new();

    let mut stats = S::default();
    let mut connections = JoinSet::<Result<S>>::new();
    let mut connection_count = 0_usize;

    tracing::info!("ready to serve");

    while !until.is_cancelled() || !connections.is_empty() {
        let event = select! {
            Some(result) = connections.join_next() => {
                match result? {
                    Ok(connection_stats) => {
                        stats += connection_stats.clone();
                        Event::ConnectionClosed(connection_stats)
                    },
                    Err(err) => Event::ConnectionFailed(err),
                }
            },
            _ = sigint.recv(), if !until.is_cancelled() => {
                until.cancel();
                Event::SigInt
            },
            _ = sigterm.recv(), if !until.is_cancelled() => {
                until.cancel();
                Event::SigTerm
            },
            _ = sigusr1.recv() => Event::SigUSR1,
            Ok((socket, client_addr)) = listener.accept(), if !until.is_cancelled() => {
                connection_count += 1;
                let span = tracing::span!(Level::INFO, "connection", id = connection_count, client = %client_addr);
                let connection_until = until.clone();
                let handler = factory.create(socket, client_addr)?;
                connections.spawn(async move {
                    handler.handle(connection_until).await
                }.instrument(span));
                Event::ConnectionOpened(client_addr)
            },
        };

        tracing::info!(
            event = debug(event),
            connection_count = connection_count,
            open_connections = connections.len(),
            stats = debug(&stats),
        );
    }

    Ok(())
}
