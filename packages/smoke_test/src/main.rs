use std::ops::AddAssign;

use bytes::BytesMut;
use server::prelude::*;
use tokio::select;

#[tokio::main]
async fn main() -> Result<()> {
    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new())?;

    server::serve(Server, "0.0.0.0:20000".parse()?).await
}

#[derive(Debug, Default, Clone)]
struct Stats {
    message_count: usize,
    bytes_read: usize,
}

impl AddAssign for Stats {
    fn add_assign(&mut self, rhs: Self) {
        self.message_count += rhs.message_count;
        self.bytes_read += rhs.bytes_read;
    }
}

#[derive(Debug)]
struct Server;

impl TCPHandlerFactory for Server {
    type Handler = Connection;
    type Stats = Stats;

    fn create(
        &mut self,
        socket: TcpStream,
        _client_addr: std::net::SocketAddr,
    ) -> Result<Self::Handler> {
        Ok(Connection { socket })
    }
}

#[derive(Debug)]
struct Connection {
    socket: TcpStream,
}

#[async_trait]
impl TCPHandler<Stats> for Connection {
    async fn handle(mut self, until: CancellationToken) -> Result<Stats> {
        let mut buf = BytesMut::with_capacity(4096);
        let mut stats = Stats::default();

        while !until.is_cancelled() {
            select! {
                result = self.socket.read_buf(&mut buf) => {
                    match result {
                        Ok(0) => break,
                        Ok(size) => {
                            stats.bytes_read += size;
                            stats.message_count += 1;

                            self.socket.write_all(&buf).await?;
                            self.socket.flush().await?;
                            buf.clear();
                        }
                        Err(err) => return Err(err.into()),
                    }
                },
                _ = until.cancelled() => self.socket.shutdown().await?
            }
        }

        Ok(stats)
    }
}
