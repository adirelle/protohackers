use std::{convert::Infallible, io::Write, time::Duration};

use bytes::{Buf, BufMut, Bytes, BytesMut};
use futures::{Sink, SinkExt, Stream, StreamExt};
use tokio::sync::mpsc::{error::SendError, *};

use crate::{
    messages::{self, *},
    timer::Timer,
};

// ========================================== Session ==========================================

pub struct Session;

impl Session {
    #[tracing::instrument(level = "debug", skip_all, err(level = "warn"))]
    pub async fn run<I, O, P>(
        input: I,
        output: O,
        processor: P,
        session_timeout: Duration,
        ack_timeout: Duration,
    ) -> Result<(), Error>
    where
        I: Stream<Item = Payload> + Unpin + Send + 'static,
        O: Sink<Payload> + Unpin + Send + 'static,
        Error: From<O::Error>,
        P: Processor + Send + 'static,
    {
        let (ack_tx, ack_rx) = unbounded_channel();
        let (data_tx, data_rx) = unbounded_channel();
        let (collect_tx, collect_rx) = unbounded_channel();
        let (processed_tx, processed_rx) = unbounded_channel();

        let dispatcher = Self::dispatch(input, ack_tx, data_tx, session_timeout);
        let collector = Self::collect(collect_rx, output);
        let receiver = Self::receive(
            data_rx,
            processed_tx,
            processor,
            collect_tx.clone(),
            ack_timeout / 1000,
        );
        let sender = Self::send(processed_rx, ack_rx, collect_tx, ack_timeout);

        tokio::try_join!(dispatcher, collector, receiver, sender).map(
            |(dispatcher, collector, receiver, sender)| {
                tracing::debug!(
                    ?dispatcher,
                    ?collector,
                    ?receiver,
                    ?sender,
                    "session tasks ended"
                );
            },
        )
    }

    async fn dispatch<I>(
        mut input: I,
        ack_tx: UnboundedSender<usize>,
        data_tx: UnboundedSender<(usize, Data)>,
        timeout: Duration,
    ) -> Result<&'static str, Error>
    where
        I: Stream<Item = Payload> + Unpin,
    {
        let mut timer = Box::pin(Timer::new(timeout));

        loop {
            tokio::select! {
                item = input.next() => {
                    let Some(payload) = item else {
                        return Err(Error::ClosedUnexpectedly);
                    };
                    tracing::trace!(?payload, "received");
                    timer.as_mut().restart();
                    match payload {
                        Payload::Connect => data_tx.send((0, Data::default()))?,
                        Payload::Ack(length) => ack_tx.send(length.into())?,
                        Payload::Data(pos, data) => data_tx.send((pos.into(), data))?,
                        Payload::Close => {
                            return Ok("received Close packet");
                        }
                    }
                }
                _ = timer.as_mut() => {
                    return Err(Error::Timeout(timeout));
                }
                _ = data_tx.closed() => {
                    return Ok("data channel closed");
                }
                _ = ack_tx.closed() => {
                    return Ok("ack channel closed");
                }
            }
        }
    }

    async fn collect<O>(
        mut collect_rx: UnboundedReceiver<Payload>,
        mut output: O,
    ) -> Result<&'static str, Error>
    where
        O: Sink<Payload> + Unpin,
        Error: From<O::Error>,
    {
        while let Some(payload) = collect_rx.recv().await {
            tracing::trace!(?payload, "sending");
            output.send(payload).await?;
        }

        tracing::trace!("sending Close");
        _ = output.send(Payload::Close).await;

        Ok("channel closed")
    }

    async fn receive<P>(
        mut data_rx: UnboundedReceiver<(usize, Data)>,
        processed_tx: UnboundedSender<Bytes>,
        mut processor: P,
        send_tx: UnboundedSender<Payload>,
        debounce_delay: Duration,
    ) -> Result<&'static str, Error>
    where
        P: Processor,
    {
        let mut debounce = Box::pin(Timer::new(debounce_delay));
        let mut acked: usize = 0;

        loop {
            tokio::select! {
                item = data_rx.recv() => {
                    let Some((pos, data)) = item else {
                        return Ok("data channel closed");
                    };
                    if pos > acked {
                        tracing::trace!(?acked, ?pos, "detected loss of data payload");
                    } else if pos + data.len() <= acked {
                        if pos != 0 || acked != 0 {
                            tracing::trace!(?pos, ?acked, "ignored obsolete data payload");
                        }
                    } else {
                        let mut buf = Bytes::from(data);
                        buf.advance(acked - pos);
                        debug_assert!(!buf.is_empty());
                        acked += buf.len();
                        processor.feed(buf);
                        while let Some(processed) = processor.produce() {
                            processed_tx.send(processed)?;
                        }
                    }
                    debounce.as_mut().start();
                }
                _ = debounce.as_mut() => {
                    send_tx.send(Payload::ack(acked)?)?;
                }
                _ = processed_tx.closed() => {
                    return Ok("processed channel closed");
                }
                _ = send_tx.closed() => {
                    return Ok("send channel closed");
                }
            }
        }
    }

    async fn send(
        mut processed_rx: UnboundedReceiver<Bytes>,
        mut ack_rx: UnboundedReceiver<usize>,
        send_tx: UnboundedSender<Payload>,
        timeout: Duration,
    ) -> Result<&'static str, Error> {
        let mut timer = Box::pin(Timer::new(timeout));
        let mut acked: usize = 0;
        let mut sent: usize = 0;
        let mut buffer = BytesMut::new();
        let mut closing = None::<&'static str>;

        while closing.is_none() {
            tokio::select! {
                item = processed_rx.recv() => {
                    match item {
                        Some(data) => {
                            buffer.put(data);
                        }
                        None => {
                            closing.get_or_insert("processed receiver closed");
                        }
                    };
                }
                item = ack_rx.recv() => {
                    match item {
                        Some(ack) => {
                            if ack > sent {
                                return Ok("misbehaving client");
                            }
                            if ack >= acked {
                                timer.as_mut().cancel();
                                buffer.advance(ack - acked);
                                acked = ack;
                                sent = ack;
                            } else {
                                tracing::trace!(?ack, ?acked, "ignored obsolete ack");
                            }
                        }
                        None => {
                            closing.get_or_insert("ack receiver closed");
                        }
                    };
                }
                _ = timer.as_mut() => {
                    tracing::trace!(?acked, ?sent, ?timeout, "ack timed out");
                    sent = acked;
                }
                _ = send_tx.closed() => {
                    return Ok("send channel closed")
                }
            }

            let tail = acked + buffer.len();
            if sent < tail {
                timer.as_mut().start();
                while sent < tail {
                    let mut data = Data::default();
                    let len = data
                        .write(&buffer[sent - acked..])
                        .expect("Data::writer never return Err(_)");
                    assert!(len > 0);
                    send_tx.send(Payload::data(sent, data)?)?;
                    sent += len;
                }
            }
        }

        Ok(closing.unwrap_or("no reason"))
    }
}

// ========================================== Processor ==========================================

pub trait Processor {
    fn feed<T: Buf>(&mut self, data: T);
    fn produce(&mut self) -> Option<Bytes>;
}

// ========================================== Error ==========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("session timeout ({0:?})")]
    Timeout(Duration),

    #[error("closed unexpectedly")]
    ClosedUnexpectedly,

    #[error("i/o: {0}")]
    IO(#[from] std::io::Error),

    #[error("codec: {0}")]
    Codec(#[from] crate::codec::Error),

    #[error("invalid payload")]
    InvalidPayload(#[from] messages::Error),

    #[error("invalid size")]
    InvalidSize(#[from] messages::InvalidSize),

    #[error("channel has benn disconnected")]
    ChannelDisconnected,
}

impl From<Infallible> for Error {
    fn from(_: Infallible) -> Self {
        unreachable!("converting from Infallible")
    }
}

impl<T> From<SendError<T>> for Error {
    fn from(_: SendError<T>) -> Self {
        Error::ChannelDisconnected
    }
}
