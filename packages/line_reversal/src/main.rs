mod ellipsis;
mod messages;
mod reverser;
mod server;
mod session;
mod timer;

use std::{net::SocketAddr, time::Duration};

use clap::Parser;
use futures::{
    channel::mpsc::{unbounded, SendError, UnboundedSender},
    future, select, FutureExt,
};
use futures::{SinkExt, StreamExt};
use reverser::LineReverser;
use server::SessionFactory;
use tokio::{
    net::UdpSocket,
    signal::{self, unix},
    task::JoinSet,
};
use tracing::{level_filters::LevelFilter, Instrument};

use messages::*;
use server::*;
use session::*;
use tracing_subscriber::layer::SubscriberExt;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = Config::parse();

    tracing_forest::worker_task()
        .set_global(true)
        .build_on(|subscriber| subscriber.with(LevelFilter::from_level(config.log_level)))
        .on(async {
            tracing::trace!(?config, "configuration");

            let socket = config.create_socket().await?;
            tracing::trace!(?socket, "bound socket");

            let report_period = config.report_period;
            let (server, closer) = Server::new(socket, config, report_period);

            tokio::pin!(server);

            tokio::spawn(async move {
                let mut sigterm = unix::signal(unix::SignalKind::terminate()).unwrap();
                tokio::pin! {
                    let sigint = signal::ctrl_c().fuse();
                    let sigterm = sigterm.recv().fuse();
                };
                select! {
                    _ = sigint => (),
                    _ = sigterm => (),
                };
                closer.close().await;
            });

            server.serve().await?;

            Ok(())
        })
        .await
}

// ========================================== Config ==========================================

#[derive(Debug, Parser)]
struct Config {
    #[arg(short, long, default_value = "0.0.0.0:20000")]
    bind_addr: SocketAddr,

    #[arg(short = 't', long, value_parser = Config::parse_line_sep, default_value = r"\n")]
    line_separator: u8,

    #[arg(short, long, value_parser = humantime::parse_duration, default_value = "60s")]
    session_timeout: Duration,

    #[arg(short, long, value_parser = humantime::parse_duration, default_value = "3s")]
    ack_timeout: Duration,

    #[arg(short, long, value_parser = humantime::parse_duration, default_value = "30s")]
    report_period: Duration,

    #[arg(short, long, value_enum, default_value = "INFO")]
    log_level: tracing::Level,
}

impl Config {
    async fn create_socket(&self) -> Result<UdpSocket, std::io::Error> {
        UdpSocket::bind(&self.bind_addr).await
    }

    fn parse_line_sep(input: &str) -> Result<u8, String> {
        match input {
            "\\n" => Ok(b'\n'),
            "\\t" => Ok(b'\t'),
            "\\r" => Ok(b'\r'),
            c if c.len() == 1 => Ok(*c.as_bytes().first().unwrap()),
            _ => Err(format!("invalid line separator: {input}")),
        }
    }
}

// ========================================== SessionFactorys ==========================================

impl SessionFactory for Config {
    fn builder(
        &self,
        id: Id,
        remote: SocketAddr,
        output: UnboundedSender<(Message, SocketAddr)>,
    ) -> impl SessionBuilder {
        ConfigSessionBuilder {
            config: self,
            id,
            remote,
            output,
            span: None,
        }
    }
}

// ========================================== SessionBuilder ==========================================

#[derive(Debug)]
struct ConfigSessionBuilder<'c> {
    config: &'c Config,
    id: Id,
    remote: SocketAddr,
    output: UnboundedSender<(Message, SocketAddr)>,
    span: Option<tracing::Span>,
}

impl<'c> SessionBuilder for ConfigSessionBuilder<'c> {
    fn with_span(self, span: tracing::Span) -> Self {
        let Self {
            config,
            id,
            remote,
            output,
            ..
        } = self;
        Self {
            config,
            id,
            remote,
            output,
            span: Some(span),
        }
    }

    fn spawn_on(self, tasks: &mut JoinSet<Id>) -> UnboundedSender<(Message, SocketAddr)> {
        let ConfigSessionBuilder {
            id,
            remote,
            output,
            span,
            config,
        } = self;

        let (rx, input) = unbounded();
        let input = input.filter_map(move |(msg, premote): (Message, SocketAddr)| {
            future::ready((remote == premote).then_some(msg.payload))
        });

        let output = output
            .with(move |payload| {
                future::ready(Ok::<(Message, SocketAddr), SendError>((
                    Message { id, payload },
                    remote,
                )))
            })
            .sink_map_err(|_| session::Error::ChannelDisconnected);

        let processor = LineReverser::new(config.line_separator);
        let session_timeout = config.session_timeout;
        let ack_timeout = config.ack_timeout;

        let task = async move {
            if let Err(err) =
                Session::run(input, output, processor, session_timeout, ack_timeout).await
            {
                tracing::warn!(%err, "session failure");
            } else {
                tracing::info!("session ended normally");
            }
            id
        };

        if let Some(span) = span {
            tasks.spawn(task.instrument(span));
        } else {
            tasks.spawn(task);
        }

        rx
    }
}
