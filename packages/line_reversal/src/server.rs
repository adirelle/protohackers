use std::{collections::HashMap, net::SocketAddr, pin::Pin, time::Duration};

use futures::channel::mpsc::{self, SendError, TrySendError, UnboundedSender};
use futures::{SinkExt, StreamExt};
use tokio::{net::UdpSocket, task::JoinSet, time::interval};
use tokio_util::udp::UdpFramed;

use crate::{codec, messages::*};

// ========================================== Server ==========================================

pub struct Server<F>
where
    F: SessionFactory,
{
    sessions: HashMap<Id, UnboundedSender<(Message, SocketAddr)>>,
    tasks: JoinSet<Id>,
    factory: F,
    conn: UdpFramed<codec::Codec, UdpSocket>,
    output_tx: mpsc::UnboundedSender<(Message, SocketAddr)>,
    output_rx: mpsc::UnboundedReceiver<(Message, SocketAddr)>,
    closer: mpsc::Receiver<()>,
    report_period: Duration,
}

impl<F> Server<F>
where
    F: SessionFactory + Unpin,
{
    pub fn new(socket: UdpSocket, factory: F, report_period: Duration) -> (Self, CloseHandle) {
        let (output_tx, output_rx) = mpsc::unbounded();
        let (closer_tx, closer) = mpsc::channel(1);
        (
            Self {
                sessions: HashMap::default(),
                tasks: JoinSet::default(),
                conn: UdpFramed::new(socket, codec::Codec),
                report_period,
                factory,
                output_tx,
                output_rx,
                closer,
            },
            CloseHandle(closer_tx),
        )
    }

    pub async fn serve(self: Pin<&mut Self>) -> Result<(), Error> {
        let this = self.get_mut();
        let mut packets_received = 0;
        let mut packets_sent = 0;
        tokio::pin! {
            let report_interval = interval(this.report_period);
        };

        tracing::info!("server ready to process packets");
        loop {
            tokio::select! {
                biased;
                Some(join_result) = this.tasks.join_next() => {
                    match join_result {
                        Ok(id) => {
                            this.remove_session(id);
                        },
                        Err(err) => {
                            tracing::warn!(%err, "session join failed");
                        }
                    }
                }
                Some(packet) = this.output_rx.next() => {
                    packets_sent += 1;
                    this.conn.send(packet).await?;
                }
                Some(packet) = this.conn.next() => {
                    packets_received += 1;
                    this.handle(packet)?;
                }
                _ = this.closer.next() => {
                    this.tasks.shutdown().await;
                    tracing::info!("server closed");
                    return Ok(())
                }
                _ = report_interval.tick() => {
                    tracing::info!(
                        %packets_received,
                        %packets_sent,
                        tasks=%this.tasks.len(),
                        sessions=%this.sessions.len(),
                        "report"
                    );
                }
            }
        }
    }

    fn handle(&mut self, packet: Result<(Message, SocketAddr), codec::Error>) -> Result<(), Error> {
        match packet {
            Ok((message, remote)) => self.dispatch(message, remote),
            Err(err) if err.is_invalid_message() => {
                tracing::info!(%err, "dropped invalid packet");
            }
            Err(err) => return Err(err.into()),
        }
        Ok(())
    }

    fn dispatch(&mut self, message: Message, remote: SocketAddr) {
        let id = message.id;
        let session = if let Some(session) = self.sessions.get_mut(&id) {
            session
        } else if message.payload.is_connect() {
            self.create_session(id, remote)
        } else {
            tracing::debug!(%remote, ?message, "received packet for non-open session");
            _ = self.output_tx.unbounded_send((
                Message {
                    id,
                    payload: Payload::Close,
                },
                remote,
            ));
            return;
        };

        if let Err(err) = session.unbounded_send((message, remote)) {
            tracing::warn!(%err, "could not forward packet to session");
            self.remove_session(id);
        }
    }

    fn create_session(
        &mut self,
        id: Id,
        remote: SocketAddr,
    ) -> &mut UnboundedSender<(Message, SocketAddr)> {
        let span = tracing::info_span!("session", ?id, ?remote);
        let sender = self
            .factory
            .builder(id, remote, self.output_tx.clone())
            .with_span(span.clone())
            .spawn_on(&mut self.tasks);
        let session = self.sessions.entry(id).or_insert(sender);

        tracing::debug!(%id, "registered session");

        session
    }

    fn remove_session(&mut self, id: Id) {
        if self.sessions.remove(&id).is_some() {
            tracing::debug!(%id, "unregistered session");
        }
    }
}

// ========================================== CloseHandle ==========================================

#[derive(Debug, Clone)]
pub struct CloseHandle(mpsc::Sender<()>);

impl CloseHandle {
    pub async fn close(mut self) {
        _ = self.0.send(()).await;
    }
}

impl Drop for CloseHandle {
    fn drop(&mut self) {
        self.0.disconnect()
    }
}

// ========================================== SessionFactory ==========================================

pub trait SessionFactory {
    fn builder(
        &self,
        id: Id,
        remote: SocketAddr,
        output: UnboundedSender<(Message, SocketAddr)>,
    ) -> impl SessionBuilder;
}

// ========================================== SessionBuilder ==========================================

pub trait SessionBuilder {
    fn with_span(self, span: tracing::Span) -> Self;
    fn spawn_on(self, tasks: &mut JoinSet<Id>) -> UnboundedSender<(Message, SocketAddr)>;
}

// ========================================== Error ==========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("i/o: {0}")]
    IO(#[from] std::io::Error),

    #[error("codec: {0}")]
    Codec(#[from] codec::Error),

    #[error("channel: {0}")]
    Channel(#[from] SendError),
}

impl<T> From<TrySendError<T>> for Error {
    fn from(value: TrySendError<T>) -> Self {
        Error::Channel(value.into_send_error())
    }
}
