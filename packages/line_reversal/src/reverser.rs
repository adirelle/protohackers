use bytes::{Buf, BufMut, Bytes, BytesMut};

use crate::session::Processor;

// ========================================== LineReverser ==========================================

#[derive(Debug)]
pub struct LineReverser {
    eol: u8,
    reversed: usize,
    buffer: BytesMut,
}

impl LineReverser {
    pub fn new(eol: u8) -> Self {
        Self {
            eol,
            reversed: 0,
            buffer: BytesMut::new(),
        }
    }

    fn next_eol(&self) -> Option<usize> {
        self.buffer
            .iter()
            .enumerate()
            .skip(self.reversed)
            .find_map(|(i, b)| self.eol.eq(b).then_some(i))
    }

    fn reverse_to(&mut self, pos: usize) {
        assert!(pos > self.reversed);
        let mut i = self.reversed;
        let mut j = pos - 1;
        while i < j {
            self.buffer.swap(i, j);
            i += 1;
            j -= 1;
        }
        self.reversed = pos + 1;
    }
}

impl Processor for LineReverser {
    fn feed<T: Buf>(&mut self, data: T) {
        self.buffer.put(data);
        while let Some(pos) = self.next_eol() {
            self.reverse_to(pos)
        }
    }

    fn produce(&mut self) -> Option<Bytes> {
        if self.reversed > 0 {
            let output = self.buffer.split_to(self.reversed).freeze();
            self.reversed = 0;
            Some(output)
        } else {
            None
        }
    }
}

// ========================================== tests ==========================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simple() {
        let mut rev = LineReverser::new(b';');

        rev.feed(&b"foo;ba"[..]);
        rev.feed(&b"r;quz"[..]);

        let output = rev.produce().expect("should produce some bytes");

        assert_eq!(&b"oof;rab;"[..], output.chunk());
    }
}
