use bytes::{BufMut, BytesMut};

use crate::messages::*;

// ========================================== Codec ==========================================

pub struct Encoder;

impl Encoder {
    pub fn encode_message(&mut self, msg: Message, buf: &mut BytesMut) -> Result<(), Error> {
        let Message { id, payload } = msg;

        match payload {
            Payload::Connect => buf.put_slice(b"/connect/"),
            Payload::Data(_, _) => buf.put_slice(b"/data/"),
            Payload::Ack(_) => buf.put_slice(b"/ack/"),
            Payload::Close => buf.put_slice(b"/close/"),
        }

        Self::write_number(buf, id.into());

        match payload {
            Payload::Ack(length) => {
                Self::write_number(buf, length.into());
            }
            Payload::Data(pos, data) => {
                Self::write_number(buf, pos.into());
                buf.put(data.into_raw_bytes());
                buf.put_u8(b'/');
            }
            _ => (),
        }

        if buf.len() > Message::MAX_LENGTH {
            Err(Error::MessageTooLong(buf.len()))
        } else {
            Ok(())
        }
    }

    fn write_number(dst: &mut BytesMut, mut value: i32) {
        let mut buf = [b'0'; 10];
        let mut cur = 10;
        while value > 0 || cur == 10 {
            cur -= 1;
            buf[cur] += (value % 10) as u8;
            value /= 10;
        }
        dst.put_slice(&buf[cur..]);
        dst.put_u8(b'/');
    }
}

// ========================================== Error ==========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("message too long: {0} bytes ({} max)", Message::MAX_LENGTH)]
    MessageTooLong(usize),

    #[error(transparent)]
    InvalidId(InvalidId),

    #[error(transparent)]
    InvalidSize(InvalidSize),
}

// ========================================== tests ==========================================

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{ack, data};

    macro_rules! test_encoder {
        ($( $name:ident: ($id:literal, $payload:expr) => $bytes:literal ),*) => {
            $(
                #[test] fn $name() { test_encode(&$bytes[..], Message::new($id, $payload).unwrap()); }
            )*
        }
    }

    fn test_encode(expected: &[u8], message: Message) {
        let mut actual = BytesMut::with_capacity(expected.len());
        Encoder
            .encode_message(message, &mut actual)
            .expect("should not error");
        assert_eq!(BytesMut::from(expected), actual);
    }

    test_encoder! {
        encode_connect: (10, Payload::Connect) => b"/connect/10/",
        encode_close: (10, Payload::Close) => b"/close/10/",
        encode_ack: (10, ack!(15)) => b"/ack/10/15/",
        encode_data_simple: (10, data!(16, b"foobar")) => b"/data/10/16/foobar/",
        encode_data_escaped: (10, data!(16, br#"fo\/ob\\ar"#)) => br#"/data/10/16/fo\/ob\\ar/"#
    }

    #[test]
    fn encode_max_length_message() {
        let message = Message {
            id: Id::MAX,
            payload: Payload::Data(Size::MAX, Data::from(b"x".repeat(Data::MAX_LENGTH))),
        };

        let mut actual = BytesMut::with_capacity(Message::MAX_LENGTH);
        Encoder
            .encode_message(message, &mut actual)
            .expect("should not error");
    }
}
