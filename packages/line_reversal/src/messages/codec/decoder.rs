use std::{
    num::ParseIntError,
    str::{from_utf8, Utf8Error},
};

use crate::messages::*;
use bytes::Buf;

// ========================================== Decoder ==========================================

pub struct Decoder;

impl Decoder {
    pub fn decode_message(&self, buf: Bytes) -> Result<Message, Error> {
        let mut r = Splitter::new(buf);

        r.skip_whitespace();
        if r.is_empty() {
            return Err(r.error(ErrorKind::IncompleteMessage));
        } else if r.remaining() > Message::MAX_LENGTH {
            return Err(r.error(ErrorKind::MessageTooLong));
        }

        match r.next() {
            Ok(part) if part.is_empty() => (),
            _ => return Err(r.error(ErrorKind::MalformedMessage)),
        }

        let mut payload = r.next()?.try_into()?;
        let id = r.next()?.try_into()?;
        match payload {
            Payload::Ack(ref mut length) => *length = r.next()?.try_into()?,
            Payload::Data(ref mut pos, ref mut data) => {
                *pos = r.next()?.try_into()?;
                *data = r.next()?.try_into()?;
            }
            _ => (),
        };

        r.skip_whitespace();
        if !r.is_empty() {
            return Err(r.error(ErrorKind::TrailingGarbage));
        }

        Ok(Message { id, payload })
    }
}

// ========================================== Reader ==========================================

#[derive(Debug)]
struct Splitter {
    pos: usize,
    buf: Bytes,
}

impl Splitter {
    fn new(buf: Bytes) -> Self {
        Self { pos: 0, buf }
    }

    fn error<T: Into<ErrorKind>>(&self, kind: T) -> Error {
        let start = self.pos;
        let end = start + self.buf.len();
        Error {
            kind: kind.into(),
            start,
            end,
            data: self.buf.clone(),
        }
    }

    fn remaining(&self) -> usize {
        self.buf.len()
    }

    fn is_empty(&self) -> bool {
        self.buf.is_empty()
    }

    fn skip_whitespace(&mut self) {
        let n = self
            .buf
            .chunk()
            .iter()
            .position(|byte| !byte.is_ascii_whitespace())
            .unwrap_or(self.buf.len());
        self.buf.advance(n);
        self.pos += n;
    }

    fn next(&mut self) -> Result<Part, Error> {
        let mut escaped = false;
        for (i, byte) in self.buf.chunk().iter().enumerate() {
            match (escaped, byte) {
                (true, b'/') | (true, b'\\') => escaped = false,
                (false, b'\\') => escaped = true,
                (false, b'/') => {
                    let fragment = Part {
                        start: self.pos,
                        end: self.pos + i,
                        data: self.buf.split_to(i),
                    };
                    self.buf.advance(1);
                    self.pos += i + 1;
                    return Ok(fragment);
                }
                (false, _) => (),
                _ => break,
            }
        }
        Err(self.error(ErrorKind::MalformedMessage))
    }
}

#[derive(Debug)]
struct Part {
    start: usize,
    end: usize,
    data: Bytes,
}

impl Part {
    fn error<T: Into<ErrorKind>>(self, kind: T) -> Error {
        let Self { start, end, data } = self;
        Error {
            kind: kind.into(),
            start,
            end,
            data,
        }
    }

    fn do_and_map_err<F, T, E>(self, f: F) -> Result<T, Error>
    where
        F: Fn(&Bytes) -> Result<T, E>,
        E: Into<ErrorKind>,
    {
        f(&self.data).map_err(|err| self.error(err))
    }

    fn is_empty(&self) -> bool {
        self.data.is_empty()
    }
}

impl TryFrom<Part> for Size {
    type Error = Error;

    fn try_from(value: Part) -> Result<Self, Self::Error> {
        value.do_and_map_err(|data| {
            let s = from_utf8(data.chunk())?;
            let int = s.parse::<i32>()?;
            Size::try_from(int).map_err(ErrorKind::from)
        })
    }
}

impl TryFrom<Part> for Id {
    type Error = Error;

    fn try_from(value: Part) -> Result<Self, Self::Error> {
        value.do_and_map_err(|data| {
            let s = from_utf8(data.chunk())?;
            let int = s.parse::<i32>()?;
            Id::try_from(int).map_err(ErrorKind::from)
        })
    }
}

impl TryFrom<Part> for Payload {
    type Error = Error;

    fn try_from(value: Part) -> Result<Self, Self::Error> {
        Ok(match value.data.chunk() {
            b"connect" => Payload::Connect,
            b"close" => Payload::Close,
            b"data" => Payload::Data(Size(0), Data::default()),
            b"ack" => Payload::Ack(Size(0)),
            _ => return Err(value.error(ErrorKind::InvalidType)),
        })
    }
}

impl TryFrom<Part> for Data {
    type Error = Error;

    fn try_from(value: Part) -> Result<Self, Self::Error> {
        Data::from_raw_bytes(value.data.clone()).map_err(|err| value.error(ErrorKind::from(err)))
    }
}

// ========================================== Error ==========================================

#[derive(Debug, thiserror::Error, Eq, PartialEq)]
#[error("{kind} from {start} to {end}: {:?}", Ellipsis::new(data))]
pub struct Error {
    kind: ErrorKind,
    start: usize,
    end: usize,
    data: Bytes,
}

impl Error {
    pub fn is_incomplete_message(&self) -> bool {
        self.kind.is_incomplete_message()
    }
}

#[derive(Debug, thiserror::Error, Eq, PartialEq)]
pub enum ErrorKind {
    #[error("invalid message type")]
    InvalidType,

    #[error("invalid number")]
    InvalidNumber,

    #[error("invalid id")]
    InvalidId,

    #[error("invalid size")]
    InvalidSize,

    #[error("trailing garbage")]
    TrailingGarbage,

    #[error("malformed message")]
    MalformedMessage,

    #[error("message too long")]
    MessageTooLong,

    #[error("incomplete message")]
    IncompleteMessage,
}

impl ErrorKind {
    pub fn is_incomplete_message(&self) -> bool {
        matches!(self, Self::IncompleteMessage)
    }
}

impl From<ParseIntError> for ErrorKind {
    fn from(_: ParseIntError) -> Self {
        Self::InvalidNumber
    }
}

impl From<Utf8Error> for ErrorKind {
    fn from(_: Utf8Error) -> Self {
        Self::MalformedMessage
    }
}

impl From<InvalidId> for ErrorKind {
    fn from(_: InvalidId) -> Self {
        Self::InvalidId
    }
}

impl From<InvalidSize> for ErrorKind {
    fn from(_: InvalidSize) -> Self {
        Self::InvalidSize
    }
}

impl From<DataError> for ErrorKind {
    fn from(_: DataError) -> Self {
        Self::MalformedMessage
    }
}

// ========================================== tests ==========================================

#[cfg(test)]
mod tests {
    use std::io::Write;

    use super::*;
    use crate::{ack, data};

    fn test_decode(buf: &'static [u8], expected: Message) {
        let actual = Decoder
            .decode_message(Bytes::from_static(buf))
            .expect("should not error");
        assert_eq!(expected, actual);
    }

    macro_rules! test_decoder {
        ($( $name:ident: $bytes:expr => ($id: literal, $payload:expr) ; )*) => {
            $(
                #[test] fn $name() { test_decode(&$bytes[..], Message::new($id, $payload).unwrap()); }
            )*

        }
    }

    test_decoder! {
        decode_connect: b"/connect/10/" => (10, Payload::Connect);
        decode_close: b"/close/10/" => (10, Payload::Close);
        decode_ack: b"/ack/10/15/" => (10, ack!(15));
        decode_data_simple: b"/data/10/16/foobar/" => (10, data!(16, b"foobar"));
        decode_data_empty: b"/data/10/48//" => (10, data!(48, b""));
        decode_data_escaped: br#"/data/10/16/fo\/ob\\ar/"# => (10, data!(16, br#"fo\/ob\\ar"#));
    }

    macro_rules! test_decoder_error {
        ($( $name:ident: $bytes:expr => ($kind:pat, $start:literal, $end:literal, $data:literal) ),*) => {
            $(
                #[test]
                fn $name() {
                    let actual = Decoder.decode_message(Bytes::from_static($bytes)).expect_err("should return an error, not");
                    //println!("error message: {:?}", actual.to_string());
                    assert!(matches!(actual.kind, $kind), "expected {}, got {:?}", stringify!($kind), actual);
                    assert_eq!(($start, $end, Bytes::from_static($data)), (actual.start, actual.end, actual.data), "error information mismatch");
                }
            )*
        }
    }

    test_decoder_error! {
        malformed_message: b"foobarfoobarfoobar" => (ErrorKind::MalformedMessage, 0, 18, b"foobarfoobarfoobar"),
        invalid_type: b"/reset/10/" => (ErrorKind::InvalidType, 1, 6, b"reset"),
        invalid_id: b"/close/dsq/" => (ErrorKind::InvalidNumber, 7, 10, b"dsq"),
        id_overflow: b"/close/2147483648/" => (ErrorKind::InvalidNumber, 7, 17, b"2147483648"),
        invalid_size: b"/ack/10/fddsds/" => (ErrorKind::InvalidNumber, 8, 14, b"fddsds"),
        negative_size: b"/ack/10/-5/" => (ErrorKind::InvalidSize, 8, 10, b"-5"),
        size_overflow: b"/ack/10/2147483648/" => (ErrorKind::InvalidNumber, 8, 18, b"2147483648"),
        trailing_garbage: b"/close/10/fddsds/" => (ErrorKind::TrailingGarbage, 10, 17, b"fddsds/"),
        incomplete_message: b"/close/1" => (ErrorKind::MalformedMessage, 7, 8, b"1"),
        malformed_data: br#"/data/10/0/aaa\bbb/"# => (ErrorKind::MalformedMessage, 11, 19, br#"aaa\bbb/"#),
        only_whitespaces: b"  \n\r\n \t  " => (ErrorKind::IncompleteMessage, 9, 9, b""),
        illegal_escape: br#"/data/10/0/\n/"# => (ErrorKind::MalformedMessage, 11, 14, br#"\n/"#)
    }

    fn generate_big_message(length: usize) -> Bytes {
        let mut w = BytesMut::default().writer();
        write!(w, "/data/{}/{}/", Id::MAX, Size::MAX).unwrap();
        write!(w, "{}/", "x".repeat(length - w.get_ref().len() - 1)).unwrap();

        let buf = w.into_inner();
        assert_eq!(length, buf.len());

        buf.freeze()
    }

    #[test]
    fn decode_max_length_message() {
        let buf = generate_big_message(Message::MAX_LENGTH);
        let actual = Decoder.decode_message(buf).expect("should not return ");

        assert!(matches!(
            actual,
            Message {
                id: Id::MAX,
                payload: Payload::Data(Size::MAX, _)
            }
        ));
    }

    #[test]
    fn decode_message_too_long() {
        let buf = generate_big_message(Message::MAX_LENGTH + 1);

        let actual = Decoder
            .decode_message(buf)
            .expect_err("should return an error, not");

        assert!(matches!(
            actual,
            Error {
                kind: ErrorKind::MessageTooLong,
                ..
            }
        ));
    }
}
