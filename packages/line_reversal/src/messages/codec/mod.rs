mod decoder;
mod encoder;

use std::io;

use tokio_util::codec::{Decoder, Encoder};

use super::*;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("decoder: {0}")]
    Decoder(#[from] decoder::Error),

    #[error("encoder: {0}")]
    Encoder(#[from] encoder::Error),

    #[error(transparent)]
    IO(#[from] io::Error),
}

impl Error {
    pub fn is_invalid_message(&self) -> bool {
        matches!(self, Self::Decoder(_))
    }
}

impl From<Error> for io::Error {
    fn from(value: Error) -> Self {
        match value {
            Error::IO(err) => err,
            _ => io::Error::other(value),
        }
    }
}

#[derive(Debug)]
pub struct Codec;

impl Encoder<Message> for Codec {
    type Error = Error;

    fn encode(&mut self, msg: Message, dst: &mut bytes::BytesMut) -> Result<(), Self::Error> {
        encoder::Encoder
            .encode_message(msg, dst)
            .map_err(Into::into)
    }
}

impl Decoder for Codec {
    type Item = Message;
    type Error = Error;

    fn decode(&mut self, src: &mut bytes::BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        match decoder::Decoder.decode_message(src.split_to(src.len()).freeze()) {
            Ok(msg) => Ok(Some(msg)),
            Err(err) if err.is_incomplete_message() => Ok(None),
            Err(err) => Err(err.into()),
        }
    }
}
