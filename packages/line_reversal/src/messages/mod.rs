pub mod codec;

use std::{
    convert::Infallible,
    fmt::{self, Debug},
    io::{Read, Write},
};

use bytes::{Buf, BufMut, Bytes, BytesMut};

use crate::ellipsis::Ellipsis;

// ========================================== Message ==========================================

#[derive(Eq, PartialEq, Clone, derive_more::From, derive_more::Into)]
pub struct Message {
    pub id: Id,
    pub payload: Payload,
}

impl Message {
    pub const MAX_LENGTH: usize = 1000;

    pub fn new<I>(id: I, payload: Payload) -> Result<Self, Error>
    where
        I: TryInto<Id>,
        Error: From<I::Error>,
    {
        Ok(Self {
            id: id.try_into()?,
            payload,
        })
    }
}

impl fmt::Debug for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.payload {
            Payload::Connect => write!(f, "Connect({})", self.id),
            Payload::Ack(length) => write!(f, "Ack({}, {length})", self.id),
            Payload::Data(pos, data) => write!(f, "Data({}, {pos}, {data:?})", self.id),
            Payload::Close => write!(f, "Close({})", self.id),
        }
    }
}

// ========================================== Payload ==========================================

#[derive(Eq, PartialEq, Clone, derive_more::IsVariant)]
pub enum Payload {
    Connect,
    Ack(Size),
    Data(Size, Data),
    Close,
}

impl Payload {
    pub const MAX_DATA_SIZE: usize = Message::MAX_LENGTH - 32;

    #[inline]
    pub const fn connect() -> Self {
        Self::Connect
    }

    #[inline]
    pub const fn close() -> Self {
        Self::Close
    }

    #[inline]
    pub fn ack<L>(length: L) -> Result<Self, Error>
    where
        L: TryInto<Size>,
        Error: From<L::Error>,
    {
        Ok(Self::Ack(length.try_into()?))
    }

    #[inline]
    pub fn data<P, D>(pos: P, data: D) -> Result<Self, Error>
    where
        P: TryInto<Size>,
        D: TryInto<Data>,
        Error: From<P::Error> + From<D::Error>,
    {
        Ok(Self::Data(pos.try_into()?, data.try_into()?))
    }

    pub fn requires_response(&self) -> bool {
        matches!(self, Self::Ack(_) | Self::Data(_, _))
    }

    pub fn extend_timeout(&self) -> bool {
        matches!(self, Self::Ack(_) | Self::Data(_, _))
    }

    pub fn requires_ack(&self) -> Option<Size> {
        match self {
            Self::Data(pos, _) => Some(*pos),
            _ => None,
        }
    }

    pub fn cancel_ack_timer(&self, expected: Size) -> bool {
        matches!(self, Self::Ack(ack) if ack >= &expected)
    }
}

impl fmt::Debug for Payload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Connect => write!(f, "Connect"),
            Self::Ack(length) => write!(f, "Ack({length})"),
            Self::Data(pos, data) => write!(f, "Data({pos}, {data:?})"),
            Self::Close => write!(f, "Close"),
        }
    }
}

#[cfg(test)]
#[macro_export]
macro_rules! ack {
    ( $length:expr ) => {
        Payload::ack($length).unwrap()
    };
}

#[cfg(test)]
#[macro_export]
macro_rules! data {
    ( $pos:expr, $data:expr ) => {
        Payload::data($pos, $data).unwrap()
    };
}

// ========================================== Data ==========================================

#[derive(Eq, PartialEq, Clone, Default)]
pub struct Data {
    data: BytesMut,
    decoded_len: usize,
}

impl Data {
    pub const MAX_LENGTH: usize = 900;

    fn from_raw_bytes<T: Buf>(bytes: T) -> Result<Self, DataError> {
        let mut data = BytesMut::with_capacity(bytes.remaining());
        data.put(bytes);
        let decoded_len = Self::validate(data.iter())?;
        Ok(Self { data, decoded_len })
    }

    fn into_raw_bytes(self) -> Bytes {
        self.data.freeze()
    }

    fn validate<'a, I: Iterator<Item = &'a u8>>(iter: I) -> Result<usize, DataError> {
        let mut iter = iter.enumerate();
        let mut len = 0;
        while let Some((pos, byte)) = iter.next() {
            if b'\\'.eq(byte) {
                match iter.next() {
                    Some((_, b'\\')) | Some((_, b'/')) => (),
                    Some((_, other)) => return Err(DataError::InvalidEscape(pos, *other)),
                    None => return Err(DataError::UnterminatedEscape),
                }
            }
            len += 1;
        }
        Ok(len)
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.decoded_len
    }
}

impl Read for Data {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        if self.data.is_empty() || buf.is_empty() {
            return Ok(0);
        }
        let mut w = 0;
        let mut r = 0;
        let mut iter = self.data.chunk().iter();
        while w < buf.len() {
            match iter.next() {
                Some(b'\\') => match iter.next() {
                    Some(b'/') => {
                        buf[w] = b'/';
                        r += 2;
                    }
                    Some(b'\\') => {
                        buf[w] = b'\\';
                        r += 2;
                    }
                    _ => unreachable!("internal data are checked at read/write boundaries"),
                },
                Some(b) => {
                    buf[w] = *b;
                    r += 1;
                }
                None => break,
            }
            w += 1;
        }
        self.data.advance(r);
        self.decoded_len -= w;
        Ok(w)
    }
}

impl Write for Data {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let mut iter = buf.iter();
        let mut len = 0;
        while self.data.len() < Self::MAX_LENGTH {
            let Some(byte) = iter.next() else {
                break;
            };
            if *byte == b'/' || *byte == b'\\' {
                self.data.put_u8(b'\\');
            }
            self.data.put_u8(*byte);
            len += 1;
        }
        self.decoded_len += len;
        Ok(len)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

#[cfg(test)]
impl<const N: usize> From<&'static [u8; N]> for Data {
    fn from(value: &'static [u8; N]) -> Self {
        Self::from_raw_bytes(Bytes::from_static(value)).unwrap()
    }
}

#[cfg(test)]
impl From<Vec<u8>> for Data {
    fn from(value: Vec<u8>) -> Self {
        Self::from_raw_bytes(Bytes::from(value)).unwrap()
    }
}

impl From<Data> for Bytes {
    fn from(mut value: Data) -> Self {
        let mut dst = BytesMut::zeroed(value.len());
        let n = value
            .read(&mut dst[..])
            .expect("Data::read never return Err");
        assert_eq!(n, dst.len());
        dst.freeze()
    }
}

impl fmt::Display for Data {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&Ellipsis::new(&self.data), f)
    }
}

impl fmt::Debug for Data {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&Ellipsis::new(&self.data), f)
    }
}

#[derive(Debug, thiserror::Error, PartialEq, Eq)]
pub enum DataError {
    #[error("unterminated escape sequence at end of data")]
    UnterminatedEscape,

    #[error("invalid escaping sequence {} at position {0}", &[b'\\', *.1][..].escape_ascii())]
    InvalidEscape(usize, u8),
}

// ========================================== Error ==========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    Data(#[from] DataError),

    #[error(transparent)]
    InvalidSize(#[from] InvalidSize),

    #[error(transparent)]
    InvalidId(#[from] InvalidId),
}

impl From<Infallible> for Error {
    fn from(value: Infallible) -> Self {
        match value {}
    }
}

// ========================================== Id ==========================================

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone, Default, derive_more::Display)]
pub struct Id(u32);

impl Id {
    pub const MIN: Id = Id(0_u32);
    pub const MAX: Id = Id(i32::MAX as u32);
}

impl TryFrom<i32> for Id {
    type Error = InvalidId;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        if value >= 0 && value <= Self::MAX.0 as i32 {
            Ok(Id(value as u32))
        } else {
            Err(InvalidId)
        }
    }
}

impl TryFrom<u32> for Id {
    type Error = InvalidId;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        if value <= Self::MAX.0 {
            Ok(Id(value))
        } else {
            Err(InvalidId)
        }
    }
}

impl From<Id> for u32 {
    fn from(value: Id) -> Self {
        value.0
    }
}

impl From<Id> for i32 {
    fn from(value: Id) -> Self {
        value.0 as i32
    }
}

#[derive(Debug, thiserror::Error, PartialEq, Eq)]
#[error("id overflow")]
pub struct InvalidId;

// ========================================== Size ==========================================

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Default, derive_more::Display)]
pub struct Size(u32);

impl Size {
    pub const MIN: Size = Size(0);
    pub const MAX: Size = Size(i32::MAX as u32);
}

impl TryFrom<i32> for Size {
    type Error = InvalidSize;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        if value >= 0 {
            Ok(Size(value as u32))
        } else {
            Err(InvalidSize)
        }
    }
}

impl TryFrom<usize> for Size {
    type Error = InvalidSize;

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        if value <= Self::MAX.0 as usize {
            Ok(Size(value as u32))
        } else {
            Err(InvalidSize)
        }
    }
}

impl From<Size> for i32 {
    fn from(value: Size) -> Self {
        value.0 as i32
    }
}

impl From<Size> for u32 {
    fn from(value: Size) -> Self {
        value.0
    }
}

impl From<Size> for usize {
    fn from(value: Size) -> Self {
        usize::try_from(value.0).expect("@todo: handle usized < u32")
    }
}

#[derive(Debug, thiserror::Error, PartialEq, Eq)]
#[error("session length overflow")]
pub struct InvalidSize;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn data_from_raw_bytes() {
        let actual = Data::from_raw_bytes(&br#"fo\/ob\\ar"#[..]).expect("should not error");
        assert_eq!(8, actual.len());
    }

    #[test]
    fn data_from_raw_bytes_unterminated() {
        Data::from_raw_bytes(&br#"foobar\"#[..]).expect_err("should error");
    }

    #[test]
    fn data_from_raw_bytes_invalid_escape() {
        Data::from_raw_bytes(&br#"foo\bar"#[..]).expect_err("should error");
    }

    #[test]
    fn data_write_simple() {
        let mut actual = Data::default();
        let n = actual.write(&b"foobar"[..]).expect("should not error");

        assert_eq!(6, n);
        assert_eq!(6, actual.len());
        assert_eq!(&b"foobar"[..], actual.into_raw_bytes());
    }

    #[test]
    fn data_write_escaped() {
        let mut actual = Data::default();
        let n = actual.write(&br#"fo\ob/ar"#[..]).expect("should not error");

        assert_eq!(8, n);
        assert_eq!(8, actual.len());
        assert_eq!(&br#"fo\\ob\/ar"#[..], actual.into_raw_bytes());
    }

    #[test]
    fn data_write_long_simple() {
        let mut actual = Data::default();
        let data = b"0".repeat(Data::MAX_LENGTH * 2);
        let n = actual.write(&data[..]).expect("should not error");

        assert_eq!(Data::MAX_LENGTH, n);
        assert_eq!(Data::MAX_LENGTH, actual.len());
        assert_eq!(Data::MAX_LENGTH, actual.into_raw_bytes().len());
    }

    #[test]
    fn data_write_long_escaped() {
        let mut actual = Data::default();
        let data = b"/".repeat(Data::MAX_LENGTH * 2);
        let n = actual.write(&data[..]).expect("should not error");

        assert_eq!(Data::MAX_LENGTH / 2, n);
        assert_eq!(Data::MAX_LENGTH / 2, actual.len());
        assert_eq!(Data::MAX_LENGTH, actual.into_raw_bytes().len());
    }

    #[test]
    fn data_read_simple() {
        let mut src = Data::from(b"foobar");
        let mut dst = [0; 4];

        let n = src.read(&mut dst[..]).expect("should not error");

        assert_eq!(b"foob", &dst[..n]);
        assert_eq!(2, src.len());
    }

    #[test]
    fn data_read_escpaed() {
        let mut src = Data::from(br#"fo\/ob\\ar"#);
        let mut dst = [0; 4];

        let n = src.read(&mut dst[..]).expect("should not error");

        assert_eq!(b"fo/o", &dst[..n]);
        assert_eq!(4, src.len());
    }

    // fn data_into_iter_simple() {
    //     let data = Data::from_raw_bytes(Bytes::from_static(b"0123456789"));

    //     assert_eq!(10, data.len());

    //     let actual: Vec<u8> = data.into_iter().collect();
    //     assert_eq!(&b"0123456789"[..], &actual);
    // }

    // #[test]
    // fn data_into_iter_escape() {
    //     let data = Data::from_raw_bytes(Bytes::from_static(br#"01234\/56789"#));

    //     assert_eq!(11, data.len());

    //     let actual: Vec<u8> = data.into_iter().collect();
    //     assert_eq!(&b"01234/56789"[..], &actual);
    // }
}
