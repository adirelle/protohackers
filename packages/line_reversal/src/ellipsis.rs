use std::{borrow::Borrow, fmt};

const MAX_FULL_LENGTH: usize = 16;

pub struct Ellipsis<'e, T>(&'e T);

impl<'e, T> Ellipsis<'e, T> {
    pub fn new(inner: &'e T) -> Self {
        Self(inner)
    }
}

impl<'e, T> fmt::Display for Ellipsis<'e, T>
where
    T: Borrow<[u8]>,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let data = <T as Borrow<[u8]>>::borrow(self.0);
        let len = data.len();
        if len > MAX_FULL_LENGTH {
            write!(
                f,
                "{}\\{{{}}}{}",
                data[..MAX_FULL_LENGTH / 2].escape_ascii(),
                len - MAX_FULL_LENGTH,
                data[len - MAX_FULL_LENGTH / 2..].escape_ascii(),
            )
        } else {
            data.escape_ascii().fmt(f)
        }
    }
}

impl<'e, T> fmt::Debug for Ellipsis<'e, T>
where
    T: Borrow<[u8]>,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let data = <T as Borrow<[u8]>>::borrow(self.0);
        let len = data.len();
        if len > MAX_FULL_LENGTH {
            write!(
                f,
                r#"b"{}..{}" ({} bytes)"#,
                data[..MAX_FULL_LENGTH / 2].escape_ascii(),
                data[len - MAX_FULL_LENGTH / 2..].escape_ascii(),
                len
            )
        } else {
            write!(f, r#"b"{}" ({} bytes)"#, data.escape_ascii(), len)
        }
    }
}
