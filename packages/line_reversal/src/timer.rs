use std::{
    pin::Pin,
    task::{Context, Poll, Waker},
    time::Duration,
};

use futures::Future;
use tokio::time::{sleep, Instant, Sleep};

// ========================================== Timer ==========================================

#[derive(Debug)]
pub struct Timer {
    delay: Duration,
    state: State,
}

impl Timer {
    pub fn new(delay: Duration) -> Self {
        Self {
            delay,
            state: State::Stopped(None),
        }
    }

    pub fn is_started(&self) -> bool {
        matches!(self.state, State::Pending(_))
    }

    pub fn start(self: Pin<&mut Self>) -> bool {
        match self.state {
            State::Pending(_) => return true,
            State::Stopped(Some(ref waker)) => waker.wake_by_ref(),
            _ => (),
        }
        self.get_mut().state = State::Pending(Box::pin(sleep(self.delay)));
        false
    }

    pub fn cancel(self: Pin<&mut Self>) -> bool {
        let was_started = self.is_started();
        self.get_mut().state = State::Stopped(None);
        was_started
    }

    pub fn restart(self: Pin<&mut Self>) -> bool {
        if !self.is_started() {
            return self.start();
        };
        let Self {
            delay,
            state: State::Pending(ref mut sleep),
        } = self.get_mut()
        else {
            unreachable!();
        };
        sleep.as_mut().reset(Instant::now() + *delay);
        true
    }
}

impl Future for Timer {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();
        use State::*;
        match this.state {
            Stopped(ref mut waker) => {
                *waker = Some(cx.waker().clone());
                Poll::Pending
            }
            Pending(ref mut sleep) => {
                futures::ready!(sleep.as_mut().poll(cx));
                this.state = Stopped(None);
                Poll::Ready(())
            }
        }
    }
}

// ========================================== State ==========================================

#[derive(Debug)]
enum State {
    Stopped(Option<Waker>),
    Pending(Pin<Box<Sleep>>),
}
