use std::ops::AddAssign;

use server::prelude::*;
use tokio::select;

#[tokio::main]
async fn main() -> Result<()> {
    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new())?;

    server::serve(Server, "0.0.0.0:20000".parse()?).await
}

#[derive(Debug, Default, Clone)]
struct Stats {
    requests: usize,
    responses: usize,
    errors: usize,
    was_prime: usize,
}

impl AddAssign for Stats {
    fn add_assign(&mut self, rhs: Self) {
        self.requests += rhs.requests;
        self.responses += rhs.responses;
        self.errors += rhs.errors;
        self.was_prime += rhs.was_prime;
    }
}

struct Server;

impl TCPHandlerFactory for Server {
    type Handler = Connection;
    type Stats = Stats;

    fn create(&mut self, socket: TcpStream, _client_addr: SocketAddr) -> Result<Self::Handler> {
        Ok(Connection {
            stream: BufStream::new(socket),
        })
    }
}

struct Connection {
    stream: BufStream<TcpStream>,
}

#[async_trait]
impl TCPHandler<Stats> for Connection {
    async fn handle(mut self, until: CancellationToken) -> Result<Stats> {
        let mut buf = String::with_capacity(4096);
        let mut stats = Stats::default();

        loop {
            let size = select! {
                res = self.stream.read_line(&mut buf) => res,
                _ = until.cancelled() => Ok(0),
            }?;
            if size == 0 {
                break Ok(stats);
            }
            stats.requests += 1;

            let resp: Result<Response> =
                serde_json::from_str(&buf)
                    .map_err(Into::into)
                    .and_then(|req: Request| {
                        tracing::info!("request: {req:?}");
                        req.handle().map_err(Into::into)
                    });
            buf.clear();

            tracing::info!("response: {resp:?}");
            match resp {
                Ok(value) => {
                    let mut write_buf = serde_json::to_vec(&value)?;
                    write_buf.push(b'\n');
                    self.stream.write_all(&write_buf).await?;
                    if value.prime {
                        stats.was_prime += 1;
                    }
                }
                Err(err) => {
                    let errmsg = format!("{err:?}\n");
                    self.stream.write_all(errmsg.as_bytes()).await?;
                    stats.errors += 1;
                }
            }
            self.stream.flush().await?;
            stats.responses += 1;
        }
    }
}

#[derive(Debug, serde::Deserialize)]
struct Request {
    method: String,
    number: serde_json::Number,
}

impl Request {
    fn handle(self) -> Result<Response> {
        self.number
            .as_f64()
            .map(|input| Response {
                method: self.method,
                prime: is_prime(input),
            })
            .ok_or(anyhow!("expected an integer, got {:?}", self.number))
    }
}

#[derive(Debug, serde::Serialize)]
struct Response {
    method: String,
    prime: bool,
}

fn is_prime(fl: f64) -> bool {
    if fl.floor() != fl {
        return false;
    }
    let n = fl as u64;
    if n <= 3 {
        n > 1
    } else if n % 2 == 0 || n % 3 == 0 {
        false
    } else {
        let limit = fl.sqrt().ceil() as u64;
        (5..limit + 1).all(|x| n % x != 0 && n % (x + 2) != 0)
    }
}
