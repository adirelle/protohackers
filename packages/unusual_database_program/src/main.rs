use std::collections::HashMap;
use std::io::Write;

use bstr::{BStr, ByteSlice};
use lazy_static::lazy_static;
use server::prelude::*;
use tokio::net::UdpSocket;
use tracing::info;

static VERSION_KEY: &[u8] = b"version";
lazy_static! {
    static ref VERSION: &'static BStr = git_version::git_version!().into();
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new())?;

    let socket = UdpSocket::bind("0.0.0.0:20000").await?;
    let mut buffer = [0_u8; 1000];

    info!(addr = display(socket.local_addr()?), "ready to serve");

    let mut server = Server::new();

    loop {
        let (len, client) = socket.recv_from(&mut buffer).await?;
        if let Some(reply) = server.handle(&mut buffer[..len]) {
            let final_len = len + 1 + reply.len();
            write!(&mut buffer[len..final_len], "={reply}")?;
            let answer = &buffer[..final_len];
            socket.send_to(answer, client).await?;
        }
    }
}

struct Server {
    values: HashMap<Vec<u8>, Vec<u8>>,
}

impl Server {
    fn new() -> Self {
        Self {
            values: HashMap::new(),
        }
    }

    fn handle(&mut self, packet: &mut [u8]) -> Option<&BStr> {
        if let Some(key_length) = packet.find_byte(b'=') {
            if &packet[..key_length] != VERSION_KEY {
                let keystr = Vec::from(&packet[..key_length]);
                _ = self
                    .values
                    .insert(keystr, Vec::from(&packet[key_length + 1..]));
            }
            None
        } else if packet == VERSION_KEY {
            Some(VERSION.as_bstr())
        } else if let Some(value) = self.values.get(packet) {
            Some(BStr::new(value))
        } else {
            None
        }
    }
}
