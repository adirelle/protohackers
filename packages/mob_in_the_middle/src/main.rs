use std::str::{from_utf8, Utf8Error};

use bytes::{Buf, BufMut};
use futures::StreamExt;
use tokio::net::{TcpListener, TcpStream};
use tokio::task::JoinSet;
use tokio_util::codec::{Decoder, Encoder, Framed, LinesCodecError};
use tracing::Instrument;

const UPSTREAM: &str = "chat.protohackers.com:16963";
const BOGUSCOIN_ADDR: &str = "7YWHMfk9JZe0LM0g1ZauHuiSxhI";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new())?;

    let listener = TcpListener::bind("0.0.0.0:20000").await?;
    let mut clients = JoinSet::<()>::new();

    loop {
        tokio::select! {
            Ok((stream, addr)) = listener.accept() => {
                let span = tracing::info_span!("client", remote=%addr);
                clients.spawn(client(stream).instrument(span));
            },
            Some(result) = clients.join_next() => {
                if let Err(error) = result {
                    tracing::warn!(%error, "client aborted");
                }
            },
            else => {
                return Ok(());
            }
        }
    }
}

async fn client(stream: TcpStream) {
    tracing::info!("client connected");
    match client_inner(stream).await {
        Ok(()) => tracing::info!("client disconnected"),
        Err(error) => tracing::warn!(%error, "client error"),
    }
}

async fn client_inner(stream: TcpStream) -> Result<(), Error> {
    let (client_sink, client_stream) = Framed::new(stream, Codec).split();
    let (server_sink, server_stream) =
        Framed::new(TcpStream::connect(UPSTREAM).await?, Codec).split();

    let upstream = client_stream
        .map(|line| line.map(replace))
        .forward(server_sink);

    let downstream = server_stream
        .map(|line| line.map(replace))
        .forward(client_sink);

    futures::try_join!(upstream, downstream)?;

    Ok(())
}

fn replace(input: String) -> String {
    let mut output = String::with_capacity(input.len());
    for chunk in input.split_inclusive(' ') {
        let word = chunk.trim_end();
        if is_boguscoin_address(word) {
            output += BOGUSCOIN_ADDR;
        } else {
            output += word;
        }
        if word.len() < chunk.len() {
            output += &chunk[word.len()..];
        }
    }
    output
}

fn is_boguscoin_address(chunk: &str) -> bool {
    chunk.starts_with('7')
        && (26..=35).contains(&chunk.len())
        && chunk.chars().all(char::is_alphanumeric)
}

struct Codec;

impl Decoder for Codec {
    type Item = String;
    type Error = Error;

    fn decode(&mut self, src: &mut bytes::BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        let Some(eol) = src.as_ref().iter().position(|b| b'\n'.eq(b)) else {
            return Ok(None);
        };
        let slice = src.split_to(eol);
        src.advance(1);
        let line = from_utf8(slice.as_ref())?.to_owned();
        Ok(Some(line))
    }
}

impl Encoder<String> for Codec {
    type Error = Error;

    fn encode(&mut self, item: String, dst: &mut bytes::BytesMut) -> Result<(), Self::Error> {
        dst.put_slice(item.as_ref());
        dst.put_u8(b'\n');
        Ok(())
    }
}

#[derive(Debug, thiserror::Error)]
enum Error {
    #[error(transparent)]
    IO(#[from] std::io::Error),

    #[error(transparent)]
    Codec(#[from] LinesCodecError),

    #[error(transparent)]
    InvalidUTf8(#[from] Utf8Error),
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! test_replace {
        ( $( $name:ident: $input:literal => $output:literal ; )*  ) => {
            $(
                #[test]
                fn $name() {
                    assert_eq!(replace(String::from($input)), String::from($output));
                }
            )*
        };
    }

    test_replace! {
        one: "foo 7F1u3wSD5RbOHQmupo9nx4TnhQ bar" => "foo 7YWHMfk9JZe0LM0g1ZauHuiSxhI bar";
        two: "foo 7iKDZEwPZSqIvDnHvVN2r0hUWXD5rHX bar" => "foo 7YWHMfk9JZe0LM0g1ZauHuiSxhI bar";
        three: "foo 7LOrwbDlS8NujgjddyogWgIM93MV5N2VR bar" => "foo 7YWHMfk9JZe0LM0g1ZauHuiSxhI bar";
        four: "foo 7adNeSwJkMakpEcln9HEtthSRtxdmEHOT8T bar" => "foo 7YWHMfk9JZe0LM0g1ZauHuiSxhI bar";
        at_start: "7adNeSwJkMakpEcln9HEtthSRtxdmEHOT8T bar" => "7YWHMfk9JZe0LM0g1ZauHuiSxhI bar";
        at_end: "foo 7adNeSwJkMakpEcln9HEtthSRtxdmEHOT8T" => "foo 7YWHMfk9JZe0LM0g1ZauHuiSxhI";
        no_numbers: "foo 750 bar" => "foo 750 bar";
    }
}
