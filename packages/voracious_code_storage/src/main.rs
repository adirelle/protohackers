mod ellipsis;
mod model;
mod protocol;

use tokio::net::TcpSocket;

use self::model::Storage;
use self::protocol::Server;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    setup_tracing()?;

    let addr = "0.0.0.0:20000".parse().unwrap();
    let socket = TcpSocket::new_v4()?;
    socket.set_reuseaddr(true)?;
    socket.set_reuseport(true)?;
    socket.bind(addr)?;

    let storage = Storage::default();
    let mut server = Server::new(storage);

    server.serve(socket).await?;

    Ok(())
}

fn setup_tracing() -> Result<(), Box<dyn std::error::Error + 'static>> {
    use tracing::{subscriber::set_global_default, Level};
    use tracing_subscriber::{
        filter::*,
        fmt::{format::FmtSpan, layer},
        layer::SubscriberExt,
        prelude::*,
        registry,
    };

    set_global_default(
        registry().with(
            layer()
                .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
                .with_target(false)
                .with_filter(LevelFilter::from(Level::TRACE))
                .with_filter(FilterFn::new(|metadata| {
                    metadata
                        .module_path()
                        .is_some_and(|path| path.starts_with("voracious_code_storage::"))
                })),
        ),
    )?;

    Ok(())
}
