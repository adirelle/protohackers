mod messages;

use std::io;
use std::net::SocketAddr;
use std::sync::Arc;

use futures::{SinkExt, StreamExt};
use tokio::net::TcpSocket;
use tokio::sync::RwLock;
use tokio::task::JoinSet;

use crate::model::Storage;
use crate::protocol::messages::Method;

use self::messages::{Request, Response};

// ======================================== Server ========================================

pub struct Server {
    handler: Handler,
}

impl Server {
    pub fn new(storage: Storage) -> Self {
        Self {
            handler: Handler::new(storage),
        }
    }

    #[tracing::instrument(level = "info", skip(self), ret)]
    pub async fn serve(&mut self, socket: TcpSocket) -> Result<(), Error> {
        let listener = socket.listen(5)?;
        let mut clients = JoinSet::<_>::new();

        loop {
            tokio::select! {
                client = listener.accept() => {
                    let (stream, remote) = client?;
                    let client = Client::new(stream, remote, self.handler.clone());
                    clients.spawn(client.run());
                }
                result = clients.join_next() => {
                    if let Some(Err(err)) = result {
                        tracing::warn!(%err, "task failure");
                    }
                }
            };
        }
    }
}

// ======================================== Client ========================================

#[derive(Debug)]
struct Client<S> {
    stream: tokio_util::codec::Framed<S, messages::Codec>,
    remote: SocketAddr,
    handler: Handler,
}

impl<S> Client<S>
where
    S: tokio::io::AsyncRead + tokio::io::AsyncWrite + Unpin,
{
    pub fn new(stream: S, remote: SocketAddr, handler: Handler) -> Self {
        Self {
            stream: tokio_util::codec::Framed::new(stream, messages::Codec),
            remote,
            handler,
        }
    }

    #[tracing::instrument(name = "client", level = "debug", skip(self), fields(remote=%self.remote), ret, err(level="warn"))]
    pub async fn run(mut self) -> Result<(), Error> {
        loop {
            self.stream.send(Response::Ready).await?;

            let Some(result) = self.handle().await else {
                return Ok(());
            };

            let response = match result {
                Err(err) if err.is_transient() => Ok(Response::error(err)),

                other => other,
            }?;

            self.stream.send(response).await?;
        }
    }

    pub async fn handle(&mut self) -> Option<Result<Response, Error>> {
        let request = self.stream.next().await?;
        Some(match request {
            Ok(request) => {
                tracing::trace!(%request, "request");
                match self.handler.handle(request).await {
                    Ok(response) => {
                        tracing::trace!(%response, "response");
                        Ok(response)
                    }
                    Err(err) => {
                        tracing::trace!(%err, "error");
                        Err(err)
                    }
                }
            }
            Err(err) => {
                tracing::info!(%err, "bad request");
                Err(err.into())
            }
        })
    }
}

// ======================================== Handler ========================================

#[derive(Debug, Default, Clone)]
struct Handler {
    storage: Arc<RwLock<Storage>>,
}

impl Handler {
    pub fn new(storage: Storage) -> Self {
        Self {
            storage: Arc::new(RwLock::new(storage)),
        }
    }

    pub async fn handle(&mut self, req: Request) -> Result<Response, Error> {
        use Request::*;
        use Response::*;
        Ok(match req {
            Get {
                path,
                revision: Some(rev),
            } => {
                let storage = self.storage.read().await;
                let content = storage.get(&path, rev)?;
                FileContent(content.clone())
            }
            Get {
                path,
                revision: None,
            } => {
                let storage = self.storage.read().await;
                let (_, content) = storage.get_latest(&path)?;
                FileContent(content.clone())
            }
            Put { path, content } => {
                let mut storage = self.storage.write().await;
                let revision = storage.put(&path, content)?;
                RevisionCreated(revision)
            }
            List { path } => {
                let storage = self.storage.read().await;
                let listing = storage.list(&path)?;
                DirectoryListing(listing)
            }
            Request::Usage { method } => return Err(self::Error::Usage(method)),
            Help => Usage(Method::Help),
        })
    }
}

// ======================================== Handler ========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    IO(#[from] io::Error),

    #[error(transparent)]
    Model(#[from] crate::model::Error),

    #[error(transparent)]
    Messages(#[from] messages::Error),

    #[error("{}", .0.usage())]
    Usage(Method),
}

impl Error {
    pub fn is_transient(&self) -> bool {
        use Error::*;
        match self {
            IO(_) => false,
            Model(inner) => inner.is_transient(),
            Messages(inner) => inner.is_transient(),
            Usage(_) => true,
        }
    }
}
