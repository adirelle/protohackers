use std::{
    io::{self, Write},
    ops::Deref,
    str::{from_utf8_unchecked, FromStr},
};

use bytes::{Buf, BufMut, Bytes};
use tokio_util::codec::{Decoder, Encoder};

use crate::model::{self, Content, Entry, Path};

use super::*;

#[derive(Debug)]
pub struct Codec;

impl Codec {
    const END_OF_COMMAND: u8 = b'\n';

    fn is_end_of_command(b: &u8) -> bool {
        b.eq(&Self::END_OF_COMMAND)
    }

    fn is_word_separator(b: &u8) -> bool {
        b.is_ascii_whitespace()
    }

    fn decode_revision(input: &mut Bytes) -> Result<Revision, Error> {
        if let Some(b'r') = input.first() {
            input.advance(1);
        }
        Self::decode_number::<usize>(input)
            .ok_or(Error::IllegalRevision)
            .and_then(|n| Revision::try_from(n).map_err(Into::into))
    }

    fn decode_number<N: FromStr>(input: &Bytes) -> Option<N> {
        if input.iter().all(u8::is_ascii_digit) {
            // SAFETY: we just checked there are only ascii digits, which are all valid UTF8 characters.
            let digits = unsafe { from_utf8_unchecked(&input[..]) };
            digits.parse::<N>().ok()
        } else {
            None
        }
    }
}

impl Decoder for Codec {
    type Item = Request;

    type Error = Error;

    fn decode(&mut self, src: &mut bytes::BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        let Some(eoc) = src.iter().position(Self::is_end_of_command) else {
            return Ok(None);
        };
        let line = src.clone().freeze().slice(..eoc);
        let to_split = line.clone();
        let mut words = to_split
            .deref()
            .split(Self::is_word_separator)
            .map(|word| line.slice_ref(word));

        let method = Method::try_from(words.next().unwrap_or_default())?;
        let mut arguments = words.collect::<Vec<_>>();

        let request = match (method, &mut arguments[..]) {
            (Method::List, [path]) => Request::List {
                path: DirPath::parse(path)?,
            },
            (Method::Get, [path]) => Request::Get {
                path: FilePath::parse(path)?,
                revision: None,
            },
            (Method::Get, [path, ref mut revision]) => Request::Get {
                path: FilePath::parse(path)?,
                revision: Some(Self::decode_revision(revision)?),
            },
            (Method::Put, [path, length]) => {
                let length = Self::decode_number(length).unwrap_or_default();
                if src.len() < eoc + 1 + length {
                    return Ok(None);
                }
                src.advance(eoc + 1);
                let path = FilePath::parse(path)?;
                let content = src.split_to(length).freeze();
                if !content.iter().all(Content::is_valid_character) {
                    return Err(Error::IllegalContent);
                }
                return Ok(Some(Request::Put { path, content }));
            }
            (Method::Help, _) => Request::Help,
            _ => Request::Usage { method },
        };

        src.advance(eoc + 1);

        Ok(Some(request))
    }
}

impl Encoder<Response> for Codec {
    type Error = Error;

    fn encode(&mut self, item: Response, dst: &mut bytes::BytesMut) -> Result<(), Self::Error> {
        match item {
            Response::Ready => dst.put_slice(b"READY\n"),
            Response::Error(err) => writeln!(dst.writer(), "ERR {err}")?,
            Response::DirectoryListing(listing) => {
                let mut w = dst.writer();
                writeln!(w, "OK {}", listing.len())?;
                for (name, entry) in listing.into_iter() {
                    write!(w, "{}", name)?;
                    match entry {
                        Entry::Directory => writeln!(w, "/ DIR"),
                        Entry::File(rev) => writeln!(w, " {}", rev),
                    }?
                }
            }
            Response::Usage(method) => writeln!(dst.writer(), "OK usage: {}", method.usage())?,
            Response::RevisionCreated(rev) => writeln!(dst.writer(), "OK {rev}")?,
            Response::FileContent(content) => {
                writeln!(dst.writer(), "OK {}", content.len())?;
                dst.put(content.as_ref());
            }
        };
        Ok(())
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("illegal method: {}", .0.escape_ascii())]
    IllegalMethod(Bytes),

    #[error(transparent)]
    Model(#[from] model::Error),

    #[error("illegal revision")]
    IllegalRevision,

    #[error("illegal revision")]
    IllegalContent,

    #[error(transparent)]
    IO(#[from] io::Error),
}

impl From<IllegalMethod> for Error {
    fn from(value: IllegalMethod) -> Self {
        Error::IllegalMethod(value.0)
    }
}

impl Error {
    pub fn is_transient(&self) -> bool {
        !matches!(self, Self::IO(_))
    }
}

#[cfg(test)]
mod tests {
    use bytes::{BufMut, BytesMut};

    use crate::model::{ListingBuilder, Name, Path, Revision};

    use super::*;

    macro_rules! test_decoder {
        ( $test_name:ident : $input:literal => $( $expected:tt )* ) => {
            #[test]
            fn $test_name() {
                let mut input = BytesMut::new();
                input.put(&$input[..]);
                let actual = Codec.decode(&mut input);
                assert!(matches!(actual, $( $expected )*));
            }
        };
    }

    test_decoder! { decode_no_line: b"" => Ok(None) }

    test_decoder! { decode_illegal_method: b"TOTO\n" =>  Err(Error::IllegalMethod(bytes)) if bytes.eq(&b"TOTO"[..]) }

    test_decoder! { decode_help: b"HELP\n" => Ok(Some(Request::Help)) }

    test_decoder! { decode_list:
        b"LIST /some/path/\n"
        => Ok(Some(Request::List{ path }))
            if path == DirPath::parse(b"/some/path").unwrap()
    }

    test_decoder! { decode_get_latest:
        b"GET /some/file\n"
        => Ok(Some(Request::Get{ path, revision: None }))
            if path == FilePath::parse(b"/some/file").unwrap()
    }

    test_decoder! { decode_get_rev:
        b"GET /some/file 5\n"
        => Ok(Some(Request::Get{ path, revision: Some(rev) }))
            if path == FilePath::parse(b"/some/file").unwrap()
            && rev == Revision::try_from(5).unwrap()
    }

    test_decoder! { decode_get_rev_prefix:
        b"GET /some/file r5\n"
        => Ok(Some(Request::Get{ path, revision: Some(rev) }))
            if path == FilePath::parse(b"/some/file").unwrap()
            && rev == Revision::try_from(5).unwrap()
    }

    test_decoder! { decode_get_put:
        b"PUT /test 6\nfoobar"
        => Ok(Some(Request::Put{ path, content }))
            if path == FilePath::parse(b"/test").unwrap()
            && content.eq(&b"foobar"[..])
    }

    test_decoder! { decode_get_put_invalid_length:
        b"PUT /test rezezrezr\n"
        => Ok(Some(Request::Put{ path, content }))
            if path == FilePath::parse(b"/test").unwrap()
            && content.is_empty()
    }

    macro_rules! test_encoder {
        ( $test_name:ident : $input:expr => $expected:literal ) => {
            #[test]
            fn $test_name() {
                let mut dst = BytesMut::new();
                Codec.encode($input, &mut dst).expect("should success");
                assert_eq!(
                    $expected,
                    &dst[..],
                    "\n  expected: `{}`\n   but got: `{}`",
                    $expected.escape_ascii(),
                    &dst[..].escape_ascii()
                );
            }
        };
    }

    test_encoder! { encode_help: Response::Usage(Method::Help) => b"OK usage: HELP|GET|PUT|LIST\n" }

    test_encoder! { encode_ready: Response::Ready => b"READY\n" }

    test_encoder! { encode_revision_created: Response::RevisionCreated(Revision::try_from(1).unwrap()) => b"OK r1\n" }

    test_encoder! { encode_file: Response::FileContent(Bytes::from_static(b"foobar\nbazquz")) => b"OK 13\nfoobar\nbazquz" }

    test_encoder! { encode_empty_file: Response::FileContent(Bytes::new()) => b"OK 0\n" }

    test_encoder! { encode_error: Response::error(Error::IllegalRevision) => b"ERR illegal revision\n" }

    #[test]
    fn encode_listing() {
        let mut dst = BytesMut::new();

        let mut builder = ListingBuilder::default();
        builder.insert_file(&Name::parse("002").unwrap(), Revision::try_from(1).unwrap());
        builder.insert_file(&Name::parse("001").unwrap(), Revision::try_from(2).unwrap());
        builder.insert_dir(&Name::parse("dir").unwrap());
        let listing = builder.build();

        Codec
            .encode(Response::DirectoryListing(listing), &mut dst)
            .expect("should success");
        assert_eq!(
            b"OK 3\n001 r2\n002 r1\ndir/ DIR\n",
            &dst[..],
            "got: `{}`",
            &dst[..].escape_ascii()
        );
    }
}
