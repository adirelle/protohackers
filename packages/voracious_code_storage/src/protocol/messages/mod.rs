mod codec;

use std::fmt;

use bytes::Bytes;

use crate::ellipsis::Ellipsis;
use crate::model::{DirPath, FilePath, Listing, Revision};

pub use codec::*;

// ======================================== Request ========================================
pub enum Request {
    Get {
        path: FilePath,
        revision: Option<Revision>,
    },
    Put {
        path: FilePath,
        content: Bytes,
    },
    List {
        path: DirPath,
    },
    Usage {
        method: Method,
    },
    Help,
}

impl fmt::Debug for Request {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Request::*;
        match self {
            Get { path, revision } => f
                .debug_struct("Get")
                .field("path", path)
                .field("revision", revision)
                .finish(),
            Put { path, content } => f
                .debug_struct("Put")
                .field("path", path)
                .field("content", &Ellipsis::new(content))
                .finish(),
            List { path } => f.debug_struct("List").field("path", path).finish(),
            Usage { method } => f.debug_struct("Usage").field("method", method).finish(),
            Help => write!(f, "Help"),
        }
    }
}

impl fmt::Display for Request {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Request::*;
        match self {
            Get {
                path,
                revision: Some(revision),
            } => write!(f, "GET {path} {revision}"),
            Get {
                path,
                revision: None,
            } => write!(f, "GET {path}"),
            Put { path, content } => write!(
                f,
                "PUT {path}, {} ({})",
                content.len(),
                Ellipsis::new(content)
            ),
            List { path } => write!(f, "LIST {path}"),
            Usage { method } => write!(f, "{method} something something"),
            Help => f.write_str("HELP"),
        }
    }
}

// ======================================== Method ========================================

#[derive(Debug, Clone, Copy)]
pub enum Method {
    Get,
    Put,
    List,
    Help,
}

impl Method {
    pub const fn usage(&self) -> &'static str {
        use Method::*;
        match self {
            Get => "GET file [revision]",
            Put => "PUT file length newline data",
            List => "LIST dir",
            Help => "HELP|GET|PUT|LIST",
        }
    }
}

impl TryFrom<Bytes> for Method {
    type Error = IllegalMethod;

    fn try_from(value: Bytes) -> Result<Self, Self::Error> {
        use Method::*;
        Ok(match &(value.to_ascii_uppercase())[..] {
            b"GET" => Get,
            b"PUT" => Put,
            b"LIST" => List,
            b"HELP" => Help,
            _ => return Err(IllegalMethod(value)),
        })
    }
}

impl fmt::Display for Method {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Method::*;
        f.write_str(match self {
            Get => "GET",
            Put => "PUT",
            List => "LIST",
            Help => "HELP",
        })
    }
}

#[derive(Debug, thiserror::Error)]
#[error("illegal method: {}", .0.escape_ascii())]
pub struct IllegalMethod(Bytes);

// ======================================== Response ========================================

pub enum Response {
    Ready,
    Error(Box<dyn std::error::Error + Send>),
    Usage(Method),
    RevisionCreated(Revision),
    FileContent(Bytes),
    DirectoryListing(Listing),
}

impl Response {
    pub fn error<E>(err: E) -> Self
    where
        E: std::error::Error + Send + 'static,
    {
        Self::Error(Box::new(err))
    }
}

impl fmt::Display for Response {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Response::*;
        match self {
            Ready => f.write_str("READY"),
            Error(err) => write!(f, "ERR {err}"),
            Usage(method) => write!(f, "Err {}", method.usage()),
            RevisionCreated(rev) => write!(f, "OK {rev}"),
            FileContent(content) => write!(f, "OK {} ({})", content.len(), Ellipsis::new(content)),
            DirectoryListing(listing) => {
                write!(f, "OK {} [", listing.len())?;
                for (name, entry) in listing.iter() {
                    write!(f, "({name} {entry})")?;
                }
                f.write_str("]")
            }
        }
    }
}

impl fmt::Debug for Response {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Ready => write!(f, "Ready"),
            Self::Error(err) => f.debug_tuple("Error").field(err).finish(),
            Self::Usage(method) => f.debug_tuple("Usage").field(method).finish(),
            Self::RevisionCreated(rev) => f.debug_tuple("RevisionCreated").field(rev).finish(),
            Self::FileContent(content) => f
                .debug_tuple("FileContent")
                .field(&Ellipsis::new(content))
                .finish(),
            Self::DirectoryListing(listing) => {
                f.debug_tuple("DirectoryListing").field(listing).finish()
            }
        }
    }
}
