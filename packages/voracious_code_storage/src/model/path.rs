use std::fmt;

use internment::ArcIntern;
use lazy_static::lazy_static;

use super::Error;

// ======================================== Path ========================================

pub trait Path
where
    Self: Sized,
{
    fn parse<B: AsRef<[u8]>>(path: B) -> Result<Self, Error>;
    fn name(&self) -> &Name;
    fn parents(&self) -> impl Iterator<Item = &Name>;
}

// ======================================== FilePath ========================================

#[derive(Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct FilePath {
    parent: DirPath,
    name: Name,
}

impl FilePath {
    #[cfg(test)]
    fn new(mut names: Vec<Name>) -> Self {
        let name = names.pop().unwrap();
        Self {
            name,
            parent: DirPath::new(names),
        }
    }
}

impl Path for FilePath {
    fn name(&self) -> &Name {
        &self.name
    }

    fn parents(&self) -> impl Iterator<Item = &Name> {
        NameIter::all(&self.parent)
    }

    fn parse<B: AsRef<[u8]>>(input: B) -> Result<Self, Error> {
        let mut names = Name::split(input)?;
        let name = names.pop().ok_or(Error::InvalidFilePath)?;
        if !name.is_empty() {
            Ok(Self {
                parent: DirPath::new(names),
                name,
            })
        } else {
            Err(Error::InvalidFilePath)
        }
    }
}

impl fmt::Display for FilePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.parent, f)?;
        fmt::Display::fmt(&self.name, f)
    }
}

impl fmt::Debug for FilePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "FilePath(\"{}\")", self)
    }
}

// ======================================== DirPath ========================================

#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Hash)]
pub enum DirPath {
    Root,
    Dir { parents: Vec<Name>, name: Name },
}

impl DirPath {
    fn new(mut names: Vec<Name>) -> Self {
        match names.pop() {
            Some(name) => Self::Dir {
                parents: names,
                name,
            },
            None => Self::Root,
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = &Name> {
        NameIter::all(self)
    }
}

impl Path for DirPath {
    fn name(&self) -> &Name {
        match self {
            Self::Root => Name::root(),
            Self::Dir { name, .. } => name,
        }
    }

    fn parents(&self) -> impl Iterator<Item = &Name> {
        NameIter::parents(self)
    }

    fn parse<B: AsRef<[u8]>>(input: B) -> Result<Self, Error> {
        let mut names = Name::split(input)?;
        if names.last().is_some_and(Name::is_empty) {
            names.pop();
        }
        Ok(Self::new(names))
    }
}

impl fmt::Display for DirPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("/")?;
        for part in self.iter() {
            write!(f, "{part}/")?;
        }
        Ok(())
    }
}

impl fmt::Debug for DirPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "DirPath(\"{}\")", self)
    }
}

// ======================================== NameIter ========================================

pub struct NameIter<'a> {
    parents: Option<std::slice::Iter<'a, Name>>,
    name: Option<&'a Name>,
}

impl<'a> NameIter<'a> {
    fn all(path: &'a DirPath) -> Self {
        match path {
            DirPath::Root => Self {
                parents: None,
                name: None,
            },
            DirPath::Dir { parents, name } => Self {
                parents: Some(parents.iter()),
                name: Some(name),
            },
        }
    }

    fn parents(path: &'a DirPath) -> Self {
        Self {
            parents: match path {
                DirPath::Root => None,
                DirPath::Dir { parents, .. } => Some(parents.iter()),
            },
            name: None,
        }
    }
}

impl<'a> Iterator for NameIter<'a> {
    type Item = &'a Name;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(ref mut parents) = self.parents {
            if let Some(next) = parents.next() {
                return Some(next);
            } else {
                self.parents = None;
            }
        }
        self.name.take()
    }
}

// ======================================== Name ========================================

#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Hash)]
pub struct Name(ArcIntern<[u8]>);

impl Name {
    const SEPARATOR: u8 = b'/';

    fn root() -> &'static Self {
        lazy_static! {
            static ref ROOT: Name = Name::new(&[0; 0][..]);
        };
        &ROOT
    }

    fn new(name: &[u8]) -> Self {
        Self(ArcIntern::from(name))
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    fn is_valid_character(b: &u8) -> bool {
        b.is_ascii_alphanumeric() || matches!(*b, b'_' | b'-' | b'.')
    }

    fn is_separator(b: &u8) -> bool {
        Self::SEPARATOR.eq(b)
    }

    pub fn parse<B: AsRef<[u8]>>(value: B) -> Result<Self, Error> {
        let bytes = value.as_ref();
        if bytes.iter().all(Self::is_valid_character) {
            Ok(Self::new(bytes))
        } else {
            Err(Error::invalid_file_name(value))
        }
    }

    fn split<B: AsRef<[u8]>>(input: B) -> Result<Vec<Name>, Error> {
        let mut names = input
            .as_ref()
            .split(Name::is_separator)
            .map(Name::parse)
            .collect::<Result<Vec<_>, _>>()?;

        if !names.first().is_some_and(Name::is_empty) {
            return Err(Error::invalid_path(input));
        }
        names.remove(0);

        Ok(names)
    }
}

impl AsRef<[u8]> for Name {
    fn as_ref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

#[cfg(test)]
impl<const N: usize> PartialEq<[u8; N]> for Name {
    fn eq(&self, other: &[u8; N]) -> bool {
        self.0.as_ref().eq(other.as_ref())
    }
}

impl fmt::Debug for Name {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, r#"b"{}""#, self)
    }
}

impl fmt::Display for Name {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_ref().escape_ascii())
    }
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! dirpath {
        ( $( $name:literal ),* ) => {
            DirPath::new(vec![ $( Name::parse(&$name[..]).unwrap() ),* ])
        };
    }

    macro_rules! filepath {
        ( $( $name:literal ),* ) => {
            FilePath::new(vec![ $( Name::parse(&$name[..]).unwrap() ),* ])
        };
    }

    macro_rules! test_parse {
        ( $test_name:ident : $subtype:ident, $input:literal => $expected:expr ) => {
            #[test]
            fn $test_name() {
                assert_eq!(
                    $subtype::parse(&$input[..]).expect("should success"),
                    $expected
                );
            }
        };
    }

    test_parse! { root_path: DirPath, "/" => dirpath![] }

    test_parse! { some_dir_path: DirPath, "/some/path" => dirpath![b"some", b"path"] }

    test_parse! { some_dir_path_with_trailing_slash: DirPath, "/some/path/" => dirpath![b"some", b"path"] }

    test_parse! { file_at_root_path: FilePath, "/foobar.txt" => filepath![b"foobar.txt"] }

    test_parse! { some_file: FilePath, "/subdir/foobar.txt" => filepath![b"subdir", b"foobar.txt"] }

    #[test]
    fn invalid_file_path_trailing_slash() {
        FilePath::parse("/some/path/").expect_err("should fail");
    }

    #[test]
    fn invalid_file_path_relative() {
        FilePath::parse("some/path").expect_err("should fail");
    }

    #[test]
    fn invalid_path_whitespace() {
        FilePath::parse("som ath").expect_err("should fail");
    }

    #[test]
    fn root_has_no_parent() {
        let dirpath = DirPath::Root;

        let parents = dirpath.parents().collect::<Vec<_>>();

        assert!(parents.is_empty());
        assert_eq!(dirpath.name(), b"");
    }

    #[test]
    fn directory_with_two_parents() {
        let dirpath = DirPath::Dir {
            parents: vec![
                Name::parse("parent1").unwrap(),
                Name::parse("parent2").unwrap(),
            ],
            name: Name::parse("directory").unwrap(),
        };

        let parents = dirpath.parents().map(|n| n.as_ref()).collect::<Vec<_>>();

        assert_eq!(parents, [b"parent1", b"parent2"]);
        assert_eq!(dirpath.name(), b"directory");
    }

    #[test]
    fn parents_of_file_in_root() {
        let filepath = FilePath {
            parent: DirPath::Root,
            name: Name::parse("file").unwrap(),
        };

        let parents = filepath.parents().collect::<Vec<_>>();

        assert!(parents.is_empty());
        assert_eq!(filepath.name(), b"file");
    }

    #[test]
    fn parents_of_file_with_two_parents() {
        let filepath = FilePath {
            parent: DirPath::Dir {
                parents: vec![
                    Name::parse("parent1").unwrap(),
                    Name::parse("parent2").unwrap(),
                ],
                name: Name::parse("directory").unwrap(),
            },
            name: Name::parse("file").unwrap(),
        };

        let parents = filepath.parents().map(|n| n.as_ref()).collect::<Vec<_>>();

        assert_eq!(
            parents,
            [&b"parent1"[..], &b"parent2"[..], &b"directory"[..]]
        );
        assert_eq!(filepath.name(), b"file");
    }

    #[test]
    fn test_illegal_name() {
        assert!(Name::parse(&b"]H0KL`vb8"[..]).is_err());
    }
}
