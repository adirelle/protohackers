mod path;
mod storage;

pub use path::*;
pub use storage::*;

// ======================================== Error ========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("invalid path: {0}")]
    InvalidPath(String),

    #[error("invalid file path")]
    InvalidFilePath,

    #[error("invalid revision: {0}")]
    InvalidRevision(String),

    #[error("invalid file name: {0}")]
    InvalidFileName(String),

    #[error("no such file")]
    FileNotFound,

    #[error("no such revision")]
    RevisionNotFound,
}

impl Error {
    pub fn is_transient(&self) -> bool {
        true
    }

    pub fn invalid_file_name<B: AsRef<[u8]>>(name: B) -> Self {
        Self::InvalidFileName(format!("{}", name.as_ref().escape_ascii()))
    }

    pub fn invalid_path<B: AsRef<[u8]>>(path: B) -> Self {
        Self::InvalidPath(format!("{}", path.as_ref().escape_ascii()))
    }

    pub fn invalid_revision<V: std::fmt::Display>(value: V) -> Self {
        Self::InvalidRevision(format!("{}", value))
    }
}
