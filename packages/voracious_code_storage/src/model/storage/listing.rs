use std::collections::{btree_map, BTreeMap};
use std::fmt;

use super::{Directory, DirectoryVisitor, File, Name, Revision};

// ======================================== Listing ========================================

#[derive(Debug)]
pub struct Listing {
    entries: BTreeMap<Name, Entry>,
}

impl Listing {
    pub fn from_directory(dir: &Directory) -> Self {
        let mut builder = ListingBuilder::default();
        dir.visit(&mut builder);
        builder.build()
    }

    pub fn len(&self) -> usize {
        self.entries.len()
    }

    pub fn iter(&self) -> btree_map::Iter<'_, Name, Entry> {
        self.entries.iter()
    }
}

impl IntoIterator for Listing {
    type Item = (Name, Entry);

    type IntoIter = btree_map::IntoIter<Name, Entry>;

    fn into_iter(self) -> Self::IntoIter {
        self.entries.into_iter()
    }
}

// ======================================== Entry ========================================

#[derive(Debug, Default)]
pub(crate) struct ListingBuilder {
    entries: BTreeMap<Name, Entry>,
}

impl ListingBuilder {
    pub fn build(self) -> Listing {
        Listing {
            entries: self.entries,
        }
    }

    pub fn insert_dir(&mut self, name: &Name) {
        self.entries.insert(name.clone(), Entry::Directory);
    }

    pub fn insert_file(&mut self, name: &Name, revision: Revision) {
        self.entries.insert(name.clone(), Entry::File(revision));
    }
}

impl DirectoryVisitor for ListingBuilder {
    fn visit_directory(&mut self, name: &Name, _directory: &Directory) {
        self.insert_dir(name);
    }

    fn visit_file(&mut self, name: &Name, file: &File) {
        self.insert_file(name, file.latest_revision());
    }
}

// ======================================== Entry ========================================

#[derive(Debug, PartialEq, Eq)]
pub enum Entry {
    File(Revision),
    Directory,
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Directory => f.write_str("DIR"),
            Self::File(rev) => rev.fmt(f),
        }
    }
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn listing_of_empty_directoy() {
        let dir = Directory::default();

        let listing = Listing::from_directory(&dir);

        assert_eq!(listing.len(), 0);
    }

    #[test]
    fn listing_of_simple_directory() {
        let mut dir = Directory::default();

        let file1 = &Name::parse("000").unwrap();
        let file2 = &Name::parse("001").unwrap();
        let dir1 = &Name::parse("002").unwrap();

        dir.file_mut(file2).or_insert(File::fake(1));
        dir.dir_mut(dir1);
        dir.file_mut(file1).or_insert(File::fake(2));

        let listing = Listing::from_directory(&dir);

        assert_eq!(listing.len(), 3);

        let mut iter = listing.iter();
        assert_eq!(
            iter.next(),
            Some((file1, &Entry::File(2.try_into().unwrap())))
        );
        assert_eq!(
            iter.next(),
            Some((file2, &Entry::File(1.try_into().unwrap())))
        );
        assert_eq!(iter.next(), Some((dir1, &Entry::Directory)));
    }

    #[test]
    fn listing_with_overlapping_names() {
        let mut dir = Directory::default();

        let file1 = &Name::parse("000").unwrap();
        let file2 = &Name::parse("001").unwrap();
        let dir1 = &Name::parse("001").unwrap();

        dir.file_mut(file2).or_insert(File::fake(1));
        dir.dir_mut(dir1);
        dir.file_mut(file1).or_insert(File::fake(2));

        let listing = Listing::from_directory(&dir);

        assert_eq!(listing.len(), 2);

        let mut iter = listing.iter();
        assert_eq!(
            iter.next(),
            Some((file1, &Entry::File(2.try_into().unwrap())))
        );
        assert_eq!(iter.next(), Some((dir1, &Entry::Directory)));
    }
}
