mod listing;

use std::collections::hash_map;
use std::num::NonZeroUsize;
use std::{collections::HashMap, fmt};

use bytes::Bytes;
use internment::ArcIntern;
use ring::digest;

use super::{DirPath, Error, FilePath, Name, Path};
pub use listing::*;

// ======================================== Storage ========================================

#[derive(Debug, Default)]
pub struct Storage {
    root: Directory,
    contents: HashMap<Content, Bytes>,
}

impl Storage {
    pub fn put(&mut self, filepath: &FilePath, content: Bytes) -> Result<Revision, Error> {
        let content = self.store(content);
        let dir = filepath.parents().fold(&mut self.root, Directory::dir_mut);
        use hash_map::Entry::*;
        Ok(match dir.file_mut(filepath.name()) {
            Occupied(mut occupied) => occupied.get_mut().put(content),
            Vacant(vacant) => vacant.insert(File::new(content)).latest_revision(),
        })
    }

    pub fn get(&self, filepath: &FilePath, revision: Revision) -> Result<&Bytes, Error> {
        self.get_file(filepath)?
            .revision(revision)
            .map(|content| self.fetch(content))
            .ok_or(Error::RevisionNotFound)
    }

    pub fn get_latest(&self, filepath: &FilePath) -> Result<(Revision, &Bytes), Error> {
        let (rev, content) = self.get_file(filepath)?.latest();
        Ok((rev, self.fetch(content)))
    }

    pub fn list(&self, dirpath: &DirPath) -> Result<Listing, Error> {
        dirpath
            .iter()
            .try_fold(&self.root, Directory::dir)
            .ok_or(Error::FileNotFound)
            .map(Listing::from_directory)
    }

    fn get_file(&self, filepath: &FilePath) -> Result<&File, Error> {
        filepath
            .parents()
            .try_fold(&self.root, Directory::dir)
            .and_then(|dir| dir.file(filepath.name()))
            .ok_or(Error::FileNotFound)
    }

    fn store(&mut self, data: Bytes) -> Content {
        let content = Content::from_data(&data);
        self.contents.entry(content.clone()).or_insert(data);
        content
    }

    fn fetch<'c>(&'c self, content: &'c Content) -> &'c Bytes {
        self.contents.get(content).unwrap()
    }
}

// ======================================== Content ========================================

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct Content(ArcIntern<Vec<u8>>);

impl Content {
    pub fn is_valid_character(b: &u8) -> bool {
        matches!(b, 0x20..=0x7E | b'\n' | b'\r' | b'\t')
    }

    fn from_data<B: AsRef<[u8]>>(data: &B) -> Self {
        let sha256 = digest::digest(&digest::SHA256, data.as_ref());
        Self(ArcIntern::from_ref(sha256.as_ref()))
    }
}

#[cfg(test)]
#[allow(clippy::derivable_impls)]
impl Default for Content {
    fn default() -> Self {
        Self(Default::default())
    }
}

// ======================================== File ========================================

#[derive(Debug)]
pub struct File {
    revisions: Vec<Content>,
}

impl File {
    fn new(content: Content) -> Self {
        Self {
            revisions: vec![content],
        }
    }

    fn put(&mut self, content: Content) -> Revision {
        if &content != self.revisions.last().unwrap() {
            self.revisions.push(content);
        }
        self.latest_revision()
    }

    fn revision(&self, rev: Revision) -> Option<&Content> {
        self.revisions.get(usize::from(rev) - 1)
    }

    fn latest(&self) -> (Revision, &Content) {
        let latest = self.latest_revision();
        (latest, self.revision(latest).unwrap())
    }

    fn latest_revision(&self) -> Revision {
        Revision::try_from(self.revisions.len())
            .expect("File are created with at least one revision")
    }

    #[cfg(test)]
    pub(crate) fn fake(count: usize) -> Self {
        let mut this = Self::new(Content::default());
        for _ in 1..count {
            this.revisions.push(Content::default());
        }
        this
    }
}

// ======================================== Directory ========================================

#[derive(Debug, Default)]
pub struct Directory {
    directories: HashMap<Name, Directory>,
    files: HashMap<Name, File>,
}

impl Directory {
    fn file_mut(&mut self, name: &Name) -> hash_map::Entry<'_, Name, File> {
        self.files.entry(name.clone())
    }

    fn file(&self, name: &Name) -> Option<&File> {
        self.files.get(name)
    }

    fn dir_mut(&mut self, name: &Name) -> &mut Directory {
        self.directories.entry(name.clone()).or_default()
    }

    fn dir(&self, name: &Name) -> Option<&Directory> {
        self.directories.get(name)
    }

    fn visit<V: DirectoryVisitor>(&self, visitor: &mut V) {
        self.files
            .iter()
            .for_each(|(name, file)| visitor.visit_file(name, file));
        self.directories
            .iter()
            .for_each(|(name, dir)| visitor.visit_directory(name, dir));
    }
}

// ======================================== DirectoryVisitor ========================================

pub trait DirectoryVisitor {
    fn visit_directory(&mut self, name: &Name, directory: &Directory);
    fn visit_file(&mut self, name: &Name, file: &File);
}

// ======================================== Revision ========================================

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct Revision(NonZeroUsize);

impl fmt::Display for Revision {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "r{}", self.0)
    }
}

impl TryFrom<usize> for Revision {
    type Error = Error;

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        NonZeroUsize::try_from(value)
            .map(Self)
            .map_err(|_| Error::invalid_revision(value))
    }
}

impl From<NonZeroUsize> for Revision {
    fn from(value: NonZeroUsize) -> Self {
        Self(value)
    }
}

impl From<Revision> for usize {
    fn from(value: Revision) -> Self {
        value.0.into()
    }
}

impl From<&Revision> for usize {
    fn from(value: &Revision) -> Self {
        value.0.into()
    }
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! dirpath {
        ( $name:expr ) => {
            DirPath::parse($name).expect("should be a valid DirPath")
        };
    }

    macro_rules! filepath {
        ( $name:expr ) => {
            FilePath::parse($name).expect("should be a valid FilePath")
        };
    }

    #[test]
    fn do_not_create_revision_on_identical_content() {
        let mut storage = Storage::default();
        let content = storage.store(Bytes::from_static(b"content"));
        let mut file = File::new(content.clone());

        let r1 = file.latest_revision();
        let r2 = file.put(content);

        assert_eq!(r1, r2);
    }

    #[test]
    fn store_and_retrieve_file_in_root() {
        let mut storage = Storage::default();
        let path = filepath!("/test");
        let content = Bytes::from_static(b"content");

        let rev = storage.put(&path, content.clone()).expect("should success");
        let actual = storage.get(&path, rev).expect("should success");

        assert_eq!(actual, &content);
    }

    #[test]
    fn store_and_retrieve_file_in_subdirectory() {
        let mut storage = Storage::default();
        let path = filepath!("/some/path/test");
        let content = Bytes::from_static(b"content");

        let rev = storage.put(&path, content.clone()).expect("should success");
        let actual = storage.get(&path, rev).expect("should success");

        assert_eq!(actual, &content);
    }

    #[test]
    fn retrieve_several_revisions() {
        let mut storage = Storage::default();
        let path = filepath!("/some/path/test");
        let content1 = Bytes::from_static(b"content1");
        let content2 = Bytes::from_static(b"content2");

        let rev1 = storage
            .put(&path, content1.clone())
            .expect("should success");
        let rev2 = storage
            .put(&path, content2.clone())
            .expect("should success");

        let previous = storage.get(&path, rev1).expect("should success");
        let (latest_rev, latest_content) = storage.get_latest(&path).expect("should success");

        assert_eq!(previous, &content1);
        assert_eq!(latest_rev, rev2);
        assert_eq!(latest_content, &content2);
    }

    #[test]
    fn list_root() {
        let mut storage = Storage::default();

        for path in [
            "/file1",
            "/file2",
            "/file3",
            "/dir1/file11",
            "/dir1/file12",
            "/dir2/file21",
        ] {
            storage
                .put(&filepath!(path), Bytes::new())
                .expect("should success");
        }

        let listing = storage.list(&dirpath!("/")).expect("should success");

        assert_eq!(listing.len(), 5, "should contains 5 entries: {listing:?}");
    }

    #[test]
    fn list_directory() {
        let mut storage = Storage::default();

        for path in [
            "/file1",
            "/file2",
            "/file3",
            "/dir1/file11",
            "/dir1/file12",
            "/dir2/file21",
        ] {
            storage
                .put(&filepath!(path), Bytes::new())
                .expect("should success");
        }

        let listing = storage.list(&dirpath!("/dir1")).expect("should success");

        assert_eq!(listing.len(), 2, "should contains 2 entries: {listing:?}");
    }
}
