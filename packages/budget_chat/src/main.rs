mod room;

use std::fmt::Write;
use std::ops::AddAssign;

use bstr::{BString, ByteSlice};
use room::*;
use server::prelude::*;
use tokio::select;

#[tokio::main]
async fn main() -> Result<()> {
    tracing::subscriber::set_global_default(tracing_subscriber::FmtSubscriber::new())?;

    let room = Room::new();

    let mut spy = room.receiver();
    tokio::spawn(async move {
        loop {
            match spy.recv().await {
                Ok(msg) => tracing::info!("message: {:?}", msg),
                Err(err) => {
                    tracing::info!("error spying: {}", err);
                    break;
                }
            }
        }
    });

    server::serve(Server { room }, "0.0.0.0:20000".parse()?).await
}

#[derive(Debug, Default, Clone)]
struct Stats {
    received_bytes: usize,
    received_lines: usize,
    sent_bytes: usize,
    sent_lines: usize,
    rejected_usernames: usize,
}

impl AddAssign for Stats {
    fn add_assign(&mut self, rhs: Self) {
        self.received_bytes += rhs.received_bytes;
        self.received_lines += rhs.received_lines;
        self.sent_bytes += rhs.sent_bytes;
        self.sent_lines += rhs.sent_lines;
        self.rejected_usernames += rhs.rejected_usernames;
    }
}

struct Server {
    room: Room,
}

impl TCPHandlerFactory for Server {
    type Handler = Connection;
    type Stats = Stats;

    fn create(&mut self, socket: TcpStream, _client_addr: SocketAddr) -> Result<Self::Handler> {
        Ok(Connection::new(socket, self.room.clone()))
    }
}

struct Connection {
    stream: BufStream<TcpStream>,
    room: Room,
    read_buf: Vec<u8>,
    stats: Stats,
}

impl Connection {
    fn new(socket: TcpStream, room: Room) -> Self {
        Self {
            stream: BufStream::new(socket),
            room,
            read_buf: Vec::with_capacity(128),
            stats: Stats::default(),
        }
    }

    async fn read_line(&mut self) -> Result<Option<BString>> {
        let read = self.stream.read_until(b'\n', &mut self.read_buf).await?;
        tracing::info!(
            "received {} bytes, buffer size: {}",
            read,
            self.read_buf.len()
        );
        if read == 0 && self.read_buf.is_empty() {
            return Ok(None);
        }
        self.stats.received_lines += 1;
        self.stats.received_bytes += read;

        let msg = BString::from(self.read_buf.trim_end());
        self.read_buf.clear();
        tracing::info!("received {:?}", msg);
        Ok(Some(msg))
    }

    async fn write<S: AsRef<[u8]>>(&mut self, line: S) -> Result<()> {
        self.stream.write_all(line.as_ref()).await?;
        self.stream.flush().await?;
        self.stats.sent_bytes += line.as_ref().len();
        self.stats.sent_lines += 1;

        Ok(())
    }

    async fn login(&mut self) -> Result<Username> {
        self.write("Welcome to budget_chat! What is your username?\n")
            .await?;

        if let Some(answer) = self.read_line().await? {
            match answer.try_into() {
                Ok(username) => Ok(username),
                Err(err) => {
                    let msg = format!("Invalid username: {}\n", err);
                    self.write(msg).await?;
                    self.stats.rejected_usernames += 1;
                    Err(err)
                }
            }
        } else {
            Err(anyhow!("user disconnected"))
        }
    }

    async fn welcome(&mut self, members: Vec<Username>) -> Result<()> {
        let mut msg = String::new();
        if members.is_empty() {
            writeln!(msg, "* the room in empty.")?;
        } else {
            let mut iter = members.iter();
            write!(
                msg,
                "* these people are in the room: {}",
                iter.next().unwrap()
            )?;
            iter.try_for_each(|m| write!(&mut msg, ", {m}"))?;
            writeln!(&mut msg, ".")?;
        }

        self.write(msg).await.map_err(Into::into)
    }

    async fn chat(&mut self, member: &mut Member) -> Result<()> {
        loop {
            select!(
                recv = member.receive() => {
                    let msg = recv?;
                    if msg.source() != member.name() {
                        let buf = format!("{}", msg);
                        self.write(buf).await?;
                    }
                },
                input = self.read_line() => {
                    if let Some(msg) = input? {
                        member.says(msg).await?;
                    } else {
                        break Ok(());
                    }
                }
            )
        }
    }
}

#[async_trait]
impl TCPHandler<Stats> for Connection {
    async fn handle(mut self, _until: CancellationToken) -> Result<Stats> {
        let username = self.login().await?;

        let (mut member, members) = self.room.join(username).await?;

        let res = {
            self.welcome(members).await?;
            self.chat(&mut member).await
        };

        member.leave().await?;

        res.and(Ok(self.stats.clone()))
    }
}
