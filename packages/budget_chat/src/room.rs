use std::collections::BTreeSet;
use std::fmt::{self, Debug, Display};
use std::hash::Hash;
use std::sync::Arc;

use async_broadcast::*;
use bstr::{BString, ByteSlice};
use server::prelude::*;
use tokio::sync::Mutex;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub struct Username(Arc<BString>);

impl TryFrom<BString> for Username {
    type Error = Error;

    fn try_from(value: BString) -> Result<Self> {
        if value.is_empty() {
            return Err(anyhow!("at least one character is requried"));
        }
        if let Some(rejected) = value
            .chars()
            .find(|b| !matches!(b, '0'..='9' | 'A'..='Z' | 'a'..='z'))
        {
            return Err(anyhow!(
                "only digits, lower- and uppercase letters are allowed, found {}",
                rejected
            ));
        }

        Ok(Username(Arc::new(value)))
    }
}

impl Display for Username {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        (&self.0 as &dyn std::fmt::Display).fmt(f)
    }
}

impl PartialEq<&Username> for Username {
    fn eq(&self, other: &&Username) -> bool {
        self.eq(*other)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Message {
    Joined(Username),
    Left(Username),
    Said(Username, Arc<BString>),
}

impl Message {
    pub fn source(&self) -> &Username {
        use Message::*;
        match self {
            Joined(name) => name,
            Left(name) => name,
            Said(name, _) => name,
        }
    }
}

impl Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Message::*;
        match self {
            Joined(name) => writeln!(f, "* {name} has entered the room."),
            Left(name) => writeln!(f, "* {name} has left the room."),
            Said(name, msg) => writeln!(f, "[{name}] {msg}"),
        }
    }
}

#[derive(Debug)]
pub struct Room {
    members: Arc<Mutex<BTreeSet<Username>>>,
    sender: Sender<Message>,
    receiver: Receiver<Message>,
}

impl Clone for Room {
    fn clone(&self) -> Self {
        Self {
            members: self.members.clone(),
            sender: self.sender.clone(),
            receiver: self.receiver.new_receiver(),
        }
    }
}

impl Room {
    pub fn new() -> Self {
        let (sender, receiver) = broadcast(100);
        Self {
            members: Arc::new(Mutex::new(BTreeSet::new())),
            sender,
            receiver,
        }
    }

    pub fn receiver(&self) -> Receiver<Message> {
        self.receiver.clone()
    }

    pub async fn join(&self, name: Username) -> Result<(Member, Vec<Username>)> {
        let previous_members = {
            let mut members = self.members.lock().await;
            if members.contains(&name) {
                return Err(anyhow!("username already in use: {}", name));
            }

            let previous_members: Vec<Username> = members.iter().cloned().collect();

            members.insert(name.clone());
            previous_members
        };

        self.sender.broadcast(Message::Joined(name.clone())).await?;

        Ok((
            Member {
                name,
                room: self.clone(),
            },
            previous_members,
        ))
    }

    async fn leave(&self, name: Username) -> Result<()> {
        {
            let mut members = self.members.lock().await;
            _ = members.remove(&name);
        }

        self.sender.broadcast(Message::Left(name)).await?;

        Ok(())
    }
}

#[derive(Debug)]
pub struct Member {
    name: Username,
    room: Room,
}

impl Member {
    pub fn name(&self) -> &Username {
        &self.name
    }

    pub async fn says(&self, message: BString) -> Result<()> {
        self.room
            .sender
            .broadcast(Message::Said(self.name.clone(), Arc::new(message)))
            .await?;
        Ok(())
    }

    pub async fn receive(&mut self) -> Result<Message> {
        self.room.receiver.recv().await.map_err(Into::into)
    }

    pub async fn leave(self) -> Result<()> {
        self.room.leave(self.name).await.map_err(Into::into)
    }
}

impl PartialOrd for Member {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.name.partial_cmp(&other.name)
    }
}

impl PartialEq for Member {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Hash for Member {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state)
    }
}
