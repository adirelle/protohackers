use std::mem::size_of;

use bytes::{Buf, BufMut, BytesMut};
use tokio_util::codec::{Decoder, Encoder};

use super::{Message, PolicyAction, PolicyId, PopulationCount, PopulationTargets, SiteId, Species};

use crate::protocol::serde::{self, *};

// ======================================== Codec ========================================

#[derive(Debug)]
pub struct Codec;

impl Codec {
    const PROTOCOL: &'static str = "pestcontrol";
    const VERSION: u32 = 1;
    const OVERHEAD: usize = 2 * size_of::<u8>() + size_of::<u32>();
    const MAX_LENGTH: usize = 0xFFFF;
    const VALID_CHECKSUM: u8 = 0;
}

impl Decoder for Codec {
    type Item = Message;
    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if src.len() < Self::OVERHEAD {
            return Ok(None);
        }

        let mut buf = src.clone();
        let msg_type = MessageType::deserialize(&mut buf)?;
        let length = usize::deserialize(&mut buf)?;
        if length > Self::MAX_LENGTH {
            Err(Error::MessageTooLong(length))
        } else if src.len() < length {
            Ok(None)
        } else if src.split_to(length).into_iter().reduce(u8::wrapping_add)
            != Some(Self::VALID_CHECKSUM)
        {
            Err(Error::InvalidChecksum)
        } else {
            let mut buf = buf.take(length - Self::OVERHEAD);
            match msg_type.deserialize_body(&mut buf) {
                Err(Error::Serde(serde::Error::UnexpectedEOF)) => Err(Error::InvalidLength),
                Ok(_) if buf.has_remaining() => Err(Error::InvalidLength),
                Ok(msg) => Ok(Some(msg)),
                Err(error) => Err(error),
            }
        }
    }
}

impl Encoder<Message> for Codec {
    type Error = Error;

    fn encode(&mut self, item: Message, dst: &mut BytesMut) -> Result<(), Self::Error> {
        let mut body = BytesMut::new();
        let msg_type = MessageType::serialize_body(item, &mut body)?;

        msg_type.serialize(dst)?;
        (body.len() + Self::OVERHEAD).serialize(dst)?;
        dst.put(body);

        let sum = dst.clone().into_iter().reduce(u8::wrapping_add).unwrap();
        Self::VALID_CHECKSUM.wrapping_sub(sum).serialize(dst)?;

        Ok(())
    }
}

// ======================================== MessageType ========================================

enum MessageType {
    Hello,
    Error,
    Ok,
    DialAuthority,
    TargetPopulations,
    CreatePolicy,
    DeletePolicy,
    PolicyResult,
    SiteVisit,
}

impl Deserialize for MessageType {
    type Error = Error;

    fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Error> {
        Ok(match u8::deserialize(src)? {
            Self::HELLO => MessageType::Hello,
            Self::ERROR => MessageType::Error,
            Self::OK => MessageType::Ok,
            Self::DIAL_AUTHORITY => MessageType::DialAuthority,
            Self::TARGET_POPULATIONS => MessageType::TargetPopulations,
            Self::CREATE_POLICY => MessageType::CreatePolicy,
            Self::DELETE_POLICY => MessageType::DeletePolicy,
            Self::POLICY_RESULT => MessageType::PolicyResult,
            Self::SITE_VISIT => MessageType::SiteVisit,
            other => return Err(Error::InvalidMessageType(other)),
        })
    }
}

impl Serialize for MessageType {
    type Error = <u8 as Serialize>::Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), <u8 as Serialize>::Error> {
        use MessageType::*;
        match self {
            Hello => Self::HELLO,
            Error => Self::ERROR,
            Ok => Self::OK,
            DialAuthority => Self::DIAL_AUTHORITY,
            TargetPopulations => Self::TARGET_POPULATIONS,
            CreatePolicy => Self::CREATE_POLICY,
            DeletePolicy => Self::DELETE_POLICY,
            PolicyResult => Self::POLICY_RESULT,
            SiteVisit => Self::SITE_VISIT,
        }
        .serialize(dst)
    }
}

impl MessageType {
    const HELLO: u8 = 0x50;
    const ERROR: u8 = 0x51;
    const OK: u8 = 0x52;
    const DIAL_AUTHORITY: u8 = 0x53;
    const TARGET_POPULATIONS: u8 = 0x54;
    const CREATE_POLICY: u8 = 0x55;
    const DELETE_POLICY: u8 = 0x56;
    const POLICY_RESULT: u8 = 0x57;
    const SITE_VISIT: u8 = 0x58;

    fn deserialize_body<B: Buf>(self, src: &mut B) -> Result<Message, Error> {
        Ok(match self {
            Self::Hello => {
                let protocol = String::deserialize(src)?;
                let version = u32::deserialize(src)?;
                if protocol != Codec::PROTOCOL || version != Codec::VERSION {
                    return Err(Error::InvalidProcotol(protocol, version));
                }
                Message::Hello
            }
            Self::Ok => Message::Ok,
            Self::Error => Message::Error(String::deserialize(src)?),
            Self::DialAuthority => {
                let site = SiteId::deserialize(src)?;
                Message::DialAuthority(site)
            }
            Self::TargetPopulations => {
                let site = SiteId::deserialize(src)?;
                let targets = PopulationTargets::deserialize(src)?;
                Message::TargetPopulations(site, targets)
            }
            Self::CreatePolicy => {
                let species = Species::deserialize(src)?;
                let action = PolicyActionType::from_byte(u8::deserialize(src)?)?;
                Message::CreatePolicy(species, action)
            }
            Self::DeletePolicy => {
                let policy = PolicyId::deserialize(src)?;
                Message::DeletePolicy(policy)
            }
            Self::PolicyResult => {
                let policy = PolicyId::deserialize(src)?;
                Message::PolicyResult(policy)
            }
            Self::SiteVisit => {
                let site = SiteId::deserialize(src)?;
                let counts = PopulationCount::deserialize(src)?;
                Message::SiteVisit(site, counts)
            }
        })
    }

    fn serialize_body<B: BufMut>(msg: Message, dst: &mut B) -> Result<Self, Error> {
        Ok(match msg {
            Message::Hello => {
                Codec::PROTOCOL.serialize(dst)?;
                Codec::VERSION.serialize(dst)?;
                Self::Hello
            }
            Message::Error(reason) => {
                reason.serialize(dst)?;
                Self::Error
            }
            Message::Ok => Self::Ok,
            Message::DialAuthority(site) => {
                site.serialize(dst)?;
                Self::DialAuthority
            }
            Message::TargetPopulations(site, targets) => {
                site.serialize(dst)?;
                targets.serialize(dst)?;
                Self::TargetPopulations
            }
            Message::CreatePolicy(species, action) => {
                species.serialize(dst)?;
                PolicyActionType::as_byte(action).serialize(dst)?;
                Self::CreatePolicy
            }
            Message::DeletePolicy(policy) => {
                policy.serialize(dst)?;
                Self::DeletePolicy
            }
            Message::PolicyResult(policy) => {
                policy.serialize(dst)?;
                Self::PolicyResult
            }
            Message::SiteVisit(site, counts) => {
                site.serialize(dst)?;
                counts.serialize(dst)?;
                Self::SiteVisit
            }
        })
    }
}

// ======================================== PolicyActionType ========================================

struct PolicyActionType;

impl PolicyActionType {
    const CULL: u8 = 0x90;
    const CONSERVE: u8 = 0xA0;

    fn from_byte(byte: u8) -> Result<PolicyAction, Error> {
        use PolicyAction::*;
        Ok(match byte {
            Self::CULL => Cull,
            Self::CONSERVE => Conserve,
            other => return Err(Error::InvalidPolicyAction(other)),
        })
    }

    fn as_byte(value: PolicyAction) -> u8 {
        use PolicyAction::*;
        match value {
            Cull => Self::CULL,
            Conserve => Self::CONSERVE,
        }
    }
}

// ======================================== Error ========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    IO(#[from] std::io::Error),

    #[error(transparent)]
    Serde(#[from] crate::protocol::serde::Error),

    #[error("invalid message type: {0:#02x}")]
    InvalidMessageType(u8),

    #[error("invalid policy action: {0:#02x}")]
    InvalidPolicyAction(u8),

    #[error("invalid message checksum")]
    InvalidChecksum,

    #[error("invalid message length")]
    InvalidLength,

    #[error("invalid protocol or version: {0} v{1}")]
    InvalidProcotol(String, u32),

    #[error("message too long: maximum {} bytes, got {0}", Codec::MAX_LENGTH)]
    MessageTooLong(usize),
}

impl Error {
    pub fn is_unexpected_eof(&self) -> bool {
        matches!(self, Self::Serde(err) if err.is_unexpected_eof())
    }
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests {
    use bytes::{Buf, BytesMut};
    use hex_literal::hex;

    use crate::protocol::messages::{Count, SiteId};

    use super::*;

    macro_rules! test_codec {
        ( $name:ident: $bytes:literal => $message:expr ) => {
            mod $name {
                use super::*;

                #[test]
                fn decode() {
                    let mut src = BytesMut::from_iter(hex!($bytes));

                    let message = Codec.decode(&mut src)
                        .expect("should success")
                        .expect("should yield a message");

                    assert_eq!(message, $message);
                    assert!(!src.has_remaining());
                }

                #[test]
                fn encode() {
                    let mut dst = BytesMut::new();

                    Codec.encode($message, &mut dst).expect("should success");

                    let message = Codec.decode(&mut dst)
                        .expect("should success")
                        .expect("should yield a message");

                    assert_eq!(message, $message);
                }
            }
        };
        ( $name:ident: $hex:literal => err: $( $expected:tt )* ) => {
            #[test]
            fn $name() {
                let mut input = BytesMut::from_iter(hex!($hex));
                let result = Codec
                    .decode(&mut input)
                    .expect_err("should return an error");
                assert!(
                    matches!(result, $( $expected )*),
                    "expected {}, got {result:?}",
                    stringify!($( $expected )*)
                );
            }
        };
    }

    test_codec! { hello:
        "50 00000019 0000000b 70657374636f6e74726f6c 00000001 ce"
        => Message::Hello
    }

    test_codec! { ok:
        "52 00000006 a8"
        => Message::Ok
    }

    test_codec! { error:
        "51 0000000d 00000003 626164 78"
        => Message::Error("bad".into())
    }

    test_codec! { dial_authority:
        "53 0000000a 00003039 3a"
        => Message::DialAuthority(SiteId::from(12345))
    }

    test_codec! { target_populations:
        "54 0000002c 00003039 00000002 00000003 646f67 00000001 00000003 00000003 726174 00000000 0000000a 80"
        => Message::TargetPopulations(
            SiteId::from(12345),
            PopulationTargets::from_iter([
                (Species::from("dog"), (Count::from(1), Count::from(3))),
                (Species::from("rat"), (Count::from(0), Count::from(10))),
            ])
        )
    }

    test_codec! { create_policy:
        "55 0000000e 00000003 646f67 a0 c0"
        => Message::CreatePolicy(Species::from("dog"), PolicyAction::Conserve)
    }

    test_codec! { delete_policy:
        "56 0000000a 0000007b 25"
        => Message::DeletePolicy(PolicyId::from(123))
    }

    test_codec! { policy_result:
        "57 0000000a 0000007b 24"
        => Message::PolicyResult(PolicyId::from(123))
    }

    test_codec! { site_visit:
        "58 00000024 00003039 00000002 00000003 646f67 00000001 00000003 726174 00000005 8c"
        => Message::SiteVisit(
            SiteId::from(12345),
            PopulationCount::from_iter([
                (Species::from("dog"), Count::from(1)),
                (Species::from("rat"), Count::from(5)),
            ])
        )
    }

    test_codec! { invalid_protocol:
        "50 00000019 0000000b 70657374636f6e74726f6d 00000001 cd"
        => err: Error::InvalidProcotol(ref proto, 1) if proto == "pestcontrom"
    }

    test_codec! { invalid_protocol_version:
        "50 00000019 0000000b 70657374636f6e74726f6c 00000002 cd"
        => err: Error::InvalidProcotol(ref proto, 2) if proto == "pestcontrol"
    }

    test_codec! { invalid_message_length_1:
        "50 0000001d 0000000b 70657374636f6e74726f6c 00000001 00000000 ca"
        => err: Error::InvalidLength
    }

    test_codec! { invalid_message_length_2:
        "50 00000019 0000000f 70657374636f6e74726f6c 00000001 ca"
        => err: Error::InvalidLength
    }

    test_codec! { message_too_long:
        "50 ee6b2800 0000000b 70657374636f6e74726f6c 00000001 66"
        => err: Error::MessageTooLong(4000000000)
    }

    test_codec! { invalid_checksum:
        "52 00000006 a9"
        => err: Error::InvalidChecksum
    }
}
