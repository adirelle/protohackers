mod codec;

use crate::model::*;

pub use codec::*;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Message {
    Hello,
    Error(String),
    Ok,
    DialAuthority(SiteId),
    TargetPopulations(SiteId, PopulationTargets),
    CreatePolicy(Species, PolicyAction),
    DeletePolicy(PolicyId),
    PolicyResult(PolicyId),
    SiteVisit(SiteId, PopulationCount),
}

impl Message {}

impl From<Message> for Result<Message, String> {
    fn from(value: Message) -> Self {
        if let Message::Error(reason) = value {
            Err(reason)
        } else {
            Ok(value)
        }
    }
}
