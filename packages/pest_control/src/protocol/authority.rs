use crate::model::*;

use super::conn::*;
use super::Message;

// @todo: configuration
static AUTHORITY_ADDRESS: &str = "pestcontrol.protohackers.com:20547";

#[derive(Debug)]
pub struct AuthorityServer {
    conn: Conn,
    targets: PopulationTargets,
}

impl Authority for AuthorityServer {
    type Error = Error;

    async fn dial(site: crate::model::SiteId) -> Result<Self, Self::Error> {
        let mut conn = Conn::dial(AUTHORITY_ADDRESS).await?;
        let targets = query!(conn, Message::DialAuthority(site), Message::TargetPopulations(s, targets) if s == site => targets);
        Ok(Self { conn, targets })
    }

    fn population_targets(&self) -> &crate::model::PopulationTargets {
        &self.targets
    }

    async fn create_policy(
        &mut self,
        species: Species,
        action: PolicyAction,
    ) -> Result<PolicyId, Self::Error> {
        Ok(
            query!(self.conn, Message::CreatePolicy(species, action), Message::PolicyResult(id) => id),
        )
    }

    async fn delete_policy(&mut self, policy: PolicyId) -> Result<(), Self::Error> {
        Ok(query!(self.conn, Message::DeletePolicy(policy), Message::Ok => ()))
    }
}
