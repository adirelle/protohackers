use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::hash::Hash;
use std::mem::size_of;
use std::num::TryFromIntError;

use bytes::{Buf, BufMut};

// ======================================== Deserialize ========================================

pub trait Deserialize
where
    Self: Sized,
{
    type Error;

    fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error>;
}

impl Deserialize for usize {
    type Error = Error;

    fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error> {
        usize::try_from(u32::deserialize(src)?).map_err(Into::into)
    }
}

impl Deserialize for String {
    type Error = Error;

    fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error> {
        let size = usize::deserialize(src)?;
        if src.remaining() >= size {
            let mut slice = vec![0_u8; size];
            src.copy_to_slice(&mut slice[..]);
            if slice.is_ascii() {
                // SAFETY: we already checked that all bytes are valid ASCII characters.
                Ok(unsafe { String::from_utf8_unchecked(slice) })
            } else {
                Err(Error::InvalidAsciiString)
            }
        } else {
            Err(Error::UnexpectedEOF)
        }
    }
}

impl<T, U> Deserialize for (T, U)
where
    T: Deserialize,
    U: Deserialize,
    Error: From<T::Error> + From<U::Error>,
{
    type Error = Error;

    fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error> {
        let a = T::deserialize(src)?;
        U::deserialize(src).map(|b| (a, b)).map_err(Into::into)
    }
}

impl<K, V> Deserialize for HashMap<K, V>
where
    K: Deserialize + Eq + Hash,
    V: Deserialize + Eq,
    Error: From<K::Error> + From<V::Error>,
{
    type Error = Error;

    fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error> {
        let size = usize::deserialize(src)?;
        let map = HashMap::with_capacity(size);
        (0..size)
            .map(|_| <(K, V)>::deserialize(src))
            .try_fold(map, |mut map, entry| {
                entry.and_then(|(key, value)| match map.entry(key) {
                    Entry::Occupied(occupied) if value.eq(occupied.get()) => Ok(map),
                    Entry::Occupied(_) => Err(Error::ConflictingMapValues),
                    Entry::Vacant(vacant) => {
                        vacant.insert(value);
                        Ok(map)
                    }
                })
            })
    }
}

// ======================================== Serialize ========================================

pub trait Serialize {
    type Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error>;
}

impl Serialize for usize {
    type Error = Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
        u32::try_from(*self)?.serialize(dst)
    }
}

impl<T> Serialize for &[T]
where
    T: Serialize,
    Error: From<T::Error>,
{
    type Error = Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
        self.len().serialize(dst)?;
        self.iter()
            .try_for_each(|item| item.serialize(dst))
            .map_err(Into::into)
    }
}

impl<T, U> Serialize for (T, U)
where
    T: Serialize,
    U: Serialize,
    Error: From<T::Error> + From<U::Error>,
{
    type Error = Error;
    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
        self.0.serialize(dst)?;
        self.1.serialize(dst).map_err(Into::into)
    }
}

impl Serialize for String {
    type Error = Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
        self.as_str().serialize(dst)
    }
}

impl Serialize for &str {
    type Error = Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
        self.len().serialize(dst)?;
        dst.put(self.as_bytes());
        Ok(())
    }
}

impl<K, V> Serialize for HashMap<K, V>
where
    K: Serialize,
    V: Serialize,
    Error: From<K::Error> + From<V::Error>,
{
    type Error = Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
        self.len().serialize(dst)?;
        self.iter().try_for_each(|(key, value)| {
            key.serialize(dst)?;
            value.serialize(dst)?;
            Ok(())
        })
    }
}

// ======================================== impls ========================================

macro_rules! impl_number_serde {
    ( $type:ty, $deserialize:ident, $serialize:ident ) => {
        impl Deserialize for $type {
            type Error = Error;

            fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error> {
                if src.remaining() >= size_of::<$type>() {
                    Ok(src.$deserialize())
                } else {
                    Err(Error::UnexpectedEOF)
                }
            }
        }

        impl Serialize for $type {
            type Error = Error;

            fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
                dst.$serialize(*self);
                Ok(())
            }
        }
    };
}

impl_number_serde! { u8,  get_u8,  put_u8 }
impl_number_serde! { i8,  get_i8 , put_i8  }
impl_number_serde! { u16, get_u16, put_u16 }
impl_number_serde! { i16, get_i16, put_i16 }
impl_number_serde! { u32, get_u32, put_u32 }
impl_number_serde! { i32, get_i32, put_i32 }
impl_number_serde! { u64, get_u64, put_u64 }
impl_number_serde! { i64, get_i64, put_i64 }

// ======================================== Error ========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("unexpected EOF")]
    UnexpectedEOF,

    #[error("invalid ASCII string")]
    InvalidAsciiString,

    #[error(transparent)]
    IntegerOverflow(#[from] TryFromIntError),

    #[error("conflicting map values")]
    ConflictingMapValues,
}

impl Error {
    pub fn is_unexpected_eof(&self) -> bool {
        matches!(self, Self::UnexpectedEOF)
    }
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests {
    use bytes::BytesMut;
    use hex_literal::hex;

    use super::*;

    #[test]
    fn duplicated_map_values() {
        let mut buf = BytesMut::from_iter(hex!("00000002 00000001 00000002 00000001 00000002"));

        let result = <HashMap<u32, u32>>::deserialize(&mut buf).expect("should success");

        assert_eq!(result.len(), 1);
        assert_eq!(result.get(&1), Some(&2));
    }

    #[test]
    fn conflicting_map_values() {
        let mut buf = BytesMut::from_iter(hex!("00000002 00000001 00000002 00000001 00000005"));

        let result = <HashMap<u32, u32>>::deserialize(&mut buf).expect_err("should fail");

        assert!(
            matches!(result, Error::ConflictingMapValues),
            "got {result:?}"
        );
    }
}
