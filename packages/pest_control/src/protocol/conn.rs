use futures::{SinkExt, StreamExt};
use tokio::net::{TcpStream, ToSocketAddrs};
use tokio_util::codec::Framed;

use crate::protocol::Message;

use super::messages::Codec;

macro_rules! expect_response {
    ( $exec:expr, $pattern:pat => $block:expr ) => {
        match $exec.await? {
            $pattern => $block,
            other => return Err(crate::protocol::Error::UnexpectedResponse(other)),
        }
    };
    ( $exec:expr, $pattern:pat if $guard:expr => $block:expr ) => {
        match $exec.await? {
            $pattern if $guard => $block,
            other => return Err(crate::protocol::Error::UnexpectedResponse(other)),
        }
    };
}

macro_rules! receive {
    ( $conn:expr, $pattern:pat => $block:expr ) => {
        crate::protocol::conn::expect_response!($conn.recv(), $pattern => $block)
    };
    ( $conn:expr, $pattern:pat if $guard:expr => $block:expr ) => {
        crate::protocol::conn::expect_response!($conn.recv(), $pattern if $guard => $block)
    };
}

macro_rules! query {
    ( $conn:expr, $request:expr, $pattern:pat => $block:expr ) => {
        crate::protocol::conn::expect_response!($conn.query($request), $pattern => $block)
    };
    ( $conn:expr, $request:expr, $pattern:pat if $guard:expr => $block:expr ) => {
        crate::protocol::conn::expect_response!($conn.query($request), $pattern  if $guard => $block)
    };
}

pub(crate) use {expect_response, query, receive};

#[derive(Debug)]
pub struct Conn {
    stream: Framed<TcpStream, Codec>,
}

impl Conn {
    pub async fn dial<A: ToSocketAddrs>(addr: A) -> Result<Self, Error> {
        let mut this = Self::new(TcpStream::connect(addr).await?);
        this.hello().await?;
        Ok(this)
    }

    pub fn new(stream: TcpStream) -> Self {
        Self {
            stream: Framed::new(stream, Codec),
        }
    }

    pub async fn hello(&mut self) -> Result<(), Error> {
        Ok(query!(self, Message::Hello, Message::Hello => ()))
    }

    pub async fn send(&mut self, msg: Message) -> Result<(), Error> {
        Ok(self.stream.send(msg).await?)
    }

    pub async fn recv(&mut self) -> Result<Message, Error> {
        match self.stream.next().await.transpose()? {
            Some(Message::Error(reason)) => Err(Error::Remote(reason)),
            Some(response) => Ok(response),
            None => Err(Error::ConnectionClosed),
        }
    }

    pub async fn query(&mut self, request: Message) -> Result<Message, Error> {
        self.send(request).await?;
        match self.stream.next().await.transpose()? {
            Some(Message::Error(reason)) => Err(Error::Remote(reason)),
            Some(response) => Ok(response),
            None => Err(Error::ClosedUnexpectedly),
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    IO(#[from] std::io::Error),

    #[error(transparent)]
    Codec(#[from] super::messages::Error),

    #[error("error response: {0}")]
    Remote(String),

    #[error("connection closed by remote side")]
    ConnectionClosed,

    #[error("connection closed unexpectedly")]
    ClosedUnexpectedly,

    #[error("unexpected response: {0:?}")]
    UnexpectedResponse(Message),
}
