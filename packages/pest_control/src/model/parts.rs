use std::collections::HashMap;
use std::fmt;

use bytes::{Buf, BufMut};
use internment::Intern;

use crate::protocol::{Deserialize, Serialize};

// ======================================== helper macros ========================================

#[macro_export]
macro_rules! impl_newtype {
    ( $newtype:ident : $inner:ty ) => {
        impl From<$inner> for $newtype {
            fn from(value: $inner) -> Self {
                Self(value)
            }
        }

        impl From<$newtype> for $inner {
            fn from(value: $newtype) -> Self {
                value.0
            }
        }

        impl Serialize for $newtype {
            type Error = <$inner as Serialize>::Error;

            fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
                self.0.serialize(dst)
            }
        }

        impl Deserialize for $newtype {
            type Error = <$inner as Deserialize>::Error;

            fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error> {
                Ok(Self(<$inner>::deserialize(src)?))
            }
        }
    };
}

// ======================================== SiteId ========================================

#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq)]
pub struct SiteId(u32);

crate::impl_newtype!(SiteId: u32);

impl fmt::Display for SiteId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "#{}", self.0)
    }
}

// ======================================== Species ========================================

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
pub struct Species(Intern<String>);

impl From<String> for Species {
    fn from(value: String) -> Self {
        Self(Intern::new(value))
    }
}

impl From<&str> for Species {
    fn from(value: &str) -> Self {
        Self(Intern::from_ref(value))
    }
}

impl AsRef<str> for Species {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl std::borrow::Borrow<str> for Species {
    fn borrow(&self) -> &str {
        self.0.as_ref()
    }
}

impl Serialize for Species {
    type Error = <String as Serialize>::Error;

    fn serialize<B: BufMut>(&self, dst: &mut B) -> Result<(), Self::Error> {
        self.0.serialize(dst)
    }
}

impl Deserialize for Species {
    type Error = <String as Deserialize>::Error;

    fn deserialize<B: Buf>(src: &mut B) -> Result<Self, Self::Error> {
        Ok(Self::from(String::deserialize(src)?))
    }
}

impl fmt::Display for Species {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

// ======================================== PolicyAction ========================================

#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq)]
pub enum PolicyAction {
    Cull,
    Conserve,
}

impl fmt::Display for PolicyAction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use PolicyAction::*;
        match self {
            Cull => f.write_str("cull"),
            Conserve => f.write_str("conserve"),
        }
    }
}

// ======================================== PolicyId ========================================

#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq)]
pub struct PolicyId(u32);

crate::impl_newtype!(PolicyId: u32);

impl fmt::Display for PolicyId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "#{}", self.0)
    }
}

// ======================================== Count ========================================

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Default)]
pub struct Count(u32);

crate::impl_newtype!(Count: u32);

impl fmt::Display for Count {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

// ======================================== PopulationTargets ========================================

pub type PopulationTargets = HashMap<Species, (Count, Count)>;

// ======================================== PopulationCount ========================================

pub type PopulationCount = HashMap<Species, Count>;
