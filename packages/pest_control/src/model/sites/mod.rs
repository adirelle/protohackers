use std::collections::HashMap;

use super::parts::*;

// ======================================== Site ========================================

#[derive(Debug)]
pub struct Site<A: std::fmt::Debug> {
    authority: A,
    species: HashMap<Species, SpeciesStatus>,
}

impl<A> Site<A>
where
    A: Authority + std::fmt::Debug,
{
    pub async fn new(site: SiteId) -> Result<Self, A::Error> {
        let authority = A::dial(site).await?;
        let species = authority
            .population_targets()
            .iter()
            .map(|(species, (min, max))| (species.clone(), SpeciesStatus::new(*min, *max)))
            .collect();
        Ok(Self { authority, species })
    }

    pub async fn visit(&mut self, counts: PopulationCount) -> Result<(), A::Error> {
        for (species, status) in self.species.iter_mut() {
            let count = counts.get(species).copied().unwrap_or_default();
            let target = status.advise(count);
            if let Some(id) = status.should_delete(target) {
                self.authority.delete_policy(id).await?;
                tracing::trace!(%species, %id, "deleted policy");
                status.deleted();
            }
            if let Some(action) = status.should_create(target) {
                let id = self
                    .authority
                    .create_policy(species.clone(), action)
                    .await?;
                tracing::trace!(%species, %action, %id, "created policy");
                status.created(id, action);
            }
        }
        Ok(())
    }
}

// ======================================== SpeciesStatus ========================================

#[derive(Debug)]
pub struct SpeciesStatus {
    min: Count,
    max: Count,
    policy: Option<(PolicyId, PolicyAction)>,
}

impl SpeciesStatus {
    pub fn new(min: Count, max: Count) -> Self {
        Self {
            min,
            max,
            policy: None,
        }
    }

    pub fn advise(&self, count: Count) -> Option<PolicyAction> {
        if count < self.min {
            Some(PolicyAction::Conserve)
        } else if count > self.max {
            Some(PolicyAction::Cull)
        } else {
            None
        }
    }

    pub fn should_delete(&self, target: Option<PolicyAction>) -> Option<PolicyId> {
        match (&self.policy, target) {
            (Some((id, _)), None) => Some(*id),
            (Some((id, current)), Some(target)) if *current != target => Some(*id),
            _ => None,
        }
    }

    pub fn deleted(&mut self) {
        self.policy = None;
    }

    pub fn should_create(&self, target: Option<PolicyAction>) -> Option<PolicyAction> {
        if self.policy.is_none() {
            target
        } else {
            None
        }
    }

    pub fn created(&mut self, id: PolicyId, action: PolicyAction) {
        self.policy = Some((id, action));
    }
}

// ======================================== Authority ========================================

#[allow(async_fn_in_trait)]
pub trait Authority
where
    Self: Sized,
{
    type Error;

    async fn dial(site: SiteId) -> Result<Self, Self::Error>;

    fn population_targets(&self) -> &PopulationTargets;

    async fn create_policy(
        &mut self,
        species: Species,
        action: PolicyAction,
    ) -> Result<PolicyId, Self::Error>;

    async fn delete_policy(&mut self, policy: PolicyId) -> Result<(), Self::Error>;
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests;
