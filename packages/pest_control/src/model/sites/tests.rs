use super::*;

#[derive(Debug)]
struct AuthorityMock {
    targets: PopulationTargets,
    policies: HashMap<PolicyId, (Species, PolicyAction)>,
    serial: u32,
}

impl AuthorityMock {
    fn has_policy(&self, species: Species, action: PolicyAction) -> bool {
        let mut actions = self
            .policies
            .values()
            .filter_map(|(s, a)| (*s == species).then_some(*a));
        let first = actions.next();
        first == Some(action) && actions.next().is_none()
    }

    fn has_no_policy(&self, species: Species) -> bool {
        !self.policies.values().any(|(s, _)| *s == species)
    }
}

impl Authority for AuthorityMock {
    type Error = ();

    async fn dial(_site: SiteId) -> Result<Self, Self::Error> {
        Ok(Self {
            targets: HashMap::from_iter([
                (Species::from("rat"), (Count::from(0), Count::from(10))),
                (Species::from("dog"), (Count::from(1), Count::from(3))),
            ]),
            policies: HashMap::default(),
            serial: 0,
        })
    }

    fn population_targets(&self) -> &PopulationTargets {
        &self.targets
    }

    async fn create_policy(
        &mut self,
        species: Species,
        action: PolicyAction,
    ) -> Result<PolicyId, Self::Error> {
        self.serial += 1;
        let id = PolicyId::from(self.serial);
        println!("creating policy {id:?} for {species:?}: {action:?}");
        self.policies.insert(id, (species, action));
        Ok(id)
    }

    async fn delete_policy(&mut self, policy: PolicyId) -> Result<(), Self::Error> {
        if self.policies.remove(&policy).is_some() {
            println!("deleted policy {policy:?}");
        }
        Ok(())
    }
}

#[tokio::test]
async fn simple_visit() {
    let mut site = Site::<AuthorityMock>::new(SiteId::from(123))
        .await
        .expect("should success");

    site.visit(HashMap::from_iter([
        (Species::from("rat"), Count::from(11)),
        (Species::from("dog"), Count::from(0)),
    ]))
    .await
    .expect("should success");

    let authority = &site.authority;
    assert!(
        authority.has_policy(Species::from("rat"), PolicyAction::Cull),
        "expected cull policy for rats"
    );
    assert!(
        authority.has_policy(Species::from("dog"), PolicyAction::Conserve),
        "expected conserve policy for dogs"
    );
}

#[tokio::test]
async fn two_visists() {
    let mut site = Site::<AuthorityMock>::new(SiteId::from(123))
        .await
        .expect("should success");

    site.visit(HashMap::from_iter([
        (Species::from("rat"), Count::from(11)),
        (Species::from("dog"), Count::from(2)),
    ]))
    .await
    .expect("should success");

    {
        let authority = &site.authority;
        assert!(
            authority.has_policy(Species::from("rat"), PolicyAction::Cull),
            "expected cull policy for rats"
        );
        assert!(
            authority.has_no_policy(Species::from("dog")),
            "expected no policy for dogs"
        );
    }

    site.visit(HashMap::from_iter([
        (Species::from("rat"), Count::from(6)),
        (Species::from("dog"), Count::from(8)),
    ]))
    .await
    .expect("should success");

    {
        let authority = &site.authority;
        assert!(
            authority.has_no_policy(Species::from("rat")),
            "expected no policy for rats"
        );
        assert!(
            authority.has_policy(Species::from("dog"), PolicyAction::Cull),
            "expected cull policy for dogs"
        );
    }
}
