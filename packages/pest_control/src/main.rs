use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::sync::Arc;

use model::SiteId;
use protocol::{AuthorityServer, Error};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::Mutex;
use tokio::task::JoinSet;
use tracing::{Instrument, Level};
use tracing_appender::non_blocking::WorkerGuard;

use crate::protocol::{Conn, Message};

mod model;
mod protocol;

type Armux<T> = Arc<Mutex<T>>;
type Site = model::Site<AuthorityServer>;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let _guard = setup_tracing()?;

    let listener = TcpListener::bind("0.0.0.0:20000").await?;
    let mut clients = JoinSet::<()>::new();
    let sites: Sites = Default::default();

    tracing::info!(socket=?listener.local_addr(), "ready to serve");

    loop {
        tokio::select! {
            Ok((stream, addr)) = listener.accept() => {
                let span = tracing::info_span!("client", %addr);
                let sites = sites.clone();
                clients.spawn(client(stream, sites).instrument(span));
            },
            Some(result) = clients.join_next() => {
                if let Err(error) = result {
                    tracing::warn!(%error, "client aborted");
                }
            },
            else => {
                tracing::info!("done");
                return Ok(());
            }
        }
    }
}

async fn client(stream: TcpStream, sites: Sites) {
    let mut conn = Conn::new(stream);
    match client_inner(&mut conn, sites).await {
        Ok(()) | Err(Error::ConnectionClosed) => tracing::info!("client disconnected"),
        Err(error) => {
            tracing::warn!(%error, "client failed");
            if let Err(error) = conn.send(Message::Error(error.to_string())).await {
                tracing::warn!(%error, "could not send error message");
            }
        }
    }
}

async fn client_inner(conn: &mut Conn, sites: Sites) -> Result<(), Error> {
    tracing::info!("client connected");
    conn.hello().await?;

    loop {
        let (site_id, counts) =
            protocol::conn::receive!(conn, Message::SiteVisit(site, counts) => (site, counts));

        let span = tracing::info_span!("site", id=%site_id);
        async {
            tracing::debug!(num=?counts.len(), "received visit");

            let site_armux = sites.site(site_id).await?;
            let mut site = site_armux.lock().await;
            site.visit(counts).await?;

            Ok::<_, Error>(())
        }
        .instrument(span)
        .await?;
    }
}

#[derive(Debug, Default, Clone)]
pub struct Sites {
    sites: Armux<HashMap<SiteId, Armux<Site>>>,
}

impl Sites {
    pub async fn site(&self, id: SiteId) -> Result<Armux<Site>, Error> {
        let mut sites = self.sites.lock().await;
        Ok(match sites.entry(id) {
            Entry::Occupied(occupied) => occupied.get().clone(),
            Entry::Vacant(vacant) => {
                let site = Arc::new(Mutex::new(Site::new(id).await?));
                vacant.insert(site.clone());
                tracing::debug!("registered new site");
                site
            }
        })
    }
}

fn setup_tracing() -> Result<WorkerGuard, Box<dyn std::error::Error + 'static>> {
    use tracing::subscriber::set_global_default;
    use tracing_subscriber::{filter::*, fmt::layer, layer::SubscriberExt, prelude::*, registry};

    let (stderr, guard) = tracing_appender::non_blocking(std::io::stderr());

    set_global_default(
        registry().with(
            layer()
                .with_writer(stderr)
                .with_target(false)
                .with_filter(LevelFilter::from(Level::TRACE))
                .with_filter(FilterFn::new(|metadata| {
                    metadata
                        .module_path()
                        .is_some_and(|path| path.starts_with("pest_control"))
                })),
        ),
    )?;

    Ok(guard)
}
